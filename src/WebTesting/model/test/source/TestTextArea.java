/**
 * 
 */
package WebTesting.model.test.source;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import WebTesting.controller.ButtonActions;
import WebTesting.controller.rightPane.test.ControlTestLowerPanelController;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.enums.Controllers;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.popup.ModifyTestPopupMenu;
import WebTesting.view.rightPane.test.EditorTest;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 31. 3. 2017
 * File: TestTextArea.java
 */
public class TestTextArea extends JTextArea {

	
	
	public TestTextArea(){
		this.addMouseListener(new TextAreaMouseListener());
		this.setFont(new Font("Monospaced", Font.PLAIN, 12));
	}
	
	/**
	 * Metoda pro nastaveni zakladnich klavesovych zkratek v textovem editoru
	 */
	private void setKeyShortCut() {
		Document doc = this.getDocument();

		UndoManager manager = new UndoManager();

		this.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
		this.getActionMap().put("Undo", new AbstractAction("Undo") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(manager.canUndo()){
						manager.undo();
					}
				}catch (CannotUndoException e){
					e.printStackTrace();
				}
			}
		});

		
		this.getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
		this.getActionMap().put("Redo", new AbstractAction("Redo") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(manager.canRedo()){
						manager.redo();
					}
				}catch(CannotRedoException e){
					e.printStackTrace();
				}

			}
		});
		

		this.getInputMap().put(KeyStroke.getKeyStroke("control S"), "Update");
		this.getActionMap().put("Update", new AbstractAction("Update") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ButtonActions.updateButton(((EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL)).getModel());
			}
		});
		
		
		this.getInputMap().put(KeyStroke.getKeyStroke("control A"), "Add");
		this.getActionMap().put("Add", new AbstractAction("Add") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				((ControlTestLowerPanelController) MainWindow.getController(Controllers.TEST_LOWER_PANEL_CTRL)).addTest(
						(TreeNode) MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getModel()
						);
			}
		});
		
		doc.addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				manager.addEdit(e.getEdit());
			}
		});
	}
	
	/** Trida pro obsluhu klikani do editoru testu. Slouzi pro editaci aktualniho testu
	 * @author Pavel Skala
	 * @version 1.0 
	 * 
	 * Created on: 2. 5. 2016
	 * File: EditorTests.java
	 */
	private class TextAreaMouseListener extends MouseAdapter {
	
		@Override
		public void mouseReleased(MouseEvent me) {

			boolean rightClick = SwingUtilities.isRightMouseButton(me);

			if(rightClick){
				ModifyTestPopupMenu popMenu = new ModifyTestPopupMenu();
				popMenu.show(me.getComponent(),me.getX(), me.getY());
			}
		}
	}
}
