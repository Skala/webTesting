/**
 * 
 */
package WebTesting.model.test.source;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.test.ControlTestListPanelController;
import WebTesting.enums.Controllers;
import WebTesting.enums.tests.Attributes;
import WebTesting.enums.tests.Locators;
import WebTesting.enums.tests.Tags;
import WebTesting.enums.tests.Values;
import WebTesting.enums.tests.Waits;
import WebTesting.view.MainWindow;
//import WebTesting.model.ListItem;
//import WebTesting.view.rightPane.test.EditorTests;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/** Trida, ktera ma za ukol vykreslit seznamy v ramci editoru pro testy
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 3. 2016
 * File: ControlScenarioListPanel.java
 */
@SuppressWarnings("serial")
public class ControlTestListPanel extends JPanel {
	
	
	private JScrollPane scrollPaneTagList;
	private JScrollPane scrollPaneLocatorsList;
	private JScrollPane scrollPaneAttributeList;
	private JScrollPane scrollPaneValueList;
	private JScrollPane scrollPaneWaitList;
	
	private ArrayList<JList<IListItem>> lists;
	private JLabel lblSeznamCekani;
		
	/**
	 	Konstruktor
	 */
	public ControlTestListPanel() {
		initComponents();
	}
	
	/**
	 * Metoda, ktera vytvori komponenty pro tento panel
	 */
	private void initComponents() {
		
		
		ControlTestListPanelController controller = new ControlTestListPanelController(this);
		MainWindow.addController(controller);
		
		setLayout(new GridLayout(0, 1, 0, 0));
		lists = new ArrayList<JList<IListItem>>();
		
		JList<IListItem> listTags = new JList<IListItem>((IListItem[]) Tags.values());
		lists.add(listTags);
		listTags.addMouseListener(controller);
		scrollPaneTagList = new JScrollPane(listTags);
		scrollPaneTagList.setColumnHeaderView(new JLabel(getLabel("listOfTags")));
				
		JList<IListItem> listLocators = new JList<IListItem>((IListItem[]) Locators.values());
		lists.add(listLocators);
		listLocators.addMouseListener(controller);
		scrollPaneLocatorsList = new JScrollPane(listLocators);
		scrollPaneLocatorsList.setColumnHeaderView(new JLabel(getLabel("listOfLocators")));
		
		JList<IListItem> listAttributes = new JList<IListItem>((IListItem[]) Attributes.values());
		lists.add(listAttributes);
		listAttributes.addMouseListener(controller);
		scrollPaneAttributeList = new JScrollPane(listAttributes);
		scrollPaneAttributeList.setColumnHeaderView(new JLabel(getLabel("listOfAttributes")));
		
		JList<IListItem> listValues = new JList<IListItem>((IListItem[]) Values.values());
		listValues.addMouseListener(controller);
		lists.add(listValues);
		scrollPaneValueList = new JScrollPane(listValues);
		scrollPaneValueList.setColumnHeaderView(new JLabel(getLabel("listOfValues")));
		
		JList<IListItem> listWaits = new JList<IListItem>((IListItem[]) Waits.values());
		lists.add(listWaits);
		listWaits.addMouseListener(controller);
		scrollPaneWaitList = new JScrollPane(listWaits);
		lblSeznamCekani = new JLabel(getLabel("listOfWaits"));
		scrollPaneWaitList.setColumnHeaderView(lblSeznamCekani);
		
		add(scrollPaneLocatorsList);
		add(scrollPaneTagList);
		add(scrollPaneAttributeList);
		add(scrollPaneValueList);
		add(scrollPaneWaitList);
	}
		
//	/**
//	 * Metoda, ktera vytvori seznamy
//	 */
//	private void initLists(){
//		for (JList<IListItem> jList : lists) {
//			UtilityClass.initList(jList);
//		}
//	}
	
	
	/*--------------------
	 	Gettery a settery
	 -------------------*/
	public JList<IListItem> getListLocators() {
		return lists.get(0);
	}
	
	public JList<IListItem> getListTags() {
		return lists.get(1);
	}
	
	public JList<IListItem> getListAttributes(){
		return lists.get(2);
	}
	
	public JList<IListItem> getListValues(){
		return lists.get(3);
	}
	
	public JList<IListItem> getListWaits(){
		return lists.get(4);
	}	
}
