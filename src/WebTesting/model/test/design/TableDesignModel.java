/**
 * 
 */
package WebTesting.model.test.design;

import static WebTesting.controller.UtilityClass.getLabel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

import org.jdom2.Element;

import WebTesting.enums.Controllers;
import WebTesting.model.TestData;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.design.modules.TestDesignCommandEdit;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 7. 12. 2016
 * File: TableDesignModel.java
 */
@SuppressWarnings("serial")
public class TableDesignModel extends AbstractTableModel {

	
	protected String [] columnName = {"#", getLabel("item")};
	protected List<Element> commands = new ArrayList<Element>();
	
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnName.length;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return commands.size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int column) {
		switch(column){
		case 0:
			return row+1 + "";
		case 1:
			return this.commands.get(row);
		default:
			//TODO dopsat vyjimku
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 1:
			((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()).setEnabled(false);
			new TestDesignCommandEdit((Element) aValue);
			break;
		default:
			break;
		}
		
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnName[column];
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	/* Gettery a settery*/
	
	
	
	/**
	 * @return the data
	 */
	public List<Element> getCommands() {
		return commands;
	}
	
	
	/**
	 * @param data the data to set
	 */
	public void setData(List<Element> data) {
		this.commands = data;
	}
	
	public void myAddRow(Element element){
		this.commands.add(element);
		fireTableRowsInserted(commands.size()-1, commands.size()-1);
	}
	
	public void removeAllCommands(){
		this.commands = new ArrayList<Element>();
	}
}
