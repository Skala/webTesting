/**
 * 
 */
package WebTesting.model.test.design;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.jdom2.Element;

import WebTesting.model.TreeNode;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 7. 12. 2016
 * File: TableDesignRenderer.java
 */
public class TableDesignRenderer extends JPanel implements TableCellRenderer {

	/* (non-Javadoc)
	 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		
		this.setLayout(new BorderLayout(5, 5));
		this.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
		
		
		switch (column) {
		case 0:
			JLabel text = new JLabel((String) value);
			this.add(text, BorderLayout.CENTER);
			break;
		case 1:
			JButton button = new JButton(textToButton((Element) value));
			this.add(button, BorderLayout.WEST);
			break;
		default:
			break;
		}
		
		return this;
	}

	/** Metoda pro vyplneni textu do tlacitka. Najde typ akce a jednotlive podelementy
	 * @param value
	 * @return
	 */
	private String textToButton(Element command) {
		String text = command.getAttributeValue("action") + ": ";
		List<Element> subCommand = command.getChildren();
		for(int i = 0 ; i<subCommand.size(); i++){
			text += subCommand.get(i).getName() + "=";
			text += subCommand.get(i).getChildText("label");
			if(i<subCommand.size()-1){
				text += ", ";
			}
		}
		
		return text;
	}

}
