package WebTesting.model;

import java.io.File;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import org.jdom2.Element;

import WebTesting.controller.UtilityClass;
import WebTesting.exceptions.UnknownTypeOfNodeException;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.core.Config.*;


/**	Trida, ktera slouzi pro uchovani informace o scenari nebo testu. Trida dedi implementaci od tridy DefaultMutableTreeNode, 
 * aby mohla byt data strukturovana ve forme stromu.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 2. 2016
 * File: TreeNode.java
 */
@SuppressWarnings("serial")
public class TreeNode extends DefaultMutableTreeNode{
	
	/**
	 * Reference na rodice daneho uzlu
	 */
	protected TreeNode parentTreeNode;
	
	/**
	 *	Reference na data prislusneho uzlu 
	 */
	protected DataNode data;
	
	/**
	 *	Cesta v souborovem systemu ke zdrojovemu souboru uzlu 
	 */
	protected String filePath;	
	
	/**
	 *	Atribut, ktery urcuje, zda se jedna o scenar nebo test 
	 */
	protected String typeOfNode;
	
	
	/*----------------------------------
	 * 
	 * Konstruktory
	 * 
	 ------------------------------------*/
	public TreeNode(DataNode data, String filePath){
		super(data);
		this.data = data;
		this.filePath = filePath;
		this.parentTreeNode = null;
 	}

	public TreeNode(DataNode data){
		this(data, null);
	}
	
	public TreeNode(File file){
		this.filePath = file.getPath();
		
		if(this.data != null){
			this.data.setName(file.getName());
		}
		else{
			this.data = new DataNode(file.getName());
		}
		
		if(file.getPath().contains("test")){
			this.setTypeOfNode(TYPE_TEST);
		}
		else if(file.getPath().contains("scenario")){
			this.setTypeOfNode(TYPE_SCENARIO);
		}
		else{
			try {
				throw new Exception(getLabel("UnknownTypeOfAddedNode"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public TreeNode(String filePath) {
		this(null, filePath);
	}
	
	public TreeNode(){
		this(null, null);
	}
	
	
	/*-----------------------------------
	 * 
	 * Gettery a settery
	 * 
	 -----------------------------------*/

	public TreeNode getParentTreeNode() {
		return parentTreeNode;
	}

	public void setParentTreeNode(TreeNode parentTreeNode) {
		this.parentTreeNode = parentTreeNode;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	

	public DataNode getData() {
		return data;
	}
	
	public void setData(DataNode data) {
		this.data = data;
	}
	
	public String getTypeOfNode() {
		if(this.typeOfNode == null){
			if(data != null){
				if(data.getRootElement() != null){
					if(data.getRootElement().getName().toLowerCase().compareTo(TYPE_TEST) == 0){
						this.typeOfNode = TYPE_TEST;
					}
					else if(data.getRootElement().getName().toLowerCase().compareTo(TYPE_SCENARIO) == 0){
						this.typeOfNode = TYPE_SCENARIO;
					}
					else{
						try {
							throw new UnknownTypeOfNodeException();
						} catch (UnknownTypeOfNodeException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return this.typeOfNode;
	}
	
	public void setTypeOfNode(String typeOfNode) {
		this.typeOfNode = typeOfNode;
	}
	

	public Element getRootElement(){
		return this.getData().getRootElement();
	}
	

	public void setRootElement(Element element){
		this.data.setRootElement(element);
	}
	
	/*---------------------------
	 	Vlastni metody
	 --------------------------*/
	
	/**	Metoda, ktera vrati, zda je soucasny uzel test nebo scenar. V pripade, ze uzel jeste nebyl dostatecne vyroben, pak ve vracena neutralni hodnota
	 * @return Hodnota 1 - test, hodnota 0 - scenar, hodnota 2 - pokud uzel doposud nebyl dodefinovan
	 * @throws UnknownTypeOfNodeException 
	 */
	public int isNodeTest() throws UnknownTypeOfNodeException  {
		if(filePath != null){
			if(filePath.contains(TYPE_TEST)){
				return 1;
			}
			else if(filePath.contains(TYPE_SCENARIO)){
				return 0;
			}
			else{
				throw new UnknownTypeOfNodeException();
			}
		}
		else{
			return 2;
		}
	}
	
	/** Metoda, ktera rozhodne, zda jsou dva uzly stejne podle jejich cesty ve file systemu
	 * @param node
	 * @return Pravdivostni hodnota
	 */
	public boolean compareNode(TreeNode node){
		String one = this.getFilePath();
		String two = node.getFilePath();
		return (one.equals(two));
	}
	
	/**
	 * Metoda, ktera vymaze vsechny informativni zpravy z instance testu i ze vsech prikazu
	 */
	public void resetAllStatus(){
		if(this.getRootElement() != null){
			Element root = this.getRootElement();
			resetElement(root);
			List<Element> els = root.getChildren();
			for (Element element : els) {
				resetElement(element);
			}
		}
	}
	
	/**	Metoda, ktera smaze informativni zpravy s daneho elementu
	 * @param element Element, ze ktereho budou smazany informace o jeho prubehu
	 */
	private void resetElement(Element element){
		element.removeAttribute(STATUS);
		element.removeAttribute(MESSAGE);
		element.removeAttribute(EXCEPTION);
	}
	
	/*---------------------------
	 	prekryte metody
	 --------------------------*/
	@Override
	public String toString() {
		if(data != null){
			if(data.getName() != null){
				return data.getName();
			}
			else{
				return null;
			}
		}
		else{
			return null;
		}
	}
}
