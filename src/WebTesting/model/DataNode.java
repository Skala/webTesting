package WebTesting.model;

import java.util.List;

import org.jdom2.Element;

import WebTesting.Interfaces.IDataNode;
import WebTesting.controller.UtilityClass;
import WebTesting.core.Config;
import WebTesting.exceptions.SemanticException;

import static WebTesting.core.Config.*;

/**	Trida, ktera v sobe uchovava data, ktera se spojuji s jednou jednotkou, tj. testu nebo scenare. Uchovava v sobe prvek Element z
 * 	frameworku JDOM, coz je rodicovsky element struktrury XML.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 3. 2016
 * File: DataNode.java
 */
public class DataNode implements IDataNode{
	
	/**
	 *	Atribut, ktery uchovava rodicovsky element, ktery tvori strom ve strukture XML 
	 */
	protected Element rootElement;
	
	/**
	 * Atribute, ktery v sobe nese jmeno konkretniho uzlu
	 */
	protected String name;
	
	/**
	 * Atribut, ve kterem je obsazen text nalezici rootovskemu elementu
	 */
	protected String nodeText;

	
	/*---------------------
	 	Konstruktory
	 --------------------*/
	public DataNode(Element rootElement){
		this.rootElement = rootElement;
		this.name = rootElement.getAttributeValue("name");
	}
	
	public DataNode(String name){
		this.rootElement = null;
		this.name = name;
	}
	
	public DataNode(){
		this.rootElement = null;
		this.name = null;
	}

	
	/*----------------------------
	 	Pristupove metody
	 ---------------------------*/
	/** Metoda pro ziskani atributu name
	 * @return atribut name
	 */
	public String getName() {
		return name;
	}
	
	/**Metoda pro nastaveni atributu name
	 * @param name nastaveni atributu name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public Element getRootElement() {
		return rootElement;
	}
	
	public void setRootElement(Element rootElement) {
		this.rootElement = rootElement;
	}
	
	public String getNodeText() {
		return nodeText;
	}
	
	public void setNodeText(String nodeText) {
		this.nodeText = nodeText;
	}

	/*----------------------
	 	Ostatni metody
	 ---------------------*/	
	
	/**	Prevod scenare na test, provede se pouze modifikace xml dokumentu 
	 * @param currentScenario Scenar ze ktereho se vytvori test
	 * @return Data pro test
	 */
	public static DataNode convertScenarioToTestXml(TreeNode currentScenario) {
		
		DataNode data = new DataNode(currentScenario.getRootElement());
		data.getRootElement().setName("test");
		return data;
	}
	
	/*----------------------
	 	Metody z rozhrani
	 ----------------------*/
	
	
	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IDataNode#elementsToString()
	 */
	public String elementsToString() {
		StringBuilder sb = new StringBuilder();
		
		List<Element> commands = this.getRootElement().getChildren();
		
		for(int i = 0 ; i < commands.size(); i++){
			sb.append(commandToString(commands.get(i)));
			sb.append(";" + Config.lineSeparator);
		}
		return sb.toString().trim();
	}
	
	/** Pomocna metoda, ktera prevede Element ze xml struktury na retezec
	 * @param command Element pro prevedeni na retezec 
	 * @return Retezec, ktery reprezentuje vstupni element
	 */
	private String commandToString(Element command){
		StringBuilder sb = new StringBuilder();
		
		String action = command.getAttributeValue("action");
		if(action != null){
			sb.append(action);
			sb.append(", ");
		}
		
		List<Element> subCommands = command.getChildren();
		for(int i = 0 ; i < subCommands.size(); i++){
			sb.append(subCommandToString(subCommands.get(i)));
			sb.append(", ");
		}

		String ret = sb.toString();
		int index =  ret.lastIndexOf(",");
		if(index != -1){
			ret = ret.substring(0, index);
		}
		
		return ret;
	}

	/** Pomocna metoda, ktera prevede Element ze xml struktury na text
	 * @param subCommand Element pro prevedeni na text
	 * @return Retezec, ktery reprezentuje vstupni element
	 */
	private String subCommandToString(Element subCommand) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(subCommand.getName());
		sb.append("[");
		List<Element> attribute = subCommand.getChildren();
		for(int i = 0 ; i < attribute.size() ; i++){
			sb.append(attribute.get(i).getName() + "(" + attribute.get(i).getText() + ") ");
		}
		String ret = sb.toString().trim() + "]";
		return ret;
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IDataNode#elementsToTextTest()
	 */
	@Override
	public String elementsToTextTest() {
		if(this.getRootElement() != null){
			StringBuilder sb = new StringBuilder();
			
			List<Element> commands = this.getRootElement().getChildren(COMMAND);
			for(int i = 0 ; i < commands.size(); i++){
				sb.append(commandToTextTest(commands.get(i)));
				sb.append(";" + Config.lineSeparator);
			}
			return sb.toString().trim();
		}
		else{
			return "";
		}
	}

	/** Pomocna metoda, ktera prevede Element ze xml struktury na test
	 * @param command Element pro prevedeni na test
	 * @return Retezec, ktery reprezentuje vstupni element
	 */
	public  String commandToTextTest(Element command) {
		StringBuilder sb = new StringBuilder();
	
		List<Element> subCommand = command.getChildren();
		for(int i = 0 ; i < subCommand.size(); i++){
			sb.append(subCommandToTestText(subCommand.get(i)));
			sb.append(", ");
		}
		String ret = sb.toString();
		int index = ret.lastIndexOf(",");
		if(index != -1){
			ret = ret.substring(0, index);
		}
		return ret;
	}
	
	
	/** Pomocna metoda, ktera prevede Element ze xml struktury na test
	 * @param subCommand Element pro prevedeni na test
	 * @return Retezec, ktery reprezentuje vstupni element
	 */
	private String subCommandToTestText(Element subCommand) {
		StringBuilder sb = new StringBuilder();
		
		List<Element> subSubCommand = subCommand.getChildren();
		Element label = subCommand.getChild("label");
		
		if(label != null){
			
			sb.append("##" + label.getTextTrim() + "##=");
			for(int i = 0 ; i < subSubCommand.size(); i++){
				sb.append(subSubCommandToTestText(subSubCommand.get(i)));
			}
		}
		else{
			try {
				throw new SemanticException();
			} catch (SemanticException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/** Metoda, ktera prevede element na text
	 * @param element Element, ktery chceme prevest
	 * @return
	 */
	private String subSubCommandToTestText(Element subSubCommand) {
		if(subSubCommand.getName() != LABEL){
			StringBuilder sb = new StringBuilder();
			
			String name = subSubCommand.getName();
			
			if(UtilityClass.isItemInLists(name)){
				//Pokud je element v subelementu pojmenovan "content", pak jej neuvedeme
				if(CONTENT.equals(name)){
					sb.append("##" + subSubCommand.getTextTrim() + "##");
				}
				else{
					sb.append(subSubCommand.getName() + "=##" + subSubCommand.getTextTrim()+ "##");
				}
			}
			return sb.toString();
		}
		else{
			return "";
		}
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IDataNode#elementsToTextScenario()
	 */
	@Override
	public String elementsToTextScenario() {
		if(this.getRootElement() != null){
			StringBuilder sb = new StringBuilder();

			List<Element> commands = this.getRootElement().getChildren(COMMAND);
			for(int i = 0 ; i < commands.size(); i++){
				sb.append(commandToTextScenario(commands.get(i)));
				sb.append(";" + Config.lineSeparator);
			}
			return sb.toString().trim();
		}
		else{
			return "";
		}		
	}
	
	/** Pomocna metoda, ktera prevede Element ze xml struktury na scenar
	 * @param element Element pro prevedeni na scenar
	 * @return Retezec, ktery reprezentuje vstupni element
	 */
	public String commandToTextScenario(Element element) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(element.getAttributeValue("action"));
		sb.append(", ");
		
		List<Element> subCommand = element.getChildren();
		for(int i = 0 ; i < subCommand.size() ; i++){
			sb.append(subCommandToTextScenario(subCommand.get(i)));
			sb.append(", ");
		}
		String ret = sb.toString();
		ret = ret.substring(0, ret.lastIndexOf(","));
		return ret;
	}

	/** Pomocna metoda, ktera prevede Element ze xml struktury na scenar
	 * @param element Element pro prevedeni na scenar
	 * @return Retezec, ktery reprezentuje vstupni element
	 */
	private String subCommandToTextScenario(Element element) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(element.getName() + "=");
		Element el;
		if((el = element.getChild("label")) != null){
			sb.append("##" + el.getTextTrim() + "##" );
		}
		
		return sb.toString();
	}
}