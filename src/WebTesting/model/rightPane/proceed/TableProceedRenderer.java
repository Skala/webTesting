/**
 * 
 */
package WebTesting.model.rightPane.proceed;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;


/**	Trida, ktera ma za ukol vykresleni tabulky slouzici pro prehledne zobrazeni pripravenych nebo dokoncenych testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 2. 2016
 * File: TableButtonRenderer.java
 */
@SuppressWarnings("serial")
public class TableProceedRenderer extends JPanel implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
			
		this.setLayout(new BorderLayout(5,5));
		this.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
		
		if(column == 0){
			JCheckBox checkBox = new JCheckBox();
			checkBox.setHorizontalAlignment(SwingConstants.CENTER);
			
			checkBox.setSelected((boolean)value);
			this.add(checkBox, BorderLayout.CENTER);
		}
		else if(column == 2){
			JProgressBar progressBar = new JProgressBar();
			progressBar.setValue((int) value);
			this.add(progressBar, BorderLayout.CENTER);
		}
		else if(column == 3){
			JButton button = new JButton((String) value);
			this.add(button, BorderLayout.CENTER);
		}
		else{
			JLabel label = new JLabel((String) value);	
			
			this.add(label, BorderLayout.WEST);
		}
		
		return this;
	}
}
