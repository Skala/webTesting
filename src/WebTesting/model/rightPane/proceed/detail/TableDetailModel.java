/**
 * 
 */
package WebTesting.model.rightPane.proceed.detail;

import static WebTesting.controller.UtilityClass.getLabel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.jdom2.Element;

import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.DataNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.RightPane;


/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 6. 4. 2016
 * File: TableDetailModel.java
 */
@SuppressWarnings("serial")
public class TableDetailModel extends AbstractTableModel {

	protected String [] columnName = {"#", getLabel("description"), getLabel("detail"), getLabel("state")};
	protected List<Element> children = new ArrayList<Element>();

	public TableDetailModel() {	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnName.length;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return children.size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int column) {
		Element element = children.get(row);

		switch(column){
		case 0:
			return (row+1);
		case 1:
			return element.getAttributeValue("message");
		case 2:
			return "Detail";
		case 3:
			return element.getAttributeValue("status");
		default:
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnName[column];
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			break;
		case 1:
			break;
		case 2:
			MainWindow.getController(Controllers.TEST_DETAIL_COMMAND_CTRL).setModel(getTextToArea((Element) aValue, columnIndex));
			((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.COMMAND_DETAIL);
			break;
		case 3:
			break;
		default:
			break;
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	/**
	 * @param aValue
	 * @return
	 */
	private String getTextToArea(Element aValue, int columnIndex) {
		
		DataNode node = new DataNode(aValue);
		
		String text = node.commandToTextTest(aValue) + System.lineSeparator(); 
		
		if(aValue.getAttributeValue("message") != null){
			text += aValue.getAttributeValue("message") + System.lineSeparator();
		}
		if(aValue.getAttributeValue("exception") != null){
			text += aValue.getAttributeValue("exception") ;
		}
		return text;
	}

	public void setRootElement(Element root){
		this.children = root.getChildren(Config.COMMAND);
		fireTableDataChanged();
	}
	
	public Element getChild(int index){
		return children.get(index);
	}
}
