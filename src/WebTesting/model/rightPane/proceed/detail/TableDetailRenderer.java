/**
 * 
 */
package WebTesting.model.rightPane.proceed.detail;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import WebTesting.controller.UtilityClass;
import WebTesting.core.Config;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 6. 4. 2016
 * File: TableDetailRenderer.java
 */
@SuppressWarnings("serial")
public class TableDetailRenderer extends JPanel implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		
		this.setLayout(new BorderLayout(5, 5));
		this.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
		
		JLabel label;
		
		switch(column){
		case 0:
			label = new JLabel((row+1) + "", SwingConstants.CENTER);
			label.setVerticalAlignment(SwingConstants.CENTER);
			this.add(label, BorderLayout.CENTER);
			break;
		case 2:
			JButton button = new JButton((String) value);
			button.setVerticalAlignment(SwingConstants.CENTER);
			this.add(button, BorderLayout.CENTER);
			break;
		case 3:
			label = new JLabel((String) value, SwingConstants.CENTER);	
			if(label.getText() != null){
				if(label.getText().equals(Config.STATUS_ERROR)){
					setBackground(Config.COLOR_RED);
				}
				else if(label.getText().equals(Config.STATUS_SUCCESS)){
					setBackground(Config.COLOR_GREEN);
				}
			}
			this.add(label, BorderLayout.CENTER);
			break;
		default:
			label = new JLabel((String) value);	
			label.setVerticalAlignment(SwingConstants.CENTER);
			this.add(label, BorderLayout.WEST);
			break;
		}
				
		return this;
	}

	
}
