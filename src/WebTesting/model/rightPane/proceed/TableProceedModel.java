/**
 * 
 */
package WebTesting.model.rightPane.proceed;

import static WebTesting.controller.UtilityClass.getLabel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.model.TestData;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.RightPane;
import WebTesting.view.rightPane.proceed.detail.TestDetail;

/**
 * Metoda, ktera predstavuje model pro tabulku, ktera se vyuziva pro zobrazeni testu, ktere jsou pripravene ke spusteni, nebo ktere jiz dobehly
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 2. 2016
 * File: TableProceedModel.java
 */
@SuppressWarnings("serial")
public class TableProceedModel extends AbstractTableModel  {

	protected String [] columnName = {"#", getLabel("title"), getLabel("process"), getLabel("detail")};
	protected ArrayList<TestData> data = new ArrayList<TestData>();
	
	
	public TableProceedModel() {
	}
	
	@Override
	public int getColumnCount() {
		return columnName.length;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	
	@Override
	public Object getValueAt(int row, int column) {
		
		TestData test = data.get(row);
		switch(column){
		case 0:
			return test.isChosen();
		case 1:
			return test.getNode().getData().getName();
		case 2:
			return test.getProgress();
		case 3:
			return test.getDetail();
		default:
			return null;
		}
	}
	
	@Override
	public String getColumnName(int column) {
		return columnName[column];
	}
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			data.get(rowIndex).setChosen((data.get(rowIndex).isChosen() == true) ? false : true);
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			MainWindow.getController(Controllers.TEST_DETAIL_CTRL).setModel(aValue);
			((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.TEST_DETAIL);
				
			break;
		default:
			break;
		}
		
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	
	public void myAddRow(TestData newData){
		newData.getNode().resetAllStatus();
		this.data.add(newData);
		fireTableRowsInserted(data.size()-1, data.size()-1);
	}

	
	/*-----------------------
	 * Gettery a settery
	 -----------------------*/
	public ArrayList<TestData> getData() {
		return data;
	}

	public void setData(ArrayList<TestData> data) {
		this.data = data;
	}
}
