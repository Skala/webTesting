/**
 * 
 */
package WebTesting.model;

import javax.swing.table.AbstractTableModel;

import WebTesting.enums.Controllers;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.proceed.EditorProceed;

/**	Trida, ktera slouzi jako data do tabulky, ve ktere budou zobrazeny veskere testy, at nove vytvorene, nebo dokoncene.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 2. 2016
 * File: TestData.java
 */
public class TestData {

	/**
	 * Atribut, ktery signalizuje zdali je aktualni test zvolen nebo ne. Slouzi predevsim pro spousteni nebo mazani testu.
	 */
	protected boolean chosen;
	
	/**
	 * Polozka, ktera bude reprezentovat tlacitko, po jehoz stisknuti se dostaneme na detail zvoleneho testu
	 */
	protected String detail;
	
	/**
	 * Progres bar, ktery signalizuje jak daleko kroky v testu pokrocily.
	 */
	protected int progress; 
	
	/**
	 * Data, ktera obsahuje dany test. 
	 */
	protected TreeNode node;
	
	
	/*---------------------
	 	Konstruktory
	 ---------------------*/
	public TestData(TreeNode node){
		this.node = node;
		this.chosen = true;
		this.detail = "detail";
		this.progress = 0;
	}
	
	public TestData(){
		this(null);
	}
	
	/*-------------------------
	  	Metody
	 -------------------------*/
	public void fireChange(){
		((AbstractTableModel)(((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).getTable().getModel())).fireTableDataChanged();
	}
	
	
	/*-------------------------
	 * Gettery a settery
	 -------------------------*/
	
	/**
	 * @return
	 */
	public boolean isChosen() {
		return chosen;
	}

	/**
	 * @param chosen
	 */
	public void setChosen(boolean chosen) {
		this.chosen = chosen;
		fireChange();
	}

	/**
	 * @return
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail
	 */
	public void setDetail(String detail) {
		this.detail = detail;
		
	}

	/**
	 * @return
	 */
	public TreeNode getNode() {
		return node;
	}

	/**
	 * @param node
	 */
	public void setNode(TreeNode node) {
		this.node = node;
		fireChange();
	}

	/**
	 * @return
	 */
	public int getProgress() {
		return progress;
	}
	
	/**
	 * @param value
	 */
	public void setProgress(int value){
		this.progress = value;
		fireChange();
	}
}
