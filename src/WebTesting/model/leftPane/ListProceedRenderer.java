/**
 * 
 */
package WebTesting.model.leftPane;


import static WebTesting.core.Config.*;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.jdom2.Element;

import WebTesting.core.Config;
import WebTesting.model.TestData;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 4. 2016
 * File: ListProceedRenderer.java
 */
@SuppressWarnings("serial")
public class ListProceedRenderer extends JLabel implements ListCellRenderer {


	public ListProceedRenderer() {
		setOpaque(true);
	}
	
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		TestData test = (TestData) value;
		Element root = test.getNode().getRootElement();
		
		setText(root.getAttributeValue("name"));
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		String status = root.getAttributeValue("status");
		if(status != null){
			if(status.equals(STATUS_ERROR)){
				setBackground(Config.COLOR_RED);
			}
			else if(status.equals(STATUS_RUNNING)){
				setBackground(Config.COLOR_BLUE);
			}
			else if(status.equals(STATUS_SUCCESS)){
				setBackground(Config.COLOR_GREEN);
			}
			
		}
		return this;
	}

}
