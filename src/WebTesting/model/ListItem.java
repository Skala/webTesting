/**
 * 
 */
package WebTesting.model;

import WebTesting.Interfaces.IListItem;
import WebTesting.enums.ListTypes;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 6. 3. 2016
 * File: MyElement.java
 */
public class ListItem implements IListItem{
	
	protected ListTypes type;
	
	/**
	 * Atribut, ktery predstavuje konkretni nazev instance
	 */
	protected String name;
	
	protected String parameters;
	
	/**
	 * Atribut doplnujici informace o prvku v seznamu
	 */
	protected String hint;
	
	
	/*-----------------
	 	Konstruktory
	 -----------------*/
	public ListItem(String name, String parameter, String hint){
		this.name = name;
		this.parameters = parameter;
		this.hint = hint;
	}
	
	public ListItem(String name, String parameter){
		this(name, parameter, null);
	}
	
	public ListItem(String name){
		this(name, null);
	}
	
	public ListItem(){
		this(null);
	}
	
	

	/*-------------------
	 	Pristupove metody
	 -------------------*/
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the label
	 */
	public String getLabel() {
		return hint;
	}
	
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.hint = label;
	}
	
	/*------------------
	  	Prekryte metody
	 ------------------*/
	@Override
	public String toString() {
		return this.name;
	}
}
