/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 4. 2016
 * File: WarningException.java
 */
@SuppressWarnings("serial")
public class WarningException extends Exception {

	public WarningException(String exceptionText){
		super(exceptionText);
	}
	
	public WarningException(){
		super("Warning exception");
	}
}
