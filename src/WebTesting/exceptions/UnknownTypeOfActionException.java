/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 3. 4. 2016
 * File: UnknownTypeOfActionException.java
 */
@SuppressWarnings("serial")
public class UnknownTypeOfActionException extends Exception {

	public UnknownTypeOfActionException(String text){
		super(text);
	}
	
	public UnknownTypeOfActionException(){
		super("Unknown type of action");
	}
}
