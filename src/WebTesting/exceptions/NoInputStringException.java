/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 15. 3. 2016
 * File: NoInputString.java
 */
@SuppressWarnings("serial")
public class NoInputStringException extends Exception {

	public NoInputStringException(String message){
		super(message);
	}
}
