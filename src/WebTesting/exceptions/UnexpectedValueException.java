/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 3. 4. 2016
 * File: UnexpectedValueException.java
 */
@SuppressWarnings("serial")
public class UnexpectedValueException extends Exception {

	public UnexpectedValueException(String exceptionText){
		super(exceptionText);
	}
	
	public UnexpectedValueException(){
		super("Unexpect value");
	}
}
