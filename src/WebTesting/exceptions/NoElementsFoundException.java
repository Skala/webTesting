/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 22. 3. 2016
 * File: NoElementsFoundException.java
 */
@SuppressWarnings("serial")
public class NoElementsFoundException extends Exception {

	public NoElementsFoundException(String exceptionText){
		super(exceptionText);
	}
	
	public NoElementsFoundException(){
		super("No elements were found");
	}
}
