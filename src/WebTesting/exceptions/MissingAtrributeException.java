/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 3. 4. 2016
 * File: MissingAtrributeException.java
 */
@SuppressWarnings("serial")
public class MissingAtrributeException extends Exception {

	public MissingAtrributeException(String exceptionText){
		super(exceptionText);
	}
	
	public MissingAtrributeException(){
		super("Missing atribute");
	}
}
