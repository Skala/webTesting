/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 22. 3. 2016
 * File: MoreElementsFoundException.java
 */
@SuppressWarnings("serial")
public class MoreElementsFoundException extends Exception {

	public MoreElementsFoundException(String exceptionText){ 
		super(exceptionText);
	}
	
	public MoreElementsFoundException(){
		super("More elements were found");
	}
}
