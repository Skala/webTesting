/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 4. 2016
 * File: LessElementsFoundException.java
 */
@SuppressWarnings("serial")
public class LessElementsFoundException extends Exception {

	public LessElementsFoundException(String exceptionText){
		super(exceptionText);
	}
	
	public LessElementsFoundException(){
		super("Less elements were found");
	}
}
