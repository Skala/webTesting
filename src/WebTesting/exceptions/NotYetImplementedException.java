/**
 * 
 */
package WebTesting.exceptions;

import javax.swing.JOptionPane;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 22. 3. 2016
 * File: NoYetImplementedException.java
 */
@SuppressWarnings("serial")
public class NotYetImplementedException extends Exception {

	public NotYetImplementedException(String exceptionText){
		super(exceptionText);
		printStackTrace();
		JOptionPane.showMessageDialog(null, exceptionText);
	}
	
	public NotYetImplementedException(){
		this("Not yet implemented");
	}
}
