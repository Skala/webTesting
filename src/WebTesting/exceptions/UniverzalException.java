/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 3. 4. 2016
 * File: UniverzalException.java
 */
@SuppressWarnings("serial")
public class UniverzalException extends Exception {

	public UniverzalException(String exceptionText){
		super(exceptionText);
	}
	
	public UniverzalException(){
		super("Univerzal exception");
	}
}
