/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 4. 2016
 * File: UnknownTypeOfLocator.java
 */
@SuppressWarnings("serial")
public class UnknownTypeOfLocator extends Exception {

	public UnknownTypeOfLocator(String exceptionText){
		super(exceptionText);
	}
	
	public UnknownTypeOfLocator(){
		super("unknown type of locator");
	}
}
