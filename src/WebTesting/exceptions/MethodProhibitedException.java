/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 7. 7. 2017
 * File: MethodProhibitedException.java
 */
public class MethodProhibitedException extends Exception {

	public MethodProhibitedException(String exceptionText) {
		super(exceptionText);
	}
	
	public MethodProhibitedException() {
		this("method is prohibited");
	}
}
