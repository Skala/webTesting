/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 4. 2016
 * File: TextIsNotPresented.java
 */
@SuppressWarnings("serial")
public class TextIsNotPresented extends Exception {

	public TextIsNotPresented(String exceptionText){
		super(exceptionText);
	}
	
	public TextIsNotPresented(){
		super("Required text has not been found");
	}
}
