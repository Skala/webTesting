/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 4. 2016
 * File: SemanticException.java
 */
@SuppressWarnings("serial")
public class SemanticException extends Exception {

	public SemanticException(String exceptionText){
		super(exceptionText);
	}
	
	public SemanticException(){
		super("Semantic exception");
	}
}
