/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: ControllerNotFoundException.java
 */
public class ControllerNotFoundException extends Exception {

	
	public ControllerNotFoundException(String exceptionText) {
		super(exceptionText);
	}
	
	public ControllerNotFoundException(){
		this("Controller not found");
	}
}
