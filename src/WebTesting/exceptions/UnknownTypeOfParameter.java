/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 4. 2016
 * File: UnknownTypeOfParameter.java
 */
@SuppressWarnings("serial")
public class UnknownTypeOfParameter extends Exception {

	public UnknownTypeOfParameter(String exceptionText){
		super(exceptionText);
	}
	
	public UnknownTypeOfParameter(){
		super("Unknown type of parameter");
	}
}
