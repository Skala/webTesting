/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 3. 2016
 * File: UnknownTypeOfNode.java
 */
@SuppressWarnings("serial")
public class UnknownTypeOfNodeException extends Exception {

	public UnknownTypeOfNodeException(String exceptionText){
		super(exceptionText);
	}
	
	public UnknownTypeOfNodeException(){
		super("Unknown type of node");
	}
}
