/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 30. 3. 2016
 * File: UnknownElementTypeException.java
 */
@SuppressWarnings("serial")
public class UnknownElementTypeException extends Exception {

	public UnknownElementTypeException(String exceptionText){
		super(exceptionText);
	}
	
	public UnknownElementTypeException(){
		super("Unknown element type");
	}
}
