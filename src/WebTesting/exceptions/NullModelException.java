/**
 * 
 */
package WebTesting.exceptions;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 7. 7. 2017
 * File: NullModelException.java
 */
public class NullModelException extends Exception {

	public NullModelException(String exceptionText) {
		super(exceptionText);
	}
	
	public NullModelException() {
		this("Model is null");
	}
}
