/**
 * 
 */
package WebTesting.enums.scenarios;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;

//import WebTesting.Interfaces.IListItem;
//import WebTesting.model.ListItem;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Actions.java
 */
public enum Actions implements IListItem {
	assertElementPresent("location, count",	"kontrola zda se element naleza na strance"),
	assertElementNotPresent("location", "kontrola zda se element nenaleza na strance"),
	assertTextPresent("location, content", "kontrola, zda se text nenaleza na strance"),
	assertTextNotPresent("location, content", "kontrola, zda se text nenaleza na strance"),
	assertTitle("content", "Kontrola titulku"),
	clear("location", "Smaze text z daneho pole"),
	click("location", "Klik na element"),
	clickAndWait("location, wait", "Klikni a cekej urcitou dobu"),
	dropDownSelectIndex("location, content", "Volba na dropdownu"),
	dropDownSelectText("location, content", "Volba na dropdownu"),
	dropDownSelectValue("location, content", ""),
	exists("location", "rozhodnuti, zda prvek existuje nebo ne"),
	existsCount("location, count",	"Rozhodnuti, zda se tento pocet prvku naleza na strance"),
	changeHtml("location, content", "spusti na danem elementu javascript"),
	navigate("direction", ""),
	sleep("count", ""),					
	submit("location", "Odešli příkaz"),
	switchToFrame("content", "Prepnutí do framu"),
	switchToDefault("", "Prepnuti do zakladniho okna"),
	type("location, content", "vloz text"),
	typeAndSubmit("location, content", "vloz text a posli"),
	visit("url", "Jdi na adresu"),
	verifyTextPresent("content", "Zjisti zda se text naleza na strance"),
	verifyTextNotPresent("content", "");		
	
	
	//Atributy
	private final String parameters;
	private final String hint;
	
	/*--------------*
	 * Konstruktory *  
	 *--------------*/
	Actions(String parameters, String hint){
		this.parameters = parameters;
		this.hint = hint;
	}
	
	
	/*----------------*
	 * Pomocne metody *
	 *----------------*/	
	public static Actions getValue(String ret){
		for(Actions action:values()){
			if(action.name().equals(ret)){
				return action;
			}
		}
		return null;
	}
	
	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Actions action : values()) {
			retVals.add(action.getName());
		}
		return retVals;
	}
	
	public static boolean contains(String s)	{
		for(Actions action: values())
		if (UtilityClass.compareString(action.name(), s)){
			return true;
		}
		return false;
	}
	
	
	@Override
	public String getName() {
		return this.toString();
	}

	@Override
	public String getParameters() {
		return this.parameters;
	} 
	
	
	public String getHint(){
		return this.hint;
	}
}
