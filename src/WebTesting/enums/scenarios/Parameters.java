/**
 * 
 */
package WebTesting.enums.scenarios;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Parameters.java
 */
public enum Parameters implements IListItem {

	LOCATION("target"),
	URL("url"),
	COUNT("pocet prvku"),
	CONTENT("obsah - nejaka textova hodnota"),
	DIRECTION("smer"),
	WAIT("wait");
	
	private String hint;
	
	/*-------------*
	 * Konstruktor *
	 *-------------*/
	private Parameters(String hint) {
		this.hint = hint;
	}
	
	/*----------------*
	 * Pomocne metody *
	 *----------------*/
	public static boolean contains(String s)
	{
		for(Parameters param: values())
			if (UtilityClass.compareString(param.name(), s)){
				return true;
			}
		return false;
	}
	
	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Parameters params : values()) {
			retVals.add(params.getName());
		}
		return retVals;
	}
	
	
	@Override
	public String getName() {
		return this.toString();
	}

	
	@Override
	public String getParameters() {
		return null;
	} 
	
	
	public String getHint(){
		return this.hint;
	}
}
