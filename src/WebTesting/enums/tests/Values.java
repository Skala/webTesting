/**
 * 
 */
package WebTesting.enums.tests;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.enums.scenarios.Parameters;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Values.java
 */
public enum Values implements IListItem {
	back("smer pri navigaci"),
	forward("smer pri navigaci");

	private String hint;

	/*-------------*
	 * Konstruktor *
	 *-------------*/
	Values(String hint){
		this.hint = hint;
	}

	/*----------------*
	 * Pomocne metody *
	 *----------------*/
	public static boolean contains(String s)
	{
		for(Values val: values())
			if (UtilityClass.compareString(val.name(), s)) 
				return true;
		return false;
	}

	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Values val : values()) {
			retVals.add(val.getName());
		}
		return retVals;
	}

	/*-----------------------*
	 * implementovane metody *
	 *-----------------------*/

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getName()
	 */
	@Override
	public String getName() {
		return this.toString();
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getParameters()
	 */
	@Override
	public String getParameters() {
		return null;
	} 

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getHint()
	 */
	public String getHint(){
		return this.hint;
	}
}
