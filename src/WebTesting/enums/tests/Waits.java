/**
 * 
 */
package WebTesting.enums.tests;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.enums.scenarios.Parameters;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Wait.java
 */
public enum Waits implements IListItem {

	threadSleep("thread; zakladni cekani diky uspani vlakna"),
	elementToBeClickable("a"),
	presenceOfAllElementsLocatedBy("a"),
	presenceOfElementLocated("a"),
	visibilityOfAllElementsLocatedBy("a"),
	visibilityOfElementLocated("a");

	private String hint;

	/*-------------*
	 * Konstruktor *
	 *-------------*/
	Waits(String hint){
		this.hint = hint;
	}

	/*----------------*
	 * Pomocne metody *
	 *----------------*/
	public static boolean contains(String s)
	{
		for(Waits wait: values())
			if (UtilityClass.compareString(wait.name(), s)) 
				return true;
		return false;
	}

	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Waits wait : values()) {
			retVals.add(wait.getName());
		}
		return retVals;
	}

	/*-----------------------*
	 * implementovane metody *
	 *-----------------------*/

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getName()
	 */
	@Override
	public String getName() {
		return this.toString();
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getParameters()
	 */
	@Override
	public String getParameters() {
		return null;
	}


	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getHint()
	 */
	@Override
	public String getHint() {
		return this.hint;
	}


}
