/**
 * 
 */
package WebTesting.enums.tests;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.enums.scenarios.Parameters;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Tags.java
 */
public enum Tags implements IListItem {
	
	DIV("div"),
	SPAN("span"),
	IMG("img"),
	INPUT("input"),
	LABEL("label"),
	A("a");
	
	private String hint;
	
	/*-------------*
	 * Konstruktor *
	 *-------------*/
	Tags(String hint){
		this.hint = hint;
	}
	
	/*----------------*
	 * Pomocne metody *
	 *----------------*/
	public static boolean contains(String s)
	{
		for(Tags tag: values())
			if (UtilityClass.compareString(tag.name(), s)) 
			return true;
		return false;
	}
	
	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Tags tag : values()) {
			retVals.add(tag.getName());
		}
		return retVals;
	}
	
	/*-----------------------*
	 * implementovane metody *
	 *-----------------------*/

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getName()
	 */
	@Override
	public String getName() {
		return this.toString();
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getParameters()
	 */
	@Override
	public String getParameters() {
		return null;
	} 
	
	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getHint()
	 */
	public String getHint(){
		return this.hint;
	}
}
