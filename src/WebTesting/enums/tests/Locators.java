/**
 * 
 */
package WebTesting.enums.tests;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.enums.scenarios.Parameters;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Locators.java
 */
public enum Locators implements IListItem{
	ID("identifikátor"),
	CLASS("třída"),
	XPATH("xpath"),
	CSS("css selektor"),
	Link("text u odkazu"),
	PartialLink("část textu u odkazu"),
	NAME("element name"),
	TAG("tag name");

	private String hint;

	/*-------------*
	 * Konstruktor *
	 *-------------*/
	Locators(String hint) {
		this.hint = hint;
	}


	/*----------------*
	 * Pomocne metody *
	 *----------------*/
	public static boolean contains(String s)
	{
		for(Locators loc: values())
			if (UtilityClass.compareString(loc.name(), s)) 
				return true;
		return false;
	}

	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Locators loc: values()) {
			retVals.add(loc.getName());
		}
		return retVals;
	}

	/*-----------------------*
	 * implementovane metody *
	 *-----------------------*/

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getName()
	 */
	@Override
	public String getName() {
		return this.toString();
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getParameters()
	 */
	@Override
	public String getParameters() {
		return null;
	} 

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getHint()
	 */
	public String getHint(){
		return this.hint;
	}
}
