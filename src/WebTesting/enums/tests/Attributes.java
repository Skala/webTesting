/**
 * 
 */
package WebTesting.enums.tests;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.enums.scenarios.Parameters;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Attributes.java
 */
public enum Attributes implements IListItem {

	alt("alt"),
	href("href"),
	method("method"),
	name("name"),
	style("style"),
	target("target"),
	title("title"),
	type("type u formularu"),
	value("value");

	private String hint;

	/*-------------*
	 * Konstruktor *
	 *-------------*/
	private Attributes(String hint) {
		this.hint = hint;
	}


	/*----------------*
	 * Pomocne metody *
	 *----------------*/
	public static boolean contains(String s)
	{
		for(Attributes attr: values())
			if (UtilityClass.compareString(attr.name(), s)) 
				return true;
		return false;
	}

	public static List<String> getValues(){
		List<String> retVals = new ArrayList<String>();
		for (Attributes attr : values()) {
			retVals.add(attr.getName());
		}
		return retVals;
	}

	/*-----------------------*
	 * implementovane metody *
	 *-----------------------*/

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getParameters()
	 */
	@Override
	public String getParameters() {
		return null;
	} 

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getHint()
	 */
	public String getHint(){
		return this.hint;
	}

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IListItem#getName()
	 */
	public String getName(){
		return this.toString();
	}
}
