/**
 * 
 */
package WebTesting.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 21. 3. 2017
 * File: DriverList.java
 */
public enum DriverList {
	FIREFOX("Firefox", true, "C:\\Program Files\\Java\\browserDrivers\\mozilla\\geckodriver.exe", 0),
	CHROME("Chrome", true, "C:\\Program Files\\Java\\browserDrivers\\chrome\\chromedriver.exe", 0),
	OPERA("Opera", true, "C:\\Program Files\\Java\\browserDrivers\\opera\\operadriver.exe", 0),
	IE("Internet Explorer", true, "C:\\Program Files\\Java\\browserDrivers\\ie\\IEDriverServer.exe", 0),
	
	HMTL_UNIT_DRIVER();
	
	private String title;
	private boolean showBrowser = false;
	private String path;
	private int version;
	
	DriverList(String title, boolean showBrowser,  String path, int version){
		this.title = title;
		this.showBrowser = showBrowser;
		this.path = path;
		this.version = version;
	}
		
	DriverList(String title, boolean showBrowser){
		this(title, showBrowser, null, 0);
	}
	
	DriverList(String title){
		this(title, false);
	}
	
	DriverList() {
		this(null);
	}
	
	public boolean showBrower(){
		return showBrowser;
	}
	
	public String getTitle(){
		if(this.title != null){
			return this.title;
		}
		else{
			return this.toString();
		}
	}
	
	public String getPath(){
		return this.path;
	}
	
	public int getVersion(){
		return this.version;
	}
	
	/**
	 * @return
	 */
	public static String[] getTitles() {
		List<String> list = new ArrayList<String>();
		
		for (DriverList  item : values()) {
			if(item.showBrowser){
				list.add(item.getTitle());
			}
		}
				
		String [] rets = new String[list.size()];
		for(int i = 0 ; i < rets.length; i++){
			rets[i] = list.get(i);
		}
		return rets;
	}
	
	public static DriverList getEnum(String title){
		for (DriverList item : values()) {
			if(item.getTitle().equals(title)){
				return item;
			}
		}
		return null;
	}
}
