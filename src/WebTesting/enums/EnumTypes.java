/**
 * 
 */
package WebTesting.enums;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 3. 3. 2017
 * File: ListTypes.java
 */
public enum EnumTypes {

	ACTION, PARAMETER, ATTRIBUTE, LOCATOR, TAG, VALUE, WAIT;
}
