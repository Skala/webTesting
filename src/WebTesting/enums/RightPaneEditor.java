/**
 * 
 */
package WebTesting.enums;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: RightPaneEditor.java
 */
public enum RightPaneEditor {

	
	EDITOR_TEST("editor_test", 0),
	EDITOR_SCENARIO("editor_scenario", 1),
	EDITOR_PROCEED("editor_proceed", 2),
	TEST_DETAIL("test_detail", 3),
	COMMAND_DETAIL("command_detail", 4);

	
	private String title;
	private int index;
	
	private RightPaneEditor(String title, int index){
		this.title = title;
		this.index = index;
	}
	
	public String getTitle() {
		return title;
	}
	public int getIndex() {
		return index;
	}
}
