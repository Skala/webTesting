/**
 * 
 */
package WebTesting.enums;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: Controllers.java
 */
public enum Controllers {

	MAIN_WINDOW_CTRL("main_window_ctrl", 0),
	MAIN_MENU_CTRL("main_menu_ctrl", 1),
	
	TREE_PANEL_CTRL("tree_panel_ctrl", 2),
	TAB_TEST_CTRL("tab_test_ctrl", 3),
	TAB_SCENARIO_CTRL("tab_scenario_ctrl", 4),
	TAB_PROCEED_CTRL("tab_proceed_ctrl", 5),
	
	RIGHT_PANEL_CTRL("right_panel_ctrl", 6),
	EDITOR_TEST_CTRL("editor_test_ctrl", 7),
	EDITOR_SCENARIO_CTRL("editor_scenario_ctrl", 8),
	EDITOR_PROCEED_CTRL("editor_proceed_ctrl", 9),
	TEST_DETAIL_CTRL("test_detail_ctrl", 10),
	TEST_DETAIL_COMMAND_CTRL("test_detail_command_ctrl", 11),
	
	TEST_UPPER_PANEL_CTRL("test_upper_panel_ctrl", 12),
	TEST_MIDDLE_PANEL_CTRL("test_middle_panel_ctrl", 13),
	TEST_LOWER_PANEL_CTRL("test_lower_panel_ctrl", 14),
	TEST_LIST_PANEL_CTRL("test_list_panel_ctrl", 15),
	
	EDITOR_TEST_SOURCE_CTRL("editor_test_source_ctrl", 16),
	EDITOR_TEST_DESIGN_CTRL("editor_test_design_ctrl", 17),
	
	MODIFY_TEST_SOURCE_CTRL("modify_test_source_ctrl", 18),
	
	SCENARIO_LOWER_PANEL_CTRL("scenario_lower_panel_ctrl", 19),
	SCENARIO_LIST_PANEL_CTRL("scenario_list_panel_ctrl", 20),
	SCENARIO_UPPER_PANEL_CTRL("scenario_upper_panel_ctrl", 21),
	
	PROCEED_UPPER_PANEL_CTRL("proceed_upper_panel_ctrl", 22),
//	PROCEED_LIST_CTRL("proceed_list_ctrl", 23);
	DETAIL_UPPER_PANEL_CTRL("detail_upper_panel_ctrl", 23);
	
	
	
	private String title;
	private int index;
	
	private Controllers(String title, int index){
		this.title = title;
		this.index = index;
	}
	
	public String getTitle() {
		return title;
	}
	public int getIndex() {
		return index;
	}
	
}
