/**
 * 
 */
package WebTesting.core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: Config.java
 */
public class Config {

	
	/**
	 * Paths to source xml files 
	 */
	public static final String PATH_TO_TESTS;
	public static final String PATH_TO_SCENARIOS;
	
	public static final String MAIN_WINDOW_TITLE = "CSWebTest";
	public static final Dimension SCREEN_RESOLUTION = Toolkit.getDefaultToolkit().getScreenSize();
	public static final Dimension DIMENSION_OF_APP = new Dimension(920, 480);
	
	/**
	 * Basic colours, which are using across the aplication
	 */
	public static final Color COLOR_GREEN = new Color(150, 255, 150);
	public static final Color COLOR_RED = new Color(255, 150, 150);
	public static final Color COLOR_BLUE = new Color(150, 150, 255);
	public static final Color COLOR_WHITE = new Color(240, 240, 240);
	
	/**
	 * Control charakterech, which are used in parser mainly
	 */
	public static final String endOfCommand = ";";
	public static final String subCommandSeparator = ",";
	public static final String constantTag = "##"; 
	public static final String lineSeparator = System.lineSeparator();
	
	/**
	 * Attribute with current locale
	 */
	public static String locale;
	
	
	public static final String TYPE_TEST = "test";
	public static final String TYPE_SCENARIO = "scenario";
	public static final String STATUS_ERROR = "chyba";
	public static final String STATUS_SUCCESS = "OK";
	public static final String STATUS_RUNNING = "Probihajici";
	public static final String TEXT_SUCCES = "V poradku";
	
	public static final String STATUS = "status";
	public static final String EXCEPTION = "exception";
	public static final String MESSAGE = "message";
	
	public static final String COMMAND = "command";
	
	public static final String LOCATION = "location";
	public static final String CONTENT = "content";
	public static final String URL = "url";
	public static final String COUNT = "count";
	public static final String VARIABLE = "variable";
	public static final String WAIT = "wait";
	public static final String DIRECTION = "direction";
	
	public static final String ACTION = "action";
	public static final String TAG = "tag";
	public static final String ATTRIBUTE = "attribute";
	public static final String PARAMETER = "parameter";
	public static final String VALUE = "value";
	
	public static final String TYPE = "type";
	public static final String LABEL= "label";

	public static final String BACK = "back";
	public static final String FORWARD = "forward";
	
	public static final String SLEEP = "sleep";
	
	static{
		String separator = File.separator;
		PATH_TO_TESTS = System.getProperty("user.dir") + separator + "files" + separator + "tests" ;
		PATH_TO_SCENARIOS = System.getProperty("user.dir") + separator + "files" + separator + "scenarios";	
	}
	
	
}
