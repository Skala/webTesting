/**
 * 
 */
package WebTesting.core.drivers.selenium;

import static WebTesting.controller.UtilityClass.findElementInEnumFromParent;
import static WebTesting.core.Config.ATTRIBUTE;
import static WebTesting.core.Config.BACK;
import static WebTesting.core.Config.CONTENT;
import static WebTesting.core.Config.COUNT;
import static WebTesting.core.Config.DIRECTION;
import static WebTesting.core.Config.FORWARD;
import static WebTesting.core.Config.LOCATION;
import static WebTesting.core.Config.URL;
import static WebTesting.core.Config.WAIT;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdom2.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.BrowserVersion;

import WebTesting.Interfaces.IListItem;
import WebTesting.Interfaces.IMyDriver;
import WebTesting.controller.UtilityClass;
import WebTesting.core.drivers.selenium.subDrivers.MyChromeDriver;
import WebTesting.core.drivers.selenium.subDrivers.MyFirefoxDriver;
import WebTesting.core.drivers.selenium.subDrivers.MyHtmlUnitDriver;
import WebTesting.core.drivers.selenium.subDrivers.MyIEDriver;
import WebTesting.core.drivers.selenium.subDrivers.MyOperaDriver;
import WebTesting.enums.Controllers;
import WebTesting.enums.DriverList;
import WebTesting.enums.EnumTypes;
import WebTesting.enums.tests.Waits;
import WebTesting.exceptions.LessElementsFoundException;
import WebTesting.exceptions.NoElementsFoundException;
import WebTesting.exceptions.NoInputStringException;
import WebTesting.exceptions.UnexpectedValueException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownTypeOfLocator;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.proceed.ControlProceedUpperPanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 21. 3. 2017
 * File: MyDriver.java
 */
public class MySelenium implements IMyDriver {

	protected WebDriver driver;

	protected WebElement element;
	protected List<WebElement> elements;

	protected int timeout;


	/**
	 * Konstruktor, ktery nastavi driver
	 */
	public MySelenium(){
		setDriver();
	}

	public void setDriver() {
		
		//TODO doimplementovat moznost vytvoreni driveru pro ostatni prohlizece
		ControlProceedUpperPanel controlPanel = (ControlProceedUpperPanel) MainWindow.getController(Controllers.PROCEED_UPPER_PANEL_CTRL).getView();
		
		if(controlPanel.showInBrowser()){

			switch(DriverList.getEnum(controlPanel.getDriverComboItem())){
			case FIREFOX:
				this.driver = new MyFirefoxDriver();
				break;
			case IE:
				this.driver = new MyIEDriver();
				break;
			case OPERA:
				this.driver = new MyOperaDriver();
				break;
			case CHROME:
				this.driver = new MyChromeDriver();
				break;
			default:
				this.driver = new MyFirefoxDriver();
				break;
			}
			this.driver.manage().window().maximize();
		}
		else{
			this.driver = new MyHtmlUnitDriver(BrowserVersion.FIREFOX_45, true); 
		}

		this.timeout = 10;
		this.driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		this.driver.manage().timeouts().pageLoadTimeout(2*timeout, TimeUnit.SECONDS);

		this.element = null;
		this.elements = new ArrayList<WebElement>();
	}

	/** Metoda, ktera zajisti prechod na zadanou adresu
	 * @param url Adresa, na kterou se ma prejit
	 * @throws Exception 
	 */
	public void visit(String url) throws Exception{
		System.out.println(url);
		driver.get(url);
	}

	/** Metoda, ktera plni stejnou funkcionalitu jako find(By locator) s tim rozdilem, ze vraci List elementu, ktere vyhovuji danemu lokatoru
	 * @param locator Umisteni hledanych elementu
	 * @return List hledanych elementu
	 * @throws Exception 
	 */
	public List<WebElement> finds(By locator, Element currentCommand) throws Exception{

		this.elements = new ArrayList<WebElement>();

		Element wait = currentCommand.getChild(WAIT); 
		if(wait != null){
			wait = UtilityClass.findElementInEnumFromParent(EnumTypes.WAIT, wait); //threadSleep, elementpresent... 
			if(wait != null){
				String waitType = wait.getName();
				if(waitType != null){
					String waitTime = "";
					try {
						waitTime = wait.getTextTrim();
						if(!UtilityClass.isNullOrEmpty(waitTime)){

							int waitTimeInt = Integer.valueOf(waitTime);
							if(!waitType.equals("threadSleep")){
								waitTimeInt = (int) waitTimeInt/1000;
							}

							WebDriverWait driverWait = new WebDriverWait(driver, waitTimeInt);	

							switch(waitType){
							case "threadSleep": //toto cekani se provadi az na konci prikazu, tak jej zde preskocime
								System.err.println(waitType + ": " + waitTimeInt);
								Thread.sleep(waitTimeInt);
								this.elements = driver.findElements(locator);
								break;
							case "elementToBeClickable":
								System.err.println(waitType + ": " + waitTimeInt);
								this.element = driverWait.until(ExpectedConditions.elementToBeClickable(locator));
								this.elements.add(element);
								break;
							case "presenceOfAllElementsLocatedBy":
								System.err.println(waitType + ": " + waitTimeInt);
								this.elements = driverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
								break;
							case "presenceOfElementLocated":
								System.err.println(waitType + ": " + waitTimeInt);
								this.element = driverWait.until(ExpectedConditions.presenceOfElementLocated(locator));
								this.elements.add(element);
								break;
							case "visibilityOfAllElementsLocatedBy":
								System.err.println(waitType + ": " + waitTimeInt);
								this.elements = driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
								System.out.println("dddf");
								break;
							case "visibilityOfElementLocated":
								System.err.println(waitType + ": " + waitTimeInt);
								this.element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
								this.elements.add(element);
								break;
							default:
								System.err.println("jedu defaultnim cekaci vetvi");
								break;
							}
							return this.elements;
						}
					}catch (NumberFormatException e) {
						throw new NumberFormatException("Je pozadovana ciselna hodnota. Zadano: " + waitTime);
					}catch(TimeoutException e){
						throw new TimeoutException("Pozadovany prvek nebyl po exlicitnim cekani nalezen");
					}
				}
				else{
					throw new UniverzalException("Doba cekani nebyla nastavena");
				}
			}
		}
		elements = driver.findElements(locator);
		return elements;

	}	

	/**	Metoda, ktera smaze text z elementu, ktery je udany v parametru
	 * @param element Element, jehoz obsah chceme vymazat
	 */
	public void clear(WebElement element){
		element.clear();
	}

	/** Metoda, ktera simuluje kliknuti na dany element
	 * @param element Element, nad kterym chceme provest operaci kliknuti
	 */
	public void click(WebElement element){
		element.click();
	}

	/** Metoda, ktera simuluje akci vklani textu
	 * @param text Text, ktery chceme zadat do elementu
	 * @param element Element, nad kterym provadime akci zapisu
	 */
	public void type(String text, WebElement element){
		element.clear();
		element.sendKeys(text);
	}

	/**	Metoda, ktera plni funkci odeslani formulare
	 * @param element Formular, jehoz obsah chceme poslat na vyhodnoceni
	 */
	public void submit(WebElement element){
		element.submit();
	}

	/**	Metoda, ktera zapise do elementu z parametru text, ktery se uveden v prvni parametru a nasledne odesle formular
	 * @param text Text, ktery vkladame do elementu
	 * @param element Element, nad kterym provadime vkladani textu a nasledne jeho odeslani
	 */
	public void typeAndSubmit(String text, WebElement element){
		type(text, element);
		element.submit();
	}

	public void setHtmlAtribute(WebElement element, String attr, String value){

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, attr, value);
	}


	public void close() { 
		this.driver.close();
	}

	public void quit() {
		this.driver.quit();
	}

	public String getCurrentUrl(){ 
		return this.driver.getCurrentUrl(); 
	}

	public String getTitle(){ 
		return this.driver.getTitle(); 
	}

	public void switchTo(WebElement element){
		this.driver.switchTo().frame(element);
	}

	public boolean verifyTextPresent(String text){
		return this.driver.getPageSource().contains(text);
	}


	/*---------------------------------
 	Metody, ktere definuji typ akce
 ---------------------------------*/

	/** Metoda, ktera urci, zda se dany element naleza na strance
	 * @throws Exception
	 */
	public void assertElements(Element currentCommand, String currentAction) throws Exception{

		this.elements = getElementsByLocator(currentCommand);
		int cnt;
		Element count;
		try{
			count = getSubElement(COUNT, currentCommand);
			count = getSubElement(CONTENT, count);

			try{
				cnt = Integer.parseInt(getTextFromElement(count));
			}catch(NumberFormatException e){
				throw new NullPointerException("Je ocekavana ciselna hodnota");
			}
		}catch(Exception e){
			cnt = 1;
		}


		if(currentAction.equals("assertelementnotpresent")){
			if(cnt == 0){
				return;
			}
			else{
				throw new UniverzalException("Prvek byl na strance nalezen, ale nemel byt");
			}
		}
		else{
			assertElementsPresent(elements, cnt);
		}
	}

	/** Metoda, ktera zjistuje zda se element, nebo pocet elementu naleza na strance
	 * @param elements Pole s vyhledanymi elementy
	 * @param cnt Pocet, ktery jsme chteli vyhledat
	 * @throws LessElementsFoundException 
	 * @throws NoElementsFoundException 
	 * @throws Exception 
	 */
	public void assertElementsPresent(List<WebElement> elements, int cnt) throws NoElementsFoundException, LessElementsFoundException{
		//elements = getElementsByLocator(command);
		getAmountElements(elements, cnt);
	}

	/** Metoda, ktera testuje zda se na uvedene pozici naleza pozadovany text
	 * @throws Exception
	 */
	public  void assertText(Element currentCommand, String currentAction) throws Exception {

		elements = getElementsByLocator(currentCommand);
		

		Element value = getSubElement(CONTENT, currentCommand);
		value = getSubElement(CONTENT, value);
		String expectedText = value.getTextTrim();

		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1);
		String text =  getTextFromWebElement(elements.get(0));


		if(currentAction.equals("asserttextpresent")){
			//assertTextPresent(text, expectedText);
			if(!text.contains(expectedText)){
				throw new UniverzalException("Text nebyl nalezen: " + expectedText + ", nalezeno: " + text);
			}
		}
		else{
			//assertTextNotPresent(text, expectedText);
			if(text.contains(expectedText)){
				throw new UniverzalException("Text byl na strance nalezen: " + expectedText);
			}
		}
	}


	/**
	 * @param expectedText
	 * @throws UniverzalException 
	 * @throws LessElementsFoundException 
	 * @throws NoElementsFoundException 
	 */
	public void assertTextPresent(String text, String expectedText) throws UniverzalException, NoElementsFoundException, LessElementsFoundException {


		if(!text.contains(expectedText)){
			throw new UniverzalException("Text nebyl nalezen: " + expectedText + ", nalezeno: " + text);
		}
		if(elements == null){
			boolean contain = verifyTextPresent(expectedText);
			if(!contain){
				throw new UniverzalException("Text se na strance nevyskytuje: " + expectedText);
			}
		}
		else{
			elements = getAmountElements(elements, 1);
			if(!expectedText.equals(elements.get(0).getText())){
				throw new UniverzalException("Text nebyl nalezen: " + expectedText + ", nalezeno: " + elements.get(0).getText());
			}
		}
	}


	///**
	// * @param textValue
	// * @throws UniverzalException 
	// * @throws LessElementsFoundException 
	// * @throws NoElementsFoundException 
	// */
	//private void assertTextNotPresent(String text, String expectedText) throws UniverzalException, NoElementsFoundException, LessElementsFoundException {
	//
	//	if(!text.contains(expectedText)){
	//		throw new UniverzalException("Text byl na strance nalezen: " + expectedText);
	//	}
	//	if(elements == null){
	//		boolean contain = verifyTextPresent(textValue);
	//		if(contain){
	//			throw new UniverzalException("Text se na strance vyskytuje: " + textValue);
	//		}
	//	}
	//	else{
	//		elements = getAmountElements(elements, 1);
	//		if(textValue.equals(elements.get(0).getText())){
	//			throw new UniverzalException("Text byl na strance nalezen: " + textValue);
	//		}
	//	}
	//}

	/** Metoda, ktera testuje, zda se pozadovany text naleza v titulce stranky
	 * @throws UnexpectedValueException
	 * @throws NoElementsFoundException
	 * @throws NoInputStringException
	 * @throws UniverzalException
	 * @throws InterruptedException
	 */
	public void assertTitle(Element currentCommand) throws UnexpectedValueException, NoElementsFoundException, NoInputStringException, UniverzalException, InterruptedException {

		Element value = getSubElement(CONTENT, currentCommand);
		value = getSubElement(CONTENT, value);
		String textValue = getTextFromElement(value);

		String title = getTitle();
		if(!title.equals(textValue)){
			throw new UniverzalException("Titulek stranky se neshoduje se zadanym textem. Hledany: " + textValue + ", skutecny: "+ title);
		}
	}

	/** Metoda, ktera vymaze obsah vstupniho pole na strance
	 * @throws Exception
	 */
	public void clear(Element currentCommand) throws Exception {
		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1);
		clear(elements.get(0));
	}

	/** Metoda, ktera provede akci kliknuti na element na strance pod danym lokatorem
	 * @throws Exception
	 */
	public void click(Element currentCommand) throws Exception {
		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1); 
		click(elements.get(0));
	}

	/** Metoda, ktera na html strance provede akci nad danym rozbalovacim seznamem
	 * @throws Exception
	 */
	public void dropDown(Element currentCommand, String currentAction) throws Exception{
		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1);

		Select select = new Select(elements.get(0));
		Element content = getSubElement(CONTENT, currentCommand);
		content = getSubElement(CONTENT, content);
		String value = getTextFromElement(content);

		if(currentAction.equals("dropdownselecttext")){
			select.selectByVisibleText(value);
		}
		else if(currentAction.equals("dropdownselectvalue")){
			select.selectByValue(value);
		}
		else{
			try {
				int index = Integer.valueOf(value);
				select.selectByIndex(index);
			} catch (NumberFormatException e) {
				throw new NumberFormatException("Prikaz vyzaduje ciselnou hodnotu");
			}
		}		
	}

	/** Metoda, ktera zjistuje, zda se na strance vyskytuje pod danym lokatorem nami pozadovany pocet elementu
	 * @throws Exception
	 */
	public void exists(Element currentCommand) throws Exception {

		elements = getElementsByLocator(currentCommand);
		//vychozi hodnota, pokud uzivatel neurcil, kolik prvku pozaduje na strance, pak je pocet nastaven na 1
		int cnt = 0;

		try {
			Element count = getSubElement(COUNT, currentCommand);
			count = getSubElement(CONTENT, count);
			if(getTextFromElement(count) != null){
				String nr = getTextFromElement(count);
				try{
					cnt = Integer.valueOf(nr);
				}catch(NumberFormatException e){
					throw new NumberFormatException("v paramteru " + COUNT + " je pozadavana ciselna hodnota. Nalezena hodnota: " + nr);
				}
			}
		} catch (UnexpectedValueException e) {
			cnt = 1;
		}
		elements = getAmountElements(elements, cnt);
	}

	/** Metoda slouzici pro modifikaci atributu u elementu danym lokatorem.
	 * @throws Exception
	 */
	public void changeHtml(Element currentCommand) throws Exception {

		elements = getElementsByLocator(currentCommand);

		Element value = getSubElement(CONTENT, currentCommand);
		value = findElementInEnumFromParent(EnumTypes.ATTRIBUTE, value);
		String attrValue = value.getTextTrim();

		elements = getAmountElements(elements, 1);
		setHtmlAtribute(elements.get(0), value.getName(), attrValue);
	}

	public void mouseHover(Element currentCommand) throws Exception{
		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1);

		Actions action = new Actions(this.driver);
		action.moveToElement(elements.get(0)).click().build().perform();
	}

	/** Metoda, ktera slouzi pro navigaci na strance ve smyslu prechodu zpet nebo vpred v historii prohlizece
	 * @throws UnexpectedValueException
	 * @throws NoElementsFoundException
	 * @throws NoInputStringException
	 * @throws UniverzalException
	 */
	public void navigate(Element currentCommand) throws UnexpectedValueException, NoElementsFoundException, NoInputStringException, UniverzalException{
		Element direction = getSubElement(DIRECTION, currentCommand);
		direction = getSubElement(CONTENT, direction);
		String dir = getTextFromElement(direction);
		switch(dir){
		case FORWARD:
			this.driver.navigate().forward();
			break;
		case BACK:
			this.driver.navigate().back();
			break;
		default:
			throw new UniverzalException("Uvedeny smer neexistuje: " + dir);
		}
	}

	/** Metoda, ktera slouzi k uspani vyhodnocovaciho mechanismu.
	 * @throws Exception
	 */
	public void sleep(Element currentCommand) throws Exception {
		Element sleep = getSubElement(COUNT, currentCommand);
		sleep = getSubElement(CONTENT, sleep);
		int sleepTime = getIntFromElement(sleep);
		Thread.sleep(sleepTime);
	}

	/** Metoda, ktera umoznuje odeslani vyplnovaneho formulare
	 * @throws Exception
	 */
	public void submit(Element currentCommand) throws Exception{
		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1);
		submit(elements.get(0));
	}

	/** Metoda, ktera poskytuje prechod do ramu uvnitr html stranky
	 * @throws Exception
	 */
	public void switchToFrame(Element currentCommand) throws Exception {
		elements = getElementsByLocator(currentCommand);
		elements = getAmountElements(elements, 1); 
		switchTo(elements.get(0));
	}

	/** Metoda, ktera se vraci zpet z ramu do zakladni html stranky
	 * 
	 */
	public void switchToDefault() {
		this.driver.switchTo().defaultContent();
	}

	/**	Metoda, ktera provede akci pro vlozeni textu do webove stranky. Poskytuje moznost odeslani formulare, pokud je v atributu 'action' definovano typeAndSubmit
	 * @throws Exception
	 */
	public void type(Element currentCommand, String currentAction) throws Exception {

		elements = getElementsByLocator(currentCommand);

		Element value = getSubElement(CONTENT, currentCommand);
		value = getSubElement(CONTENT, value);
		String text = getTextFromElement(value);

		elements = getAmountElements(elements, 1);


		boolean submit = ((currentAction.toLowerCase()).equals("type")) ? false : true;
		if(submit){
			typeAndSubmit(text, elements.get(0));
		}
		else{
			type(text, elements.get(0));
		}
	}

	/**
	 * Metoda, ktera provede prechod na danou url adresu
	 * @throws UnexpectedValueException 
	 * @throws NoInputStringException 
	 * @throws NoElementsFoundException 
	 * @throws Exception 
	 */
	public void visit(Element currentCommand) throws Exception {
		Element url = getSubElement(URL, currentCommand);
		Element value = getSubElement(CONTENT, url);
		String retUrl = getTextFromElement(value);
		visit(retUrl);	
	}

	/** Metoda, ktera urci, zda se zadany text naleza na strance nebo ne
	 * @throws UnexpectedValueException
	 * @throws UniverzalException
	 * @throws NoElementsFoundException
	 * @throws NoInputStringException
	 */
	public void verifyText(Element currentCommand, String currentAction) throws UnexpectedValueException, UniverzalException, NoElementsFoundException, NoInputStringException{
		Element content = getSubElement(CONTENT, currentCommand);
		content = getSubElement(CONTENT, content);
		String text = getTextFromElement(content);

		boolean contain = verifyTextPresent(text);

		if(currentAction.equals("verifytextpresent")){
			if(contain){
				return;
			}
			else{
				throw new UniverzalException("Text se na strance nevyskytuje");
			}
		}
		else{
			if(!contain){
				return;
			}
			else{
				throw new UniverzalException("Text se na strance vyskytuje, ale nemel by");
			}
		}
	}


	/*-------------------------------
	Pomocne metody pro ziskani urcitych dat nebo informaci
 -------------------------------*/

	/** Metoda, ktera zjisti, zda se pozadovany element naleza v jeho rodici
	 * @param subElement SubElement ktery hledame
	 * @param element Rodicovsky element
	 * @return Potomka rodicovskeho elementu
	 * @throws UnexpectedValueException 
	 */
	private Element getSubElement(String subElement, Element element) throws UnexpectedValueException{
		Element subEl = element.getChild(subElement);

		if(subEl != null){
			return subEl;
		}
		else{
			throw new UnexpectedValueException("Element neobsahuje hledaneho potomka: " + subElement);
		}
	}

	/** Metoda, ktera z konkretniho elementu vyextrahuje jeho textovy obsah
	 * @param element Element, ze ktereho se pokusime ziskat text
	 * @return Textovy retezec, ktery byl ulozen v parametru
	 * @throws NoElementsFoundException V pripade, ze element neexistuje
	 * @throws NoInputStringException V pripade, ze pozadovany element existuje, ale nema nastaven text 
	 */
	private String getTextFromElement(Element element) throws NoElementsFoundException, NoInputStringException{
		if(element != null){
			String text = element.getTextTrim();
			if(text != null){
				return text;
			}
			else{
				throw new NoInputStringException("Zadany element existuje, ale neobsahuje zadny text");
			}
		}
		else{
			throw new NoElementsFoundException("Element z parametru nelze nalezt");
		}
	}

	/** Metoda, ktera z daneho elementu vyextrahuje podelement, ktery by mel obsahoat ciselnou hodnotu
	 * @param element Element, ze ktereho se snazime vyextrahovat ciselnou hodnotu
	 * @return
	 * @throws NoInputStringException
	 * @throws NoElementsFoundException
	 */
	private int getIntFromElement(Element element) throws NoInputStringException, NoElementsFoundException{
		if(element != null){
			String text = element.getTextTrim();
			if(text != null){
				try {
					int textInt = Integer.parseInt(text);
					return textInt;
				} catch (NumberFormatException e) {
					throw new NumberFormatException("Prikaz vyzaduje ciselnou hodnotu");
				}
			}
			else{
				throw new NoInputStringException("Zadany element existuje, ale neobsahuje zadny text");
			}
		}
		else{
			throw new NoElementsFoundException("Element z parametru nelze nalezt");
		}
	}

	/** Metoda, ktera vyextrahuje text z atributu XML elementu
	 * @param command XML element
	 * @param attribute Atribut XML elementu
	 * @return Hodnota z tohoto atributu
	 * @throws UniverzalException
	 */
	private String getTextFromAttribute(Element command, String attribute) throws NullPointerException{
		if(command != null){
			String value = command.getAttributeValue(attribute);
			if(value != null){
				return value;
			}
			else{
				throw new NullPointerException("Prikaz neobsahuje atribut \"" + attribute + "\"");
			}
		}
		else{
			throw new NullPointerException("Uvedeny prikaz neexistuje");
		}
	}

	/** Metoda, ktera ziska ze WebElementu z parametru text, ktery se v nem nachazi
	 * @param element Element, ze ktereho pozadujeme text
	 * @return Text, ktery byl uvnitr elementu
	 * @throws Exception
	 */
	private String getTextFromWebElement(WebElement element) throws Exception{
		String text = element.getText().trim();
		return text;
	}

	/** Metoda, ktera vrati seznam webElementu, ktere vyhovuji vsem lokacnim pozadavkum v ramci jednoho prikazu.
	 * @param command Element, ktery by mel obsahovat potomka urcujici pozici daneho prvku na html strance
	 * @return Seznam elementu, ktere se na dane pozici nalezaji
	 * @throws Exception
	 */
	private List<WebElement> getElementsByLocator(Element command) throws Exception { //command


		Element loc = getSubElement(LOCATION, command);
		loc = findElementInEnumFromParent(EnumTypes.LOCATOR, loc); //element(name = q

		//TODO asi funkcni
		List<WebElement> list = finds(getLocator(loc.getName(), getTextFromElement(loc)), command);

		if(list == null){
			list = new ArrayList<WebElement>();
		}
		return list;
	}

	/** Metoda, ktera vrati nami pozadovany pocet Elementu. Pokud tam tento pocet neni tak dojde k vyvolani vyjimky.
	 * @param elements Elementy, ktere se podarilo najit
	 * @param amount Mnozstvi, ktere pozadujeme
	 * @return Seznam elementu, ktere pozadujeme
	 * @throws NoElementsFoundException 
	 * @throws LessElementsFoundException
	 */
	private List<WebElement> getAmountElements(List<WebElement> elements, int amount) throws NoElementsFoundException, LessElementsFoundException{

		List<WebElement> els = new ArrayList<WebElement>();

		if(amount == 0){
			throw new NoElementsFoundException("Nebyl nalezen zadny prvek");
		}

		if(elements.size() > 0 && amount == 1){
			els.add(elements.get(0));
		}	
		else if(elements.size() < amount){
			if(elements.size() == 0){
				throw new NoElementsFoundException("Nebyl nalezen zadny prvek");
			}
			else{
				throw new LessElementsFoundException("nalezeno: " + elements.size() + ", pozadovano: " + amount);
			}
		}
		else if(elements.size() >= amount){
			for(int i = 0 ; i < amount ; i++){
				els.add(elements.get(i));
			}
		}
		return els;
	}

	/** Metoda, ktera vrati lokacni mechanismus By na zaklade nazvu lokatoru a jeho hodnoty
	 * @param name Typ lokatoru
	 * @param value Hodnota lokatoru
	 * @return Instanci mechanismu By s lokaci weboveho elementu
	 * @throws UnknownTypeOfLocator 
	 */
	private By getLocator(String name, String value) throws UnknownTypeOfLocator {

		switch( name.toLowerCase() ){
		case "id":
			return By.id(value);
		case "class":
			return By.className(value);
		case "css":
			return By.cssSelector(value);
		case "link":
			return By.linkText(value);
		case "partiallink":
			return By.partialLinkText(value);
		case "name":
			return By.name(value);
		case "url":
			return By.id(value);
		case "xpath":
			return By.xpath(value);
		case "tag":
			return By.tagName(value);
		default:
			throw new UnknownTypeOfLocator();
		}
	}
}
