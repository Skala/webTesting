package WebTesting.core.drivers.selenium.subDrivers;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;
import org.w3c.css.sac.ErrorHandler;

import com.gargoylesoftware.htmlunit.BrowserVersion;

/**	Trida, ktera predstavuje headless Browser pro selenium WebDriver. 
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 2. 2016
 * File: SilentHtmlUnitDriver.java
 */
public class MyHtmlUnitDriver extends HtmlUnitDriver {
	
	static{}
	
	/*--------------------
	 	Konstruktory
	---------------------*/
	public MyHtmlUnitDriver() {
		super();
		setWarningHandler();
	}

	public MyHtmlUnitDriver(BrowserVersion version) {
		super(version);
		setWarningHandler();
	}
	
	
	public MyHtmlUnitDriver(BrowserVersion version, boolean enableJavascript) {
		super(version, enableJavascript);
		this.getWebClient().waitForBackgroundJavaScript(10000);
		setWarningHandler();
	}
	
	
	public MyHtmlUnitDriver(Capabilities capabilities) {
		super(capabilities);
	}

	/*------------------------
	 	Metody
	 -------------------------*/
	/**
	 * Metoda, ktera potlacuje vypisovani informativnich logu do prikazove radky
	 */
	private void setWarningHandler() {
		
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);
		
		//this.getWebClient().getOptions().setJavaScriptEnabled(false);
		this.getWebClient().getOptions().setThrowExceptionOnScriptError(false);
		//this.getWebClient().getOptions().setJavaScriptEnabled(true);
		
		this.getWebClient().setCssErrorHandler(new ErrorHandler() {

			@Override
			public void warning(CSSParseException arg0) throws CSSException {
				
			}

			@Override
			public void fatalError(CSSParseException arg0) throws CSSException {
				
			}

			@Override
			public void error(CSSParseException arg0) throws CSSException {
				
			}
		});
	}
}


