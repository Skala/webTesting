/**
 * 
 */
package WebTesting.core.drivers.selenium.subDrivers;

import org.openqa.selenium.ie.InternetExplorerDriver;

import WebTesting.enums.DriverList;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 31. 3. 2017
 * File: MyIEDriver.java
 */
public class MyIEDriver extends InternetExplorerDriver {

	static{
		System.setProperty("webdriver.ie.driver", DriverList.IE.getPath() );
	}
	
	public MyIEDriver(){
		
	}
}
