/**
 * 
 */
package WebTesting.core.drivers.selenium.subDrivers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 21. 3. 2016
 * File: MyEventListener.java
 */
public class MyEventListener extends AbstractWebDriverEventListener{

	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.events.AbstractWebDriverEventListener#afterFindBy(org.openqa.selenium.By, org.openqa.selenium.WebElement, org.openqa.selenium.WebDriver)
	 */
	@Override
	public void afterFindBy(By by, WebElement element, WebDriver driver) {
		System.out.println("   Event listener: finded");
	}
	
	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.events.AbstractWebDriverEventListener#afterClickOn(org.openqa.selenium.WebElement, org.openqa.selenium.WebDriver)
	 */
	@Override
	public void afterClickOn(WebElement element, WebDriver driver) {
		System.out.println("   Event listener: clicked");
	}
	
	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.events.AbstractWebDriverEventListener#afterNavigateTo(java.lang.String, org.openqa.selenium.WebDriver)
	 */
	@Override
	public void afterNavigateTo(String url, WebDriver driver) {
		System.out.println("   Event listener: GOTO: " + url);
	}
}
