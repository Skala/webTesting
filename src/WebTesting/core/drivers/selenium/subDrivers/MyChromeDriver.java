/**
 * 
 */
package WebTesting.core.drivers.selenium.subDrivers;

import org.openqa.selenium.chrome.ChromeDriver;

import WebTesting.enums.DriverList;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 31. 3. 2017
 * File: MyChromeDriver.java
 */
public class MyChromeDriver extends ChromeDriver {

	static{
		System.setProperty("webdriver.chrome.driver", DriverList.CHROME.getPath());
	}
	
	public MyChromeDriver(){}
}
