/**
 * 
 */
package WebTesting.core.drivers.selenium.subDrivers;



import WebTesting.enums.DriverList;


import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;


/** Trida, ktera tvori zaklad vyhodnocovaciho mechanismu a poskytuje funkcionalitu Selenia WebDriver.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2016
 * File: Base.java
 */
public class MyFirefoxDriver extends FirefoxDriver {
	
	
	static{
		System.setProperty("webdriver.gecko.driver", DriverList.FIREFOX.getPath());	
	}
	
	public MyFirefoxDriver(){
		
		super(new FirefoxOptions()
			       // For example purposes only
			      .setProfile(new FirefoxProfile())
			      .addTo(DesiredCapabilities.firefox()));
	}
}
