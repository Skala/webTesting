/**
 * 
 */
package WebTesting.core.drivers.selenium.subDrivers;

import org.openqa.selenium.opera.OperaDriver;

import WebTesting.enums.DriverList;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 31. 3. 2017
 * File: MyOperaDriver.java
 */
public class MyOperaDriver extends OperaDriver {

	static{
		System.setProperty("webdriver.opera.driver", DriverList.OPERA.getPath());
	}
	
	public MyOperaDriver(){}
}
