package WebTesting.core;

import static WebTesting.core.Config.COMMAND;
import static WebTesting.core.Config.STATUS_ERROR;
import static WebTesting.core.Config.STATUS_RUNNING;
import static WebTesting.core.Config.STATUS_SUCCESS;
import static WebTesting.core.Config.TEXT_SUCCES;

import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Element;

import WebTesting.Interfaces.IMyDriver;
import WebTesting.controller.rightPane.proceed.EditorProceedController;
import WebTesting.core.drivers.selenium.MySelenium;
import WebTesting.core.drivers.selenium.subDrivers.MyFirefoxDriver;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.MissingAtrributeException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownTypeOfActionException;
import WebTesting.model.DataNode;
import WebTesting.model.TestData;
import WebTesting.model.rightPane.proceed.detail.TableDetailModel;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabProceed;
import WebTesting.view.rightPane.proceed.ControlProceedUpperPanel;
import WebTesting.view.rightPane.proceed.EditorProceed;
import WebTesting.view.rightPane.proceed.detail.TestDetail;;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 2. 2016
 * File: Tests.java
 */
public class MainTest {

	/**
	 * Instance driveru, ktery ridi cely test
	 */
	protected IMyDriver driver;
	
	protected TestData test;
	
	protected Element currentCommand;
	protected String currentAction;
	
	protected int timeDelay;
	protected static boolean run = true;
	
	
	/** Konstruktor pro nastaveni a nasledne spusteni testu
	 * @param test Testovaci data
	 */
	public MainTest(TestData test){
		super();
		
		this.driver = initDriver();
		this.test = test;
		
		new Thread(){
			
			@Override
			public void run() {
				startTest();
				TestOnCloseInformation();
				closeTest();
			}
		}.start();
	}

	
	/**
	 * Vrati instanci driveru, se kterym se bude pracovat
	 */
	private IMyDriver initDriver() {
		//TODO dodelat pro ostatni drivery
		return (IMyDriver) new MySelenium();
	}


	/** Metoda, ktera spusti test
	 * 
	 */
	private void startTest(){
		
		try {
			timeDelay = Integer.parseInt(((ControlProceedUpperPanel) 
					MainWindow.getController(Controllers.PROCEED_UPPER_PANEL_CTRL).getView()).
					getTfDelay().getText());
		} catch (NumberFormatException e) {
			timeDelay = 0;
		} catch (Exception e){
			timeDelay = 0;
		}
		
		DataNode data = test.getNode().getData();
		
		System.out.println("test \"" + data.getName() + "\" is starting");	
		
		//indikace v listu se vsemi testy
		test.getNode().getData().getRootElement().setAttribute(new Attribute("status", STATUS_RUNNING));
		((TabProceed) MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView()).getListProceed().repaint();
		
		List<Element> commands = data.getRootElement().getChildren(COMMAND);
		int i = 0, size = commands.size();
		
		for (Element command : commands) {
			//uzivatelske ukonceni testu
			if(!run){
				setInfo(STATUS_ERROR);
				this.test.getNode().getRootElement().setAttribute("status", STATUS_ERROR);
				break;
			}
			
			//aktualne zpracovavany test
			this.currentCommand = command;
			System.out.println(currentCommand.getAttributeValue("action"));
			
			try {
				executeCommand();
				setInfo(STATUS_SUCCESS);
				
				if(timeDelay != 0){
					Thread.sleep(timeDelay);
				}
			}catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
				setInfo(STATUS_ERROR, e.getMessage(), e);
			}
			
			try{
				if(isSuccess()){
					i++;
					updateProgress(i, size);
					
					if( i == size){
						currentCommand.getParentElement().setAttribute("status", STATUS_SUCCESS);
					}
				}
				else{
					this.test.getNode().getRootElement().setAttribute("status", STATUS_ERROR);
					break;
				}
			}catch(NullPointerException e){
				e.printStackTrace();
			}
		}
		EditorProceedController.decrementActualNumberOfThread();
		((TabProceed) MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView()).getListProceed().repaint();
	}
		

	private void executeCommand() throws Exception{
		
		String typeOfCommand = currentCommand.getAttributeValue("action").toLowerCase();
		this.currentAction = typeOfCommand;
		
		//Kontrola, zda ma prikaz nastavenou hodnotu reprezentujici akci
		if(typeOfCommand != null){
			
			//TODO mozna budu muset odstranit lower case
			//zda je akce uvedena v sezanmu akci
			if(WebTesting.enums.scenarios.Actions.contains(typeOfCommand)){
				switch(typeOfCommand){
				//TODO opravit
				case "assertelementpresent":
				case "assertelementnotpresent":
					driver.assertElements(currentCommand, typeOfCommand);
					break;
				case "asserttextpresent":
				case "asserttextnotpresent":
					driver.assertText(currentCommand, typeOfCommand);;
					break;
				case "asserttitle":
					driver.assertTitle(currentCommand);
					break;
				case "clear":
					driver.clear(currentCommand);
					break;
				case "click":
				case "clickandwait":
					driver.click(currentCommand);
					break;
				case "dropdownselecttext":
				case "dropdownselectindex":
				case "dropdownselectvalue":
					driver.dropDown(currentCommand, typeOfCommand);
					break;
				//TODO opravit
				case "exists":
				case "existscount":
					driver.exists(currentCommand);
					break;
				case "changehtml":
					driver.changeHtml(currentCommand);
					break;
				case "mousehover":
					driver.mouseHover(currentCommand);
					break;
				case "navigate":
					driver.navigate(currentCommand);
					break;
				case "sleep":
					driver.sleep(currentCommand);
					break;
				case "submit":
					driver.submit(currentCommand);
					break;
				case "switchtoframe":
					driver.switchToFrame(currentCommand);
					break;
				case "switchtodefault":
					driver.switchToDefault();
					break;
				case "type":
				case "typeandsubmit":
					driver.type(currentCommand, typeOfCommand);
					break;
				case "visit":
					driver.visit(currentCommand);
					break;
				case "verifytextpresent":
				case "verifytextnotpresent":
					driver.verifyText(currentCommand, typeOfCommand);
					break;
				default:
					throw new NotYetImplementedException(typeOfCommand);
				}
			}
			else{
				throw new UnknownTypeOfActionException("Neznamy typ akce '" + typeOfCommand + "'");
			}
		}
		else{
			throw new MissingAtrributeException("Atribut pro definici akce nebyl nalezen");
		}
	}
		
	
	

	/*---------------------
	 	Nastavovaci metody
	 --------------------*/
	/** Metoda, ktera aktualizuje progress bar v tabulce testu
	 * @param i Pocet provedenych prikazu v testu
	 * @param size Celkovy pocet prikazu v testu
	 */
	private void updateProgress(int i, int size) {
		if(i <= size){
			int value = (int)((i*100)/size);
			test.setProgress(value);
		}
	}
	
	private String getTextFromAttribute(Element command, String attribute) throws NullPointerException{
		if(command != null){
			String value = command.getAttributeValue(attribute);
			if(value != null){
				return value;
			}
			else{
				throw new NullPointerException("Prikaz neobsahuje atribut \"" + attribute + "\"");
			}
		}
		else{
			throw new NullPointerException("Uvedeny prikaz neexistuje");
		}
	}
	
	/** Metoda, ktera kontroluje, zda byl prikaz proveden uspesne. Pokdu ano, pak pokracujeme v dalsich prikazech v testu
	 * @return Rozhodnuti, zda bude test pokracovat nebo ne.
	 * @throws UniverzalException 
	 */
	private boolean isSuccess() throws NullPointerException{
		String success = getTextFromAttribute(this.currentCommand, "status"); 
		if(success != null){
			if(success.equals(STATUS_SUCCESS)){
				return true;
			}
		}
		return false;
	}
	
	
	/** Metoda, ktera nastavim jednotlivym prikazum (XML Elementum) atributy podle jejich provedeni.
	 * @param status Status prikazu
	 * @param message Zprava o chybe
	 * @param exception Vyjimka, ktera nastala
	 */
	private void setInfo(String status, String message, Exception exception){
		if(status != null){
			if(status.equals(STATUS_SUCCESS)){
				this.currentCommand.setAttribute("status", STATUS_SUCCESS);
				this.currentCommand.setAttribute("message", TEXT_SUCCES);
				this.currentCommand.removeAttribute("exception");
			}
			else if(status.equals(STATUS_ERROR)){
				this.currentCommand.setAttribute("status", STATUS_ERROR);
				if(message != null){
					this.currentCommand.setAttribute("message", message);
				}
				if(exception != null){
					this.currentCommand.setAttribute("exception", exception.getClass().getSimpleName());
				}
			}
		}	
		((TableDetailModel) ((TestDetail) MainWindow.getController(Controllers.TEST_DETAIL_CTRL).getView()).
				getTable().getModel()).fireTableDataChanged();
	}
	
	
	/** Prekryta metoda, ktera umozni nastavit pouze status u daneho prikazu
	 * @param status Status prikazu
	 */
	private void setInfo(String status){
		setInfo(status, null, null);
	}
		
	/** Metoda, ktera ukonci vyhodnocovani a uzavre instanci WebDriveru
	 * 
	 */
	private void closeTest(){
		
		System.out.println("Test \"" + test.getNode().getData().getName() + "\" is finishing");
		//TODO doopravit chybu pri dokonceni testu
//		close();
//		quit();
	}

	private void TestOnCloseInformation() {}
	
	/** Metoda, ktera dokaze zastavit vyhodnocovaci mechanismu v pripade potreby uzivatele
	 * @param run Pravdivostni hodnota, ktera muze zastavit vyhodnocovaci mechanismus
	 */
	public static void setRun(boolean run){
		MainTest.run = run;
	}	
	
	public static boolean getRun(){
		return MainTest.run;
	}
}
