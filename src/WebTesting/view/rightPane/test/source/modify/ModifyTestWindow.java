/**
 * 
 */
package WebTesting.view.rightPane.test.source.modify;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import org.jdom2.Element;

import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.XmlOperation;
import WebTesting.controller.rightPane.test.ModifyTestWindowController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.SemanticException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownElementTypeException;
import WebTesting.exceptions.UnknownTypeOfLocator;
import WebTesting.exceptions.UnknownTypeOfNodeException;
import WebTesting.exceptions.UnknownTypeOfParameter;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioListPanel;
import WebTesting.view.rightPane.scenario.EditorScenario;
import WebTesting.view.rightPane.test.modules.ControlTestMiddlePanel;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showAlertNotification;
import static WebTesting.controller.UtilityClass.showInfoNotification;
import static WebTesting.core.Config.*;

/** Trida, ktera poskytuje upravu daneho testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 21. 4. 2016
 * File: EditTestWindow.java
 */
@SuppressWarnings("serial")
public class ModifyTestWindow extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JPanel controlPanel;
	private JButton btnClose;
	private JButton btnSave;

	private Element element;
	private int elementIndex;
	private JPanel rightPanel;
	private JPanel leftPanel;
	private JButton btnAddCommand;
	private JButton btnRemoveCommand;
	private JButton btnRemoveParam;
	private JButton btnSaveScenario;
	
	
	/*----------------------------
	 	Konstruktory
	 ---------------------------*/
	
	/** Konstruktor pro vytvoreni okna pro editaci testu nebo prikazu
	 * @param element Element, nad kterym doslo k udalosti. Bud se jedna o jeden prikaz, nebo o cely test.
	 * @param index Hodnota, ktera udava, zda se jedna o editaci testu nebo prikazu. (-1 -- test, zbyte je index prikazu)
	 * @wbp.parser.constructor
	 */
	public ModifyTestWindow(Element element, int index){
		System.err.println(index);
		this.element = element;
		this.elementIndex = index;
		initComponents();
		this.setVisible(true);
	}
		
	public ModifyTestWindow(Element element) {
		this(element, -1);
	}
	
		
	/**
	 * Metoda, ktera toto okno vytvori
	 */
	private void initComponents() {

		ModifyTestWindowController controller = new ModifyTestWindowController(this);
		MainWindow.addController(controller);
		
		setMinimumSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.6));
		setSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.8));
		setLocationRelativeTo((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
		scrollPane.setViewportView(textArea);
		controlPanel = new JPanel();
		contentPane.add(controlPanel, BorderLayout.SOUTH);
		controlPanel.setLayout(new BorderLayout(0, 0));
		leftPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) leftPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		controlPanel.add(leftPanel, BorderLayout.WEST);
		
		btnAddCommand = new JButton(getLabel("addCommand"));
		btnAddCommand.setActionCommand("addCommand");
		btnAddCommand.addActionListener(controller);
		leftPanel.add(btnAddCommand);
		
		btnRemoveCommand = new JButton(getLabel("removeCommand"));
		btnRemoveCommand.setActionCommand("removeCommand");
		btnRemoveCommand.addActionListener(controller);
		leftPanel.add(btnRemoveCommand);
		
		btnRemoveParam = new JButton(getLabel("removeParameter"));
		btnRemoveParam.setActionCommand("removeParam");
		btnRemoveParam.addActionListener(controller);
		leftPanel.add(btnRemoveParam);
		
		btnSaveScenario = new JButton(getLabel("saveScenario"));
		btnSaveScenario.setActionCommand("saveScenario");
		btnSaveScenario.addActionListener(controller);
		leftPanel.add(btnSaveScenario);
		
		rightPanel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) rightPanel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		controlPanel.add(rightPanel, BorderLayout.EAST);
		
		btnClose = new JButton(getLabel("close"));
		btnClose.setActionCommand("close");
		btnClose.addActionListener(controller);
		rightPanel.add(btnClose);
		
		btnSave = new JButton(getLabel("save"));
		btnSave.setActionCommand("save");
		btnSave.addActionListener(controller);
		rightPanel.add(btnSave);
		
		
		controller.setVisibility();
		pack();
		
		textArea.setText(controller.printToTextArea());
		
		UndoManager manager = new UndoManager();
		Document doc = textArea.getDocument();
		
		doc.addUndoableEditListener(new UndoableEditListener() {
			
			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				manager.addEdit(e.getEdit());
			}
		});
		
		textArea.getActionMap().put("Undo", new AbstractAction("Undo") {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(manager.canUndo()){
						manager.undo();
					}
				}catch (CannotUndoException e){
					e.printStackTrace();
				}
			}
		});
		
		textArea.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
		
		textArea.getActionMap().put("Redo", new AbstractAction("Redo") {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(manager.canRedo()){
						manager.redo();
					}
				}catch(CannotRedoException e){
					e.printStackTrace();
				}
				
			}
		});
		textArea.getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
		
		textArea.getActionMap().put("Save", new AbstractAction("Save") {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnSave.doClick();
			}
		});
		textArea.getInputMap().put(KeyStroke.getKeyStroke("control S"), "Save");
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	/**
	 * @return the element
	 */
	public Element getElement() {
		return element;
	}
	
	/**
	 * @return the elementIndex
	 */
	public int getElementIndex() {
		return elementIndex;
	}
	
	/**
	 * @return the textArea
	 */
	public JTextArea getTextArea() {
		return textArea;
	}
	
	/**
	 * @return the btnAddCommand
	 */
	public JButton getBtnAddCommand() {
		return btnAddCommand;
	}
	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}
	/**
	 * @return the btnRemoveCommand
	 */
	public JButton getBtnRemoveCommand() {
		return btnRemoveCommand;
	}
	/**
	 * @return the btnRemoveParam
	 */
	public JButton getBtnRemoveParam() {
		return btnRemoveParam;
	}
	/**
	 * @return the btnSaveScenario
	 */
	public JButton getBtnSaveScenario() {
		return btnSaveScenario;
	}
	
}
