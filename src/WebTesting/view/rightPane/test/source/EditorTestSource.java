/**
 * 
 */
package WebTesting.view.rightPane.test.source;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import WebTesting.Interfaces.IControlTest;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.test.EditorTestSourceController;
import WebTesting.enums.Controllers;
import WebTesting.model.test.source.ControlTestListPanel;
import WebTesting.model.test.source.TestTextArea;
import WebTesting.view.MainWindow;
import WebTesting.view.popup.ModifyTestPopupMenu;
import WebTesting.view.rightPane.test.EditorTest;

import static WebTesting.core.Config.*;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 9. 12. 2016
 * File: ControlTestSource.java
 */
@SuppressWarnings("serial")
public class EditorTestSource extends JPanel implements IControlTest  {

	private String title = "source";

	private ControlTestListPanel listPane;
	private JPanel textAreaPanel;
	private TestTextArea textArea;
	private JScrollPane scrollPane;

	private JSplitPane splitPane;


	/*------------------*
	 *   Konstruktor    *
	 *------------------*/
	public EditorTestSource() {
		initComponents();
	}



	private void initComponents() {

		EditorTestSourceController controller = new EditorTestSourceController(this);
		MainWindow.addController(controller);

		setLayout(new BorderLayout(0, 0));

		listPane = new ControlTestListPanel();
		listPane.setMinimumSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.2));

		textAreaPanel = new JPanel(new BorderLayout());

		textArea = new TestTextArea();
		scrollPane = new JScrollPane(textArea);

		textAreaPanel.add(scrollPane, BorderLayout.CENTER);

		this.splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, textAreaPanel, listPane);
		this.splitPane.setResizeWeight(0.8);
		add(this.splitPane, BorderLayout.CENTER);
	}


	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	public JTextArea getTextArea() {
		return textArea;
	}

	@Override
	public String getTitle(){
		return this.title;
	}

	@Override
	public void reloadCurrentTest(){
		MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).notifyController();
	}
}
