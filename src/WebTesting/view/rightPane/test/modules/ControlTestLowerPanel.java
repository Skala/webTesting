package WebTesting.view.rightPane.test.modules;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showAlertNotification;
import static WebTesting.controller.UtilityClass.showInfoNotification;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.XmlOperation;
import WebTesting.controller.rightPane.test.ControlTestLowerPanelController;
import WebTesting.core.Config;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.EditorTest;

/** Trida pro vykresleni dolniho ovladaciho panelu pro editor testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 13. 2. 2016
 * File: ConfigTestsLowerPanel.java
 */
@SuppressWarnings("serial")
public class ControlTestLowerPanel extends JPanel {

	private JButton btnSave;
	private JButton btnAddTest;
	private JButton btnUpdate;
	private JPanel rightPanel;
	private JPanel leftPanel;
	private JButton btnDuplicateTest;
	private JButton btnNewTest;
	private JButton btnUndoChanges;

	/**
	 * Konstruktor pro vytvoreni panelu
	 */
	public ControlTestLowerPanel() {

		initComponents();
	}
	
	/**
	 * Metoda pro vytvoreni vsech potrebnych komponent
	 */
	private void initComponents() {
		
		ControlTestLowerPanelController controller = new ControlTestLowerPanelController(this);
		MainWindow.addController(controller);
		
		setLayout(new BorderLayout(0, 0));

		leftPanel = new JPanel();
		rightPanel = new JPanel();


		btnDuplicateTest = new JButton(getLabel("duplicate"));
		btnDuplicateTest.setActionCommand("duplicate");
		btnDuplicateTest.addActionListener(controller);
		leftPanel.add(btnDuplicateTest);

		btnNewTest = new JButton(getLabel("newTest"));
		btnNewTest.setActionCommand("new");
		btnNewTest.addActionListener(controller);
		leftPanel.add(btnNewTest);

		btnUndoChanges = new JButton(getLabel("undoChanges"));
		btnUndoChanges.setActionCommand("undo");
		btnUndoChanges.setToolTipText(getLabel("eventWhichReReadsTheDataFromTheFile"));
		btnUndoChanges.addActionListener(controller);
		leftPanel.add(btnUndoChanges);


		btnAddTest = new JButton(getLabel("addTest"));
		btnAddTest.setActionCommand("add");
		btnAddTest.addActionListener(controller);
		rightPanel.add(btnAddTest);

		btnUpdate = new JButton(getLabel("update"));
		btnUpdate.setActionCommand("update");
		btnUpdate.addActionListener(controller);
		rightPanel.add(btnUpdate);

		btnSave = new JButton(getLabel("save"));
		btnSave.setActionCommand("save");
		btnSave.addActionListener(controller);
		rightPanel.add(btnSave);

		add(leftPanel, BorderLayout.WEST);
		add(rightPanel, BorderLayout.EAST);
	}
}
