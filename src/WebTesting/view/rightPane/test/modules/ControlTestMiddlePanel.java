/**
 * 
 */
package WebTesting.view.rightPane.test.modules;

import javax.sound.midi.SysexMessage;
import javax.swing.JPanel;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showAlertNotification;
import static WebTesting.controller.UtilityClass.showInfoNotification;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import WebTesting.Interfaces.IControlTest;
import WebTesting.controller.rightPane.test.ControlTestMiddlePanelController;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.design.EditorTestDesign;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 20. 12. 2016
 * File: ControlTestMiddlePanel.java
 */
public class ControlTestMiddlePanel extends JPanel{
	
	private JTabbedPane tabbedPane;
	private List<IControlTest> controlTestPanelList;
	
	private EditorTestSource editorTestSource;
	private EditorTestDesign editorTestDesign;
	
	private JScrollPane infoScrollPane;
	private JTextField infoTF;

	/**
	 * Create the panel.
	 */
	public ControlTestMiddlePanel() {
		
		initComponents();
	}
	
	
	private void initComponents() {
		
		ControlTestMiddlePanelController controller = new ControlTestMiddlePanelController(this);
		MainWindow.addController(controller);
		
		this.controlTestPanelList = new ArrayList<IControlTest>();
		
		setLayout(new BorderLayout(0, 0));
		
		tabbedPane = new JTabbedPane(JTabbedPane.BOTTOM);
		tabbedPane.addChangeListener(controller);
		add(tabbedPane, BorderLayout.CENTER);
		
		editorTestSource = new EditorTestSource();
		editorTestDesign = new EditorTestDesign();

		controlTestPanelList.add(editorTestSource);
		controlTestPanelList.add(editorTestDesign);
		
		for (IControlTest testPane : controlTestPanelList) {
			tabbedPane.addTab(testPane.getTitle(), null, (JPanel) testPane, null);
		}
					
		infoTF = new JTextField();
		infoTF.setEditable(false);
				
		infoScrollPane = new JScrollPane(infoTF);
		add(infoScrollPane, BorderLayout.SOUTH);
	}
	
	/*-----------------------*
	 *   Pristupove metody   *
	 * ----------------------*/
		
	/**
	 * @return the tabbedPane
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}
	
	/**
	 * @return the infoTF
	 */
	public JTextField getInfoTF() {
		return infoTF;
	}
	
	/**
	 * @return the controlTestPanel
	 */
	public List<IControlTest> getControlTestPanel() {
		return controlTestPanelList;
	}
	
}
