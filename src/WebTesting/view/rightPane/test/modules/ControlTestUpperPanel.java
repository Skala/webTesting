package WebTesting.view.rightPane.test.modules;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.FlowLayout;

import javax.management.InstanceAlreadyExistsException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import WebTesting.controller.rightPane.test.ControlTestUpperPanelController;
import WebTesting.view.MainWindow;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/** Trida pro vytvoreni horniho ovladaciho panelu v editoru testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 13. 2. 2016
 * File: ConfigTestsUpperPanel.java
 */
@SuppressWarnings("serial")
public class ControlTestUpperPanel extends JPanel {

	private JLabel lblTitleTest;
	private JTextField tfTitleTest;

	/**
	 * Konstruktor tohoto panelu
	 */
	public ControlTestUpperPanel() {
		initComponents();
	}
	
	
	/**
	 * Metoda pro vytvoreni komponent v tomto panelu
	 */
	private void initComponents() {
		
		ControlTestUpperPanelController controller = new ControlTestUpperPanelController(this);
		MainWindow.addController(controller);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[]{0.0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0};
		setLayout(gridBagLayout);

		GridBagConstraints lblCon = new GridBagConstraints();
		lblCon.fill = GridBagConstraints.VERTICAL;
		lblCon.anchor = GridBagConstraints.WEST;
		lblCon.insets = new Insets(5, 5, 5, 5);
		lblCon.gridx = 0;
		lblCon.gridy = 0;
		lblTitleTest = new JLabel(getLabel("titleOfTest"));
		lblTitleTest.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblTitleTest, lblCon);
		
		GridBagConstraints tfCon = new GridBagConstraints();
		tfCon.insets = new Insets(5, 5, 5, 5);
		tfCon.fill = GridBagConstraints.HORIZONTAL;
		tfCon.anchor = GridBagConstraints.WEST;
		tfTitleTest = new JTextField("");
		tfTitleTest.setEditable(false);
		add(tfTitleTest, tfCon);
	}

	/*-------------------*
	 * Gettery a settery *
	 *-------------------*/
	public JTextField getTfTitleTest() {
		return tfTitleTest;
	}

	public void setTfTitleTest(String name) {
		tfTitleTest.setText(name);
	}
}
