/**
 * 
 */
package WebTesting.view.rightPane.test.design.modules;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.jdom2.Element;

import WebTesting.enums.Controllers;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.design.modules.commandEditModules.CommandEditButtons;
import WebTesting.view.rightPane.test.design.modules.commandEditModules.CommandEditCenter;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 20. 12. 2016
 * File: TestDesignRowFrame.java
 */
public class TestDesignCommandEdit extends JFrame {

	private Element command;
	
	private JScrollPane scrollCenterPane;
	private CommandEditCenter centerPane;
	private CommandEditButtons buttonPane;
	
	
	/**
	 * Create the frame.
	 */
	public TestDesignCommandEdit(Element command) {
		this.command = command;
		initComponents();
	}
	
	
	private void initComponents() {
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setAlwaysOnTop(true);
		this.setMinimumSize(new Dimension(480, 240));
		this.setLocationRelativeTo(null);
		this.setTitle(command.getAttributeValue("action"));
		
		this.addWindowListener(new WindowAdapter() {
			/* (non-Javadoc)
			 * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
			 */
			@Override
			public void windowClosing(WindowEvent e) {
				closeCommandEdit();
			}
		});
					
		//Nastaveni okna
		this.getContentPane().setLayout(new BorderLayout(0,0));
		
		//Vytvoreni scrollpanu na stred okna
		this.centerPane = new CommandEditCenter(this.command);
		this.scrollCenterPane = new JScrollPane(this.centerPane);
		this.getContentPane().add(scrollCenterPane, BorderLayout.CENTER);
		
		//Vytvoreni okna pro tlacitka
		this.buttonPane = new CommandEditButtons(this);
		this.getContentPane().add(buttonPane, BorderLayout.SOUTH);
					
		this.setVisible(true);
	}


	public void closeCommandEdit() {
		((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()).setEnabled(true);
		dispose();
	}
	
	
}
