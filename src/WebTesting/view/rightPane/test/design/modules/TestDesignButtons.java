/**
 * 
 */
package WebTesting.view.rightPane.test.design.modules;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import WebTesting.exceptions.WarningException;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 1. 2017
 * File: TestDesignButtons.java
 */
public class TestDesignButtons extends JPanel implements ActionListener {
	private JButton addCommandButton;
	
	/**
	 * Create the panel.
	 */
	public TestDesignButtons() {
		initComponents();
	}

	
	private void initComponents() {
		this.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
				
		this.addCommandButton = new JButton("Add command");
		this.addCommandButton.setActionCommand("add");
		this.addCommandButton.addActionListener(this);
		add(this.addCommandButton);
	}

	public void actionPerformed(ActionEvent ae) {
		switch(ae.getActionCommand()){
		case "add":
			//TODO dodelat
			System.out.println("add clicked");
			break;
		default:
			try {
				throw new WarningException();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
