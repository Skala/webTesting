/**
 * 
 */
package WebTesting.view.rightPane.test.design.modules.commandEditModules;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.jdom2.Element;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 1. 2017
 * File: CommandEditCenter.java
 */
public class CommandEditCenter extends JPanel {

	
	private Element command;
	private GridBagConstraints con;
	
	/**
	 * Vytvoreni panelu pro editaci jednoho prikazu
	 */
	public CommandEditCenter(Element command) {
		this.command = command;
		initComponents();
	}

	/**
	 * 
	 */
	private void initComponents() {
		
		this.setLayout(new GridBagLayout());
		initConstraints();
		
		JLabel label = new JLabel(command.getAttributeValue("action"));
		this.add(label, con);
		con.gridy++;
				
		for (Element subElement : command.getChildren()) {
			con.gridx = 0;
			con.weightx = 1;
			con.fill = GridBagConstraints.NONE;
			label = new JLabel(subElement.getName());
			this.add(label, con);
						
			for (Element subSubElement: subElement.getChildren()) {
				con.gridx=1;
				con.weightx = 1;
				con.fill = GridBagConstraints.NONE;
				label = new JLabel(subSubElement.getName(), SwingConstants.RIGHT);
				this.add(label, con);
				
				con.gridx++;
				con.weightx = 50;
				con.fill = GridBagConstraints.HORIZONTAL;
				JTextField tf = new JTextField(subSubElement.getValue());
				//tf.setColumns(30);
				this.add(tf, con);
				con.gridy++;
			}
		}
	}

	/**
	 * 
	 */
	private void initConstraints() {
		con = new GridBagConstraints();
		con.fill = GridBagConstraints.NONE;
		con.insets = new Insets(2, 5, 2, 5);
		
		con.gridx = 0;
		con.gridy = 0;
		con.gridwidth = 1;
		con.gridheight = 1;
		con.weightx = 1;
		con.weighty = 1;
	}	
}
