/**
 * 
 */
package WebTesting.view.rightPane.test.design.modules.commandEditModules;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import WebTesting.exceptions.WarningException;
import WebTesting.view.rightPane.test.design.modules.TestDesignCommandEdit;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 25. 1. 2017
 * File: CommandEditButtons.java
 */
public class CommandEditButtons extends JPanel implements ActionListener {
	
	private JButton okButton;
	private JButton cancelButton;
	private TestDesignCommandEdit parent; 

	/**
	 * Create the panel.
	 */
	public CommandEditButtons(TestDesignCommandEdit parent) {
		this.parent = parent;
		initComponents();
	}

	private void initComponents() {
		this.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		
		this.okButton = new JButton(getLabel("save"));
		this.okButton.setActionCommand("save");
		this.okButton.addActionListener(this);
		add(this.okButton);
		
		this.cancelButton = new JButton(getLabel("cancel"));
		this.cancelButton.setActionCommand("cancel");
		this.cancelButton.addActionListener(this);
		add(this.cancelButton);
	}

	public void actionPerformed(ActionEvent ae) {
		switch(ae.getActionCommand()){
		case "save":
			//TODO dodelat
			parent.closeCommandEdit();
			System.out.println("Save clicked");
			break;
		case "cancel":
			//TODO dodelat
			parent.closeCommandEdit();
			System.out.println("Cancel clicked");
			break;
		default:
			try {
				throw new WarningException();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
