/**
 * 
 */
package WebTesting.view.rightPane.test.design;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.jdom2.Element;

import WebTesting.Interfaces.IControlTest;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.enums.Controllers;
import WebTesting.model.TestData;
import WebTesting.model.rightPane.proceed.TableProceedModel;
import WebTesting.model.rightPane.proceed.TableProceedRenderer;
import WebTesting.model.test.design.TableDesignModel;
import WebTesting.model.test.design.TableDesignRenderer;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.EditorTest;
import WebTesting.view.rightPane.test.design.modules.TestDesignButtons;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 7. 12. 2016
 * File: ControlTestDesign.java
 */
@SuppressWarnings("serial")
public class EditorTestDesign extends JPanel implements IControlTest, MouseListener{
	
	private String title = "design";
	
	private JTable table;
	private JScrollPane tableScrollPane;
	
	private TestDesignButtons testDesignButtons;
	
	
	public EditorTestDesign() {
		initComponents();
	}
	
	
	private void initComponents() {
		
		setLayout(new BorderLayout(0, 0));
		
		table = new JTable(new TableDesignModel()){
			public TableCellRenderer getCellRenderer(int row, int column) {
				TableDesignRenderer renderer = new TableDesignRenderer(); 
				return renderer;
			}
		};
		
		table.addMouseListener(this);
				
		setTable();
		tableScrollPane = new JScrollPane(table);
		add(tableScrollPane, BorderLayout.CENTER);
		
		testDesignButtons = new TestDesignButtons();
		add(testDesignButtons, BorderLayout.SOUTH);
	}
	
	
	private void setTable() {
		DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		renderer.setVerticalAlignment(SwingConstants.CENTER);

		table.getColumnModel().getColumn(0).setMinWidth(30);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		
		table.setRowHeight(30);
	}
	
	/*--------------------*
	 * Pristupove metody  *
	 *--------------------*/
	public String getTitle(){
		return this.title;
	}

	/*---------------------*
	 *   Pomocne metody    *
	 *---------------------*/

	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IControlTest#reloadCurrentTest()
	 * Vytvoreni tabulky s jednotlivymi prikazy
	 */
	public void reloadCurrentTest() {
		List<Element> els = ((EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL)).getCurrentCommands();
		((TableDesignModel) (table.getModel())).removeAllCommands();
		for (Element el : els) {
			((TableDesignModel) (table.getModel())).myAddRow(el);
		}
	}
	
	
	/*------------------*
	 *    Posluchace    *
	 *------------------*/
	
	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		int row = table.getSelectedRow();
		int column = table.getSelectedColumn();
		
		switch(column){
		case 0:
			break;
		case 1:
			TableDesignModel model = (TableDesignModel) table.getModel();
			Element data = model.getCommands().get(row);
			model.setValueAt(data, row, column);
			break;
		}
		System.out.println("table listener: " + row + ":" + column);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent e) {}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent e) {}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {}
}
