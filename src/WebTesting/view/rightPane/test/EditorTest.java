/**
 * 
 */
package WebTesting.view.rightPane.test;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;

import javax.swing.JPanel;

import org.jdom2.Element;

import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.modules.ControlTestLowerPanel;
import WebTesting.view.rightPane.test.modules.ControlTestMiddlePanel;
import WebTesting.view.rightPane.test.modules.ControlTestUpperPanel;

/** Trida, ktera vykresluje panel s editorem testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 2. 2016
 * File: EditorTests.java
 */
@SuppressWarnings("serial")
public class EditorTest extends JPanel implements IRightPane{

	private ControlTestUpperPanel upperControl;
	private ControlTestLowerPanel lowerControl;
	private ControlTestMiddlePanel middleControl;
	
	/*--------------------*
	 *    Konstruktory    * 
	 *--------------------*/

	public EditorTest() {
		initComponents();
	}
	
	
	/**
	 * Metoda pro inicializaci tohoto editoru
	 */
	private void initComponents() {
		
		EditorTestController controller = new EditorTestController(this);
		MainWindow.addController(controller);

		setBackground(Color.LIGHT_GRAY);
		setLayout(new BorderLayout(0, 0));

		upperControl = new ControlTestUpperPanel();
		lowerControl = new ControlTestLowerPanel();
		middleControl = new ControlTestMiddlePanel();
		
		add(upperControl, BorderLayout.NORTH);
		add(middleControl, BorderLayout.CENTER);
		add(lowerControl, BorderLayout.SOUTH);
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	
	public ControlTestMiddlePanel getMiddleControl() {
		return middleControl;
	}
	
	public ControlTestUpperPanel getUpperControl() {
		return upperControl;
	}
	
	public ControlTestLowerPanel getLowerControl() {
		return lowerControl;
	}

	
	@Override
	public String getPaneTitle() {
		return RightPaneEditor.EDITOR_TEST.getTitle();
	}

	
}
