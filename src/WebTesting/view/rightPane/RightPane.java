package WebTesting.view.rightPane;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;

import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.rightPane.proceed.EditorProceed;
import WebTesting.view.rightPane.proceed.detail.TestDetail;
import WebTesting.view.rightPane.proceed.detail.TestDetailOfCommand;
import WebTesting.view.rightPane.scenario.EditorScenario;
import WebTesting.view.rightPane.test.EditorTest;

import static WebTesting.core.Config.*;

/** Trida, ktera ma za ukol vykreslovat jednu ze dvou zakladnich ploch aplikace, tj. editory. 
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 2. 2. 2016
 * File: ContentPanel.java
 */
@SuppressWarnings("serial")
public class RightPane extends JPanel {

	private EditorTest editorTest;
	private EditorScenario editorScenario;
	private EditorProceed editorProceed;
	private TestDetail testDetail;
	private TestDetailOfCommand commandDetail;
	
	
	/*---------------------
	 	Konstruktor
	 --------------------*/
	public RightPane() {
		initComponents();
	}
	
	
	/**
	 * Metoda pro inicializaci tohoto panelu
	 */
	private void initComponents() {		
		
		RightPaneController controller = new RightPaneController(this);
		MainWindow.addController(controller);
		
		setBackground(new Color(0, 255, 204));
		this.setLayout(new CardLayout(0, 0));
		
		editorTest = new EditorTest();
		editorScenario = new EditorScenario();
		editorProceed = new EditorProceed();
		testDetail = new TestDetail();
		commandDetail = new TestDetailOfCommand();
		
		this.add(editorTest, RightPaneEditor.EDITOR_TEST.getTitle());
		this.add(editorScenario, RightPaneEditor.EDITOR_SCENARIO.getTitle());
		this.add(editorProceed, RightPaneEditor.EDITOR_PROCEED.getTitle());
		this.add(testDetail, RightPaneEditor.TEST_DETAIL.getTitle());
		this.add(commandDetail, RightPaneEditor.COMMAND_DETAIL.getTitle());
	}
	
	
	/*----------------------
	 	Gettery a settery
	 -----------------------*/
	
	public EditorTest getEditorTests() {
		return editorTest;
	}
	public EditorScenario getEditorScenario() {
		return editorScenario;
	}
	public EditorProceed getEditorProceed() {
		return editorProceed;
	}
	public TestDetail getTestDetail() {
		return testDetail;
	}
	public TestDetailOfCommand getTestDetailOfCommand(){
		return commandDetail;
	}
}
