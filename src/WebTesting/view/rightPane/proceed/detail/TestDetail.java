/**
 * 
 */
package WebTesting.view.rightPane.proceed.detail;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.jdom2.Element;

import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.rightPane.proceed.detail.TestDetailController;
import WebTesting.enums.RightPaneEditor;
import WebTesting.model.TestData;
import WebTesting.model.rightPane.proceed.detail.TableDetailModel;
import WebTesting.model.rightPane.proceed.detail.TableDetailRenderer;
import WebTesting.view.MainWindow;

/** Trida, ktera vykresluje detail o testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 3. 2016
 * File: TestDetail.java
 */
@SuppressWarnings("serial")
public class TestDetail extends JPanel implements IRightPane {
	
	
	private ControlDetailUpperPanel upperPanel;
	private JScrollPane detailScrollPane;
	private JTable table;
	
	/**
	 * Konstruktor tohoto panelu 
	 */
	public TestDetail() {
		initComponents();
	}
	
	/**
	 * Metoda pro vytvoreni panelu
	 */
	private void initComponents() {
		
		
		setLayout(new BorderLayout(0, 0));
		
		
		table = new JTable(new TableDetailModel()){
			public TableCellRenderer getCellRenderer(int row, int column){
				TableDetailRenderer renderer = new TableDetailRenderer();
				return renderer;
			}
		}; 
		
		TestDetailController controller = new TestDetailController(this);
		MainWindow.addController(controller);
		
		table.addMouseListener(controller);
		setTable();
		
		upperPanel = new ControlDetailUpperPanel();
		add(upperPanel, BorderLayout.NORTH);
		
		
		detailScrollPane = new JScrollPane(table);
		add(detailScrollPane, BorderLayout.CENTER);
	}
	
	/**
	 * Metoda pro vytvoreni tabulky, ve ktere je seznam prikazu, ktere test obsahuje
	 */
	private void setTable() {
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getTableHeader().setReorderingAllowed(false);
		
		DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		renderer.setVerticalAlignment(SwingConstants.CENTER);
		
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(0).setMaxWidth(50);
		table.getColumnModel().getColumn(1).setPreferredWidth(300);
		table.getColumnModel().getColumn(2).setMinWidth(80);
		table.getColumnModel().getColumn(2).setMaxWidth(80);
		table.getColumnModel().getColumn(3).setMinWidth(80);
		table.getColumnModel().getColumn(3).setMaxWidth(80);

		table.setRowHeight(30);
	}
	
	/*-------------------------
	 	Pristupove metody
	 -----------------------*/
	public JTable getTable() {
		return table;
	}


	@Override
	public String getPaneTitle() {
		return RightPaneEditor.TEST_DETAIL.getTitle();
	}
}
