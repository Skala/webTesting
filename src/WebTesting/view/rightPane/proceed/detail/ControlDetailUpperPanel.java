/**
 * 
 */
package WebTesting.view.rightPane.proceed.detail;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.controller.rightPane.proceed.detail.ControlDetailUpperPanelController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.RightPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/** Trida, ktera vykresluje horni ovladaci panel ve kterem je zobrazen detail testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 3. 2016
 * File: ControlDetailUpperPanel.java
 */
@SuppressWarnings("serial")
public class ControlDetailUpperPanel extends JPanel {
	
	private JButton btnClosedetail;
	private JLabel lblTestname;
	private JTextField tfTestName;

	/**
	 * Konstruktor
	 */
	public ControlDetailUpperPanel() {

		initComponents();
	}
		
	/**
	 * Metoda, ktera inicializuje tento panel
	 */
	private void initComponents() {
		
		ControlDetailUpperPanelController controller = new ControlDetailUpperPanelController(this);
		MainWindow.addController(controller);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0};
		setLayout(gridBagLayout);
		
		
		lblTestname = new JLabel(getLabel("titleOfTest"));
		GridBagConstraints gbc_lblTestname = new GridBagConstraints();
		gbc_lblTestname.anchor = GridBagConstraints.WEST;
		gbc_lblTestname.insets = new Insets(5, 5, 5, 5);
		gbc_lblTestname.gridx = 0;
		gbc_lblTestname.gridy = 0;
		add(this.lblTestname, gbc_lblTestname);
		
		
		tfTestName = new JTextField();
		GridBagConstraints gbc_tfTestName = new GridBagConstraints();
		gbc_tfTestName.insets = new Insets(5, 5, 5, 5);
		gbc_tfTestName.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfTestName.gridx = 1;
		gbc_tfTestName.gridy = 0;
		add(this.tfTestName, gbc_tfTestName);
		tfTestName.setEditable(false);
		tfTestName.setColumns(40);
		
		
		btnClosedetail = new JButton(getLabel("closeDetail"));
		GridBagConstraints gbc_btnClosedetail = new GridBagConstraints();
		gbc_btnClosedetail.anchor = GridBagConstraints.EAST;
		gbc_btnClosedetail.insets = new Insets(5, 5, 5, 5);
		gbc_btnClosedetail.gridx = 2;
		gbc_btnClosedetail.gridy = 0;
		btnClosedetail.addActionListener(controller);
		add(this.btnClosedetail, gbc_btnClosedetail);
	}
	
	public void setTfTestName(String newName){
		this.tfTestName.setText(newName);
	}
}
