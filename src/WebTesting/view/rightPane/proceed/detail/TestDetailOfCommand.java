/**
 * 
 */
package WebTesting.view.rightPane.proceed.detail;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.controller.rightPane.proceed.detail.TestDetailOfCommandController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.RightPane;

/** Trida, ktera zobrazuje detail konkretni prikazu v testu.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 8. 4. 2016
 * File: TestDetailOfCommand.java
 */
@SuppressWarnings("serial")
public class TestDetailOfCommand extends JPanel implements IRightPane {
	
	
	private JPanel upperPanel;
	private JButton btnCloseDetailOfCommand;
	private JTextArea textArea;
	private JScrollPane scrollTextPane;

	/**
	 * Konstruktor tohoto panelu
	 */
	public TestDetailOfCommand() {

		initComponents();
	}
	
	
	/**
	 * Metoda, ktera nastavi komponenty v tomto editoru
	 */
	private void initComponents() {
		
		TestDetailOfCommandController controller = new TestDetailOfCommandController(this);
		MainWindow.addController(controller);
		
		setLayout(new BorderLayout(0, 0));
		
		upperPanel = new JPanel();
		FlowLayout fl_upperPanel = (FlowLayout) upperPanel.getLayout();
		fl_upperPanel.setAlignment(FlowLayout.RIGHT);
		
		add(upperPanel, BorderLayout.NORTH);
		
		btnCloseDetailOfCommand = new JButton(getLabel("closeDetailOfCommand"));
		btnCloseDetailOfCommand.addActionListener(controller);
		upperPanel.add(btnCloseDetailOfCommand);
		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 11));
		
		scrollTextPane = new JScrollPane(textArea);
		add(scrollTextPane, BorderLayout.CENTER);
	}
	
	/*------------------------
	 	Pristupove metody
	 ------------------------*/
	
	public JTextArea getTextArea() {
		return textArea;
	}

	@Override
	public String getPaneTitle() {
		return RightPaneEditor.COMMAND_DETAIL.getTitle();
	}
}
