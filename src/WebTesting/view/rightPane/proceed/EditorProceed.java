/**
 * 
 */
package WebTesting.view.rightPane.proceed;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.rightPane.proceed.EditorProceedController;
import WebTesting.enums.RightPaneEditor;
import WebTesting.model.rightPane.proceed.TableProceedModel;
import WebTesting.model.rightPane.proceed.TableProceedRenderer;
import WebTesting.view.MainWindow;

/** Trida, ktera vykresluje tabulku s pripraveny testy
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 2. 2016
 * File: configuratorProceed.java
 */
@SuppressWarnings("serial")
public class EditorProceed extends JPanel implements IRightPane{

	private JButton btnRunChoosed;
	private JButton btnDeleteFinished;
	private JButton btnDeletechosen;
	private JButton btnRunAllTests;
	private JCheckBox chBChoose;
	
	private JPanel controlPanel;
	private JPanel leftPanel;
	private JPanel rightPanel;
	
	public JTable table;

	private JScrollPane scrollTable;
	private ControlProceedUpperPanel upperPanel;
	
	
	/**
	 * Konstruktor
	 */
	public EditorProceed() {
		initComponents();
	}
	
	
	/**
	 * Metoda pro vytvoreni tohoto panelu
	 */
	private void initComponents() {
		
		table = new JTable(new TableProceedModel()){
			public TableCellRenderer getCellRenderer(int row, int column) {
				TableProceedRenderer renderer = new TableProceedRenderer(); 
				return renderer;
			}
		};
		
		EditorProceedController controller = new EditorProceedController(this);
		MainWindow.addController(controller);
		
		setLayout(new BorderLayout(0, 0));
	
		table.addMouseListener(controller);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getTableHeader().setReorderingAllowed(false);
		
		upperPanel = new ControlProceedUpperPanel();
		scrollTable = new JScrollPane(table);
		
		controlPanel = new JPanel();
		
		rightPanel = new JPanel();
		leftPanel = new JPanel();
				
		controlPanel.setLayout(new BorderLayout(0, 0));
		controlPanel.add(leftPanel, BorderLayout.WEST);

		chBChoose = new JCheckBox();
		chBChoose.setActionCommand("choose");
		chBChoose.setSelected(true);
		chBChoose.addActionListener(controller);
		

		btnDeletechosen = new JButton(getLabel("deleteSelected"));
		btnDeletechosen.setActionCommand("deleteSelected");
		btnDeletechosen.addActionListener(controller);
		rightPanel.add(btnDeletechosen);

		btnDeleteFinished = new JButton(getLabel("deleteFinished"));
		btnDeleteFinished.setActionCommand("deleteFinished");
		rightPanel.add(btnDeleteFinished);
		btnDeleteFinished.addActionListener(controller);

		btnRunChoosed = new JButton(getLabel("runChosen"));
		btnRunChoosed.setActionCommand("runChosen");
		rightPanel.add(btnRunChoosed);
		btnRunChoosed.addActionListener(controller);

		btnRunAllTests = new JButton(getLabel("runTests"));
		btnRunAllTests.setActionCommand("runAll");
		btnRunAllTests.setMnemonic('R'); //KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK )
		rightPanel.add(btnRunAllTests);
		btnRunAllTests.addActionListener(controller);


		leftPanel.add(chBChoose);
		controlPanel.add(rightPanel, BorderLayout.EAST);
		add(upperPanel, BorderLayout.NORTH);
		add(scrollTable, BorderLayout.CENTER);
		add(controlPanel, BorderLayout.SOUTH);
	}
			
	/*----------------------
	 * Gettery a settery
	 ----------------------*/
	public JTable getTable(){
		return table;
	}
	
	/**
	 * @return the chBChoose
	 */
	public JCheckBox getChBChoose() {
		return chBChoose;
	}
	
	@Override
	public String getPaneTitle() {
		return RightPaneEditor.EDITOR_PROCEED.getTitle();
	}
}
