/**
 * 
 */
package WebTesting.view.rightPane.proceed;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import WebTesting.controller.rightPane.proceed.ControlProceedUpperPanelController;
import WebTesting.core.MainTest;
import WebTesting.enums.DriverList;
import WebTesting.view.MainWindow;

import javax.swing.JComboBox;
/** Trida, ktera nastaveje horni panel u panelu zobrazujici testy pripravene ke spusteni
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 2. 3. 2016
 * File: ControlProceedUpperPanel.java
 */
@SuppressWarnings("serial")
public class ControlProceedUpperPanel extends JPanel implements ActionListener{
		
	private JLabel lblShowinbrowser;
	private JCheckBox chckbxShowinbrowser;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JLabel lblZpozdeniTestu;
	private JTextField tfDelay;
	private JButton btnCanceltests;
	private JSeparator separator;
	private JTextField runningStatus;
	private JComboBox<String> driverCombo;

	/**
	 * Create the panel.
	 */
	public ControlProceedUpperPanel() {

		initComponents();
	}
		
	private void initComponents() {
		
		
		ControlProceedUpperPanelController controller = new ControlProceedUpperPanelController(this);
		MainWindow.addController(controller);
		
		setLayout(new BorderLayout(0, 0));
		
		// left panel
		leftPanel = new JPanel();
		add(leftPanel, BorderLayout.WEST);
		
		lblShowinbrowser = new JLabel(getLabel("showInBrowser"));
		leftPanel.add(lblShowinbrowser);
		
		chckbxShowinbrowser = new JCheckBox("");
		leftPanel.add(chckbxShowinbrowser);
		
		separator = new JSeparator();
		leftPanel.add(separator);
		
		btnCanceltests = new JButton(getLabel("stopTests"));
		btnCanceltests.setActionCommand("cancelTests");
		btnCanceltests.addActionListener(this);
		
		this.driverCombo = new JComboBox<String>(DriverList.getTitles());
		this.leftPanel.add(this.driverCombo);
		btnCanceltests.setHorizontalAlignment(SwingConstants.LEFT);
		leftPanel.add(btnCanceltests);
		
		//right panel
		rightPanel = new JPanel();
		add(rightPanel, BorderLayout.EAST);
		
		runningStatus = new JTextField(getLabel("beforeStarting"));
		runningStatus.setHorizontalAlignment(SwingConstants.CENTER);
		rightPanel.add(runningStatus);
		runningStatus.setColumns(10);
		lblZpozdeniTestu = new JLabel(getLabel("testDelay"));
		rightPanel.add(lblZpozdeniTestu);
		
		tfDelay = new JTextField();
		rightPanel.add(tfDelay);
		tfDelay.setColumns(8);
	}
	
	public String getDriverComboItem(){
		return driverCombo.getSelectedItem().toString();
	}
	
	public JTextField getTfDelay() {
		return tfDelay;
	}
	
	public boolean showInBrowser(){
		return chckbxShowinbrowser.isSelected();
	}
	
	public JTextField getRunningStatus() {
		return runningStatus;
	}

	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
}
