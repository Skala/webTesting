/**
 * 
 */
package WebTesting.view.rightPane.scenario;

import javax.swing.JPanel;
import javax.swing.JLabel;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.FlowLayout;
import javax.swing.JTextField;

import WebTesting.controller.rightPane.scenario.ControlScenarioUpperPanelController;
import WebTesting.view.MainWindow;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/** Trida pro vytvoreni horniho panelu v editoru scenare
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 20. 2. 2016
 * File: ControlScenarioUpperPanel.java
 */
@SuppressWarnings("serial")
public class ControlScenarioUpperPanel extends JPanel {
	
	protected static JLabel lblScenarioname;
	protected static JTextField tfScenarioName;

	/**
	 * Konstruktor
	 */
	public ControlScenarioUpperPanel() {
		initComponents();
	}
	
	/**
	 * Metoda, ktera nastavi komponenty v tomto panelu.
	 */
	private void initComponents() {
		
		ControlScenarioUpperPanelController controller = new ControlScenarioUpperPanelController(this);
		MainWindow.addController(controller);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{76, 326, 0};
		gridBagLayout.rowHeights = new int[]{20, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		lblScenarioname = new JLabel(getLabel("titleOfScenario"));
		GridBagConstraints gbc_lblScenarioname = new GridBagConstraints();
		gbc_lblScenarioname.anchor = GridBagConstraints.WEST;
		gbc_lblScenarioname.insets = new Insets(5, 5, 5, 5);
		gbc_lblScenarioname.gridx = 0;
		gbc_lblScenarioname.gridy = 0;
		add(lblScenarioname, gbc_lblScenarioname);
		tfScenarioName = new JTextField();
		tfScenarioName.setEditable(false);
		GridBagConstraints gbc_tfScenarioName = new GridBagConstraints();
		gbc_tfScenarioName.insets = new Insets(5, 5, 5, 5);
		gbc_tfScenarioName.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfScenarioName.gridx = 1;
		gbc_tfScenarioName.gridy = 0;
		add(tfScenarioName, gbc_tfScenarioName);
	}	
	
	/**
	 * @return the tfScenarioName
	 */
	public JTextField getTfScenarioName() {
		return tfScenarioName;
	}
}
