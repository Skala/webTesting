/**
 * 
 */
package WebTesting.view.rightPane.scenario;

import static WebTesting.controller.UtilityClass.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.XmlOperation;
import WebTesting.controller.rightPane.scenario.ControlScenarioLowerPanelController;
import WebTesting.core.Config;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;


/** Trida reprezentujici spodni panel v editoru scenare
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 12. 2. 2016
 * File: ControlPanelScenario.java
 * 
 */
@SuppressWarnings("serial")
public class ControlScenarioLowerPanel extends JPanel {

	private JButton btnSave;
	private JButton btnTest;
	private JButton btnUpdate;
	private JButton btnNewScenario;
	private JPanel panelLeft;
	private JPanel panelRight;
	private JButton btnDuplicate;
	private JButton btnLoadfromxml;

	/**
	 * Konstruktor
	 */
	public ControlScenarioLowerPanel() {

		initComponents();
	}


	/**
	 * Metoda pro vytvoreni tohoto panelu
	 */
	private void initComponents() {
		
		ControlScenarioLowerPanelController controller = new ControlScenarioLowerPanelController(this);
		MainWindow.addController(controller);

		setLayout(new BorderLayout(0, 0));

		panelLeft = new JPanel();
		FlowLayout leftFlow = (FlowLayout) panelLeft.getLayout();
		leftFlow.setAlignment(FlowLayout.LEFT);


		panelRight = new JPanel();
		FlowLayout rightFlow = (FlowLayout) panelRight.getLayout();
		rightFlow.setAlignment(FlowLayout.RIGHT);


		btnNewScenario = new JButton(getLabel("newScenario"));
		btnNewScenario.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewScenario.setActionCommand("new");
		btnNewScenario.addActionListener(controller);
		panelLeft.add(btnNewScenario);

		btnDuplicate = new JButton(getLabel("duplicate"));
		btnDuplicate.setActionCommand("duplicate");
		btnDuplicate.addActionListener(controller);
		panelLeft.add(btnDuplicate);

		btnLoadfromxml = new JButton(getLabel("undoChanges"));
		btnLoadfromxml.setActionCommand("undo");
		btnLoadfromxml.addActionListener(controller);
		btnLoadfromxml.setToolTipText(getLabel("eventWhichReReadsTheDataFromTheFile"));
		panelLeft.add(btnLoadfromxml);

		btnTest = new JButton(getLabel("test"));
		btnTest.setActionCommand("test");
		btnTest.addActionListener(controller);
		panelRight.add(btnTest);

		btnUpdate = new JButton(getLabel("update"));
		btnUpdate.setActionCommand("update");
		btnUpdate.addActionListener(controller);
		panelRight.add(btnUpdate);

		btnSave = new JButton(getLabel("save"));
		btnSave.setActionCommand("save");
		btnSave.addActionListener(controller);
		panelRight.add(btnSave);

		add(panelLeft, BorderLayout.WEST);
		add(panelRight, BorderLayout.EAST);
	}
}
