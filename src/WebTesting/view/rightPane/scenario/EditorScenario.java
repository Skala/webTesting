/**
 * 
 */
package WebTesting.view.rightPane.scenario;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.scenario.EditorScenarioController;
import WebTesting.enums.RightPaneEditor;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.EditorTest;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.core.Config.*;


/** Trida pro vykresleni editoru scenare
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 2. 2016
 * File: EditorScenario.java
 */
public class EditorScenario extends JPanel implements IRightPane {
	
	
	private static final long serialVersionUID = 1L;
	
	//Obsah okna
	private ControlScenarioUpperPanel upperPanel;
	private ControlScenarioListPanel listPanel;
	private ControlScenarioLowerPanel lowerPanel;
	
	private JScrollPane scrollPaneTextArea;
	private JScrollPane infoPanelScrollPane;
	
	private JPanel textControlPanel;
	private JPanel textAreaAndInfoPanel;

	private JSplitPane splitPaneMiddle;

	private JTextField infoTF;
	private JTextArea textArea;
	
	
	/**
	 * Konstruktor editoru pro scenare
	 */
	public EditorScenario() {
		initComponents();
	}
	
	
	/**
	 * Metoda, ktera vytvori cely editor
	 */
	private void initComponents() {
		
		textArea = new JTextArea();
		
		EditorScenarioController controller = new EditorScenarioController(this);
		MainWindow.addController(controller);
		
		setLayout(new GridLayout(1, 1, 0, 0));
				
		textControlPanel = new JPanel();
		textControlPanel.setLayout(new BorderLayout(0, 0));
		add(textControlPanel);
		
		upperPanel = new ControlScenarioUpperPanel();
		upperPanel.getTfScenarioName().setColumns(40);
		lowerPanel = new ControlScenarioLowerPanel();
		
		textAreaAndInfoPanel = new JPanel(new BorderLayout());
		
		
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
		scrollPaneTextArea = new JScrollPane(textArea);
		
		infoTF = new JTextField(getLabel("scenarioInformation"));
		infoTF.setEditable(false);
		infoPanelScrollPane = new JScrollPane(infoTF);

		textAreaAndInfoPanel.add(scrollPaneTextArea, BorderLayout.CENTER);
		textAreaAndInfoPanel.add(infoPanelScrollPane, BorderLayout.SOUTH);
		
	
		listPanel = new ControlScenarioListPanel();
		listPanel.setMinimumSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.2));
				
		splitPaneMiddle = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, textAreaAndInfoPanel, listPanel);
		splitPaneMiddle.setResizeWeight(0.8);
		
		textControlPanel.add(upperPanel, BorderLayout.NORTH);
		textControlPanel.add(splitPaneMiddle, BorderLayout.CENTER);
		textControlPanel.add(lowerPanel, BorderLayout.SOUTH);
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	public JTextField getInfoTF() {
		return infoTF;
	}
			
	public JTextArea getTextArea() {
		return textArea;
	}


	@Override
	public String getPaneTitle() {
		return RightPaneEditor.EDITOR_SCENARIO.getTitle();
	}
}
