/**
 * 
 */
package WebTesting.view.rightPane.scenario;

import static WebTesting.controller.UtilityClass.getLabel;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.UnexpectedException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;


import WebTesting.Interfaces.IListItem;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.scenario.ControlScenarioListPanelController;
import WebTesting.enums.Controllers;
import WebTesting.enums.scenarios.Actions;
import WebTesting.enums.scenarios.Parameters;
import WebTesting.exceptions.UniverzalException;
import WebTesting.view.MainWindow;

/** Trida, ktere zobrazuje panel se seznamy u editoru scenare
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 3. 2016
 * File: ControlScenarioListPanel.java
 */
@SuppressWarnings("serial")
public class ControlScenarioListPanel extends JPanel {
	
	
	private ArrayList<JList<IListItem>> lists; 
	private JScrollPane scrollActionPane, scrollVariablePane;

	/**
	 * Konstruktor
	 */
	public ControlScenarioListPanel() {
		initComponents();
	}

	
	/**
	 * Metoda pro nastaveni tohoto panelu
	 */
	private void initComponents() {
		
		ControlScenarioListPanelController controller = new ControlScenarioListPanelController(this);
		MainWindow.addController(controller);
		
		lists = new ArrayList<JList<IListItem>>();
		setLayout(new GridLayout(0, 1, 0, 0));

		JList<IListItem> listAction = new JList<IListItem>((IListItem[]) Actions.values());
		listAction.addMouseListener(controller);
		scrollActionPane = new JScrollPane(listAction);
		JLabel lblActions = new JLabel(getLabel("typesOfActions"));
		scrollActionPane.setColumnHeaderView(lblActions);
		add(scrollActionPane);
				
		JList<IListItem> listParams = new JList<IListItem>((IListItem[]) Parameters.values());
		listParams.addMouseListener(controller);
		scrollVariablePane = new JScrollPane(listParams);
		JLabel lblParams = new JLabel(getLabel("parameters"));
		scrollVariablePane.setColumnHeaderView(lblParams);
		add(scrollVariablePane);	
		
		lists.add(listAction);
		lists.add(listParams);
	}
	
	
	/*-------------------------
	 	Pristupove metody
	 ------------------------*/
	
	public JList<IListItem> getListAction() {
		return (lists == null) ? null : lists.get(0);
	}
	
	public JList<IListItem> getListParams() {
		return (lists == null) ? null : lists.get(1);
	}
	
	public ArrayList<JList<IListItem>> getLists() {
		return lists;
	}
}
