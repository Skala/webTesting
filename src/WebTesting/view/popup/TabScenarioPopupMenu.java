package WebTesting.view.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.leftPane.tabs.TabScenarioController;
import WebTesting.enums.Controllers;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabScenario;

/** Trida, ktera vytvari vyskakovaci okno pri kliknuti praveho tlacitka mysi do stromu se scenari
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 20. 2. 2016
 * File: TreeScenarioPopupMenu.java
 */
@SuppressWarnings("serial")
public class TabScenarioPopupMenu extends JPopupMenu implements ActionListener{
	
	private JMenuItem miDelete;
	private JMenuItem miRename;
	
	/**
	 * Konstruktor tohoto menu
	 */
	public TabScenarioPopupMenu() {
		initComponents();
	}
	
	/**
	 * Metoda pro vytvoreni vyskakovaciho menu
	 */
	private void initComponents() {
		miDelete = new JMenuItem(UtilityClass.getLabel("delete"));
		miDelete.setActionCommand("delete");
		miDelete.addActionListener(this);
		add(miDelete);
		
		miRename = new JMenuItem(UtilityClass.getLabel("rename"));
		miRename.setActionCommand("rename");
		miRename.addActionListener(this);
		add(miRename);
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		TreeNode node;
		
		switch(e.getActionCommand()){
		case "delete":
			node = (TreeNode) ((TabScenario) ((TabScenarioController) MainWindow.getController(Controllers.TAB_SCENARIO_CTRL)).getView()).getTreeScenario().getLastSelectedPathComponent();
			ButtonActions.deleteNodeFromTree(node);
			break;
		case "rename":
			node = (TreeNode) ((TabScenario) ((TabScenarioController) MainWindow.getController(Controllers.TAB_SCENARIO_CTRL)).getView()).getTreeScenario().getLastSelectedPathComponent();
			ButtonActions.renameNode(node);
			break;
		}
	}
}
