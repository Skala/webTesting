/**
 * 
 */
package WebTesting.view.popup;

import static WebTesting.controller.UtilityClass.showInfoNotification;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import org.jdom2.Element;

import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.enums.Controllers;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.EditorTest;
import WebTesting.view.rightPane.test.source.EditorTestSource;
import WebTesting.view.rightPane.test.source.modify.ModifyTestWindow;

/** Trida, ktera vytvari vyskakovaci okno pri kliknuti praveho tlacitka mysi do editoru testu
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 20. 4. 2016
 * File: editTestPopupMenu.java
 */
@SuppressWarnings("serial")

public class ModifyTestPopupMenu extends JPopupMenu {
	
	private JMenuItem menuItemEditCommand;
	private JMenuItem menuItemEditTest;
	
	/**
	 * Konstruktor pro vytvoreni okna
	 */
	public ModifyTestPopupMenu() {
		initComponents();
	}
	
	/**
	 * Metoda, ktera vytvori komponenty v tomto okne
	 */
	private void initComponents() {
		menuItemEditCommand = new JMenuItem(UtilityClass.getLabel("editCommand"));
		menuItemEditCommand.addActionListener(new MenuItemEditCommandActionListener());
		add(menuItemEditCommand);
		menuItemEditTest = new JMenuItem(UtilityClass.getLabel("editTest"));
		menuItemEditTest.addActionListener(new MenuItemEditTestActionListener());
		add(menuItemEditTest);
	}
	
	
	/*------------------------
	 	Listenery
	 -----------------------*/
	/** Posluchac, ktery reaguje na kliknuti na polozku upravit prikaz ve vyskakovacim menu
	 * @author Pavel Skala
	 * @version 1.0 
	 * 
	 * Created on: 2. 5. 2016
	 * File: ModifyTestPopupMenu.java
	 */
	private class MenuItemEditCommandActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			
			if(((EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL)).getModel() == null){
				JOptionPane.showMessageDialog(
						((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()), 
						"Nejdrive musite vybrat test.");	
			}
			else{
				EditorTestSource editor = ((EditorTestSource) MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).getView());
				int position = editor.getTextArea().getCaretPosition();
				String text = editor.getTextArea().getText().substring(0, position);
				int numberOfCommand = UtilityClass.getNumberOfContents(text, ';');
				EditTextAreaAction(ae, numberOfCommand);
				
			}
		}
	}
	
	/** Posluchac pro kliknuti na polozku upravit test
	 * @author Pavel Skala
	 * @version 1.0 
	 * 
	 * Created on: 2. 5. 2016
	 * File: ModifyTestPopupMenu.java
	 */
	private class MenuItemEditTestActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			EditTextAreaAction(ae, -1);
		}
	}
	
	/*----------------------
	 	Pomocne metody
	 ----------------------*/
	
	/**  Metoda ktera se vola po kliknuti do nektere z polozek vyskakujiciho menu
	 * @param ae
	 * @param numberOfCommand
	 */
	private void EditTextAreaAction(ActionEvent ae, int numberOfCommand){
		
		EditorTestController testCtrl = (EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL);
		
		if(testCtrl.getModel() == null){
			showInfoNotification(UtilityClass.getLabel("youHaveToSelectTheTest"));
		}
		else{
			try {
				Element currElement = testCtrl.getModel().getRootElement();
				if(numberOfCommand != -1){
					currElement = currElement.getChildren().get(numberOfCommand);
					new ModifyTestWindow(currElement, numberOfCommand);
				}
				else{
					new ModifyTestWindow(currElement);
				}
			} catch (IndexOutOfBoundsException e) {
				showInfoNotification(UtilityClass.getLabel("youHaveNotSelectAnyCommand"));
				e.printStackTrace();
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}	
}
