/**
 * 
 */
package WebTesting.view.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.controller.leftPane.tabs.TabScenarioController;
import WebTesting.controller.leftPane.tabs.TabTestController;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.enums.Controllers;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.leftPane.tabs.TabScenario;
import WebTesting.view.leftPane.tabs.TabTest;
import WebTesting.view.rightPane.test.EditorTest;

/** Trida, ktera vytvari vyskakovaci okno pri kliknuti praveho tlacitka mysi do stromu s testy
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 2. 2. 2016
 * File: TreePopupMenu.java
 */
public class TabTestPopupMenu extends JPopupMenu implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JMenuItem subMenuItemAdd;
	private JMenuItem subMenuItemDelete;
	private JMenuItem subMenuItemShow;
	private JMenuItem menuItemRename;

	/**
	 * Konstruktor vyskakovaciho menu
	 */
	public TabTestPopupMenu() {
		initComponents();
	}
	
	/**
	 * Metoda pro nastaveni tohoto menu
	 */
	private void initComponents() {
		
		subMenuItemAdd = new JMenuItem(UtilityClass.getLabel("addToList"));
		subMenuItemAdd.setActionCommand("addToList");
		subMenuItemAdd.addActionListener(this);
		
		subMenuItemShow = new JMenuItem(UtilityClass.getLabel("show"));
		subMenuItemShow.setActionCommand("show");
		subMenuItemShow.addActionListener(this);
	
		subMenuItemDelete = new JMenuItem(UtilityClass.getLabel("delete"));
		subMenuItemDelete.setActionCommand("delete");
		subMenuItemDelete.addActionListener(this);

		menuItemRename = new JMenuItem(UtilityClass.getLabel("rename"));
		menuItemRename.setActionCommand("rename");
		menuItemRename.addActionListener(this);
		
		add(subMenuItemShow);
		add(subMenuItemAdd);
		add(subMenuItemDelete);
		add(menuItemRename);
	}
		
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		System.out.println(e.getActionCommand());
		TreeNode node = (TreeNode) ((TabTest) ((TabTestController) MainWindow.getController(Controllers.TAB_TEST_CTRL)).getView()).
				getTreeTest().getLastSelectedPathComponent();
		
		switch(e.getActionCommand()){
		
		case "addToList":
//			node = (TreeNode) ((TabTest) ((TabTestController) MainWindow.getController(Controllers.TAB_TEST_CTRL)).getView()).
//				getTreeTest().getLastSelectedPathComponent();
			ArrayList<TreeNode> list = TreePanelController.getAllChildren(node);
			
			
			
			ButtonActions.addTestsToProceed(list);
			break;
		case "show":
//			node = (TreeNode) ((TabTest) ((TabTestController) MainWindow.getController(Controllers.TAB_TEST_CTRL)).getView()).
//				getTreeTest().getLastSelectedPathComponent();
			((EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getView()).setModel(node);
			break;
		case "delete":
//			node = (TreeNode) TabTest.getInstance().getTreeTest().getLastSelectedPathComponent();
			ButtonActions.deleteNodeFromTree(node);
			break;
		case "rename":
//			node = (TreeNode) TabTest.getInstance().getTreeTest().getLastSelectedPathComponent();
			ButtonActions.renameNode(node);
			break;
		}
	}

}
