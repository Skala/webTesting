/**
 * 
 */
package WebTesting.view.leftPane.tabs;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import WebTesting.controller.UtilityClass;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.controller.leftPane.tabs.TabTestController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 14. 12. 2016
 * File: TabTest.java
 */

public class TabTest extends JPanel {
	
	private JTree treeTest;
	
	public TabTest() {
		initComponents();
	}
	
	
	private void initComponents() {
		
		TabTestController controller = new TabTestController(this);
		MainWindow.addController(controller);
		
		this.setLayout(new BorderLayout(0, 0));

		this.treeTest = new JTree();
		this.treeTest.addKeyListener(controller);
		this.treeTest.addMouseListener(controller);
		add(treeTest, BorderLayout.CENTER);
		
		this.treeTest.setEditable(false);
		
		TreeNode rootTest = new TreeNode("root");
		rootTest.setParentTreeNode(null);
		rootTest.setFilePath(UtilityClass.getPathtotests());
		rootTest.setTypeOfNode(Config.TYPE_TEST);
		treeTest.setModel(new DefaultTreeModel(rootTest));
		
		((TreePanelController)MainWindow.getController(Controllers.TREE_PANEL_CTRL)).populateTree(treeTest);
		//TODO asi smazat
		((TreePanelController)MainWindow.getController(Controllers.TREE_PANEL_CTRL)).reloadTree(treeTest);
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	/**
	 * @return the treeTest
	 */
	public JTree getTreeTest() {
		return treeTest;
	}
}
