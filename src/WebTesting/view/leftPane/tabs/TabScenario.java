/**
 * 
 */
package WebTesting.view.leftPane.tabs;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.plaf.metal.MetalIconFactory.TreeControlIcon;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import WebTesting.Interfaces.ITreeTab;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.controller.leftPane.tabs.TabScenarioController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 14. 12. 2016
 * File: TabScenario.java
 */
@SuppressWarnings("serial")
public class TabScenario extends JPanel implements ITreeTab {
	
	private JTree treeScenario;

	/**
	 * Create the panel.
	 */
	public TabScenario() {
		initComponents();
	}
	
	
	private void initComponents() {
		
		TabScenarioController controller = new TabScenarioController(this);
		MainWindow.addController(controller);
		
		this.setLayout(new BorderLayout(0, 0));
		this.treeScenario = new JTree();
		
		this.treeScenario.addKeyListener(controller);
		this.treeScenario.addMouseListener(controller);
		this.treeScenario.setEditable(false);

		TreeNode rootScenario = new TreeNode("root");
		rootScenario.setParentTreeNode(rootScenario);
		rootScenario.setFilePath(UtilityClass.getPathtoscenarios());
		rootScenario.setTypeOfNode(Config.TYPE_SCENARIO);
		
		treeScenario.setModel(new DefaultTreeModel(rootScenario));
		
		((TreePanelController)MainWindow.getController(Controllers.TREE_PANEL_CTRL)).populateTree(treeScenario);
		
		this.add(treeScenario, BorderLayout.CENTER);
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	
	public JTree getTreeScenario() {
		return treeScenario;
	}
}
