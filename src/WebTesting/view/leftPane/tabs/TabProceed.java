/**
 * 
 */
package WebTesting.view.leftPane.tabs;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JPanel;

import WebTesting.controller.leftPane.tabs.TabProceedController;
import WebTesting.model.TestData;
import WebTesting.model.leftPane.ListProceedRenderer;
import WebTesting.view.MainWindow;


/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 14. 12. 2016
 * File: TabProceed.java
 */
public class TabProceed extends JPanel {
	
	
	private JList<TestData> listProceed;

	/**
	 * Create the panel.
	 */
	public TabProceed() {
		initComponents();
	}
	
	private void initComponents() {
		TabProceedController controller = new TabProceedController(this);
		MainWindow.addController(controller);
		
		this.setLayout(new BorderLayout(0, 0));
		
		listProceed = new JList<TestData>();
		listProceed.setCellRenderer(new ListProceedRenderer());
		
		//Posluchac pro seznam, ve kterem jsou zobrazeny testy, ktere jsou v tabulce testu pripravenych pro spusteni
		controller.populateListProceed(listProceed);
		
		this.listProceed.addMouseListener(controller);
		
		this.add(listProceed, BorderLayout.CENTER);
	}
		
	
	/*************************
	 * Pristupove metody
	 ************************/
	
	/**
	 * @return the listProceed
	 */
	public JList<TestData> getListProceed() {
		return listProceed;
	}
	
}
