/**
 * 
 */
package WebTesting.view.leftPane;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.core.Config.DIMENSION_OF_APP;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import WebTesting.controller.UtilityClass;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabProceed;
import WebTesting.view.leftPane.tabs.TabScenario;
import WebTesting.view.leftPane.tabs.TabTest;

/** Trida, ktera zobrazuje jednu ze dvou klicovych ploch teto aplikace. Tato plocha zobrazuje stromovou strukturu s testy a scenari
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 2. 2. 2016
 * File: TreePanel.java
 */
@SuppressWarnings("serial")
public class TreePanel extends JPanel {

	private JTabbedPane tabbedPane;
	
	/**
	 * konstruktor panelu.
	 */
	public TreePanel() {
		initComponents();
	}
	
	
	/**
	 * Metoda, ktera vytvori tento panel
	 */
	private void initComponents() {

		TreePanelController controller = new TreePanelController(this);
		MainWindow.addController(controller);
		
		this.setMinimumSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.2));
		this.setMaximumSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.25));
		
		this.setBackground(Color.LIGHT_GRAY);		
		this.setLayout(new GridLayout(1, 1, 0, 0));

		JScrollPane scrollTabTest = new JScrollPane(new TabTest());
		JScrollPane scrollTabScenario = new JScrollPane(new TabScenario());
		JScrollPane scrollTabProceed = new JScrollPane(new TabProceed());
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		tabbedPane.addTab(getLabel("tests"), null, scrollTabTest, null);
		tabbedPane.addTab(getLabel("scenarios"), null, scrollTabScenario, null);
		tabbedPane.addTab(getLabel("proceedTests"), null, scrollTabProceed, null);
		
		tabbedPane.addChangeListener(controller);
		
		this.add(tabbedPane);
		
	}

	/*
	 *Pristupove metody 
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}
}
