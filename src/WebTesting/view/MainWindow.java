package WebTesting.view;

import static WebTesting.core.Config.DIMENSION_OF_APP;
import static WebTesting.core.Config.MAIN_WINDOW_TITLE;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;

import WebTesting.Interfaces.IController;
import WebTesting.controller.DataController;
import WebTesting.controller.MainWindowController;
import WebTesting.controller.UtilityClass;
import WebTesting.enums.Controllers;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.rightPane.RightPane;


/** Hlavni trida cele aplikace, ktera ji zobrazuje.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 1. 2. 2016
 * File: MainWindow.java
 */
public class MainWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private static DataController data = new DataController();  
		
	private MainMenu menu;
	private TreePanel treePane;
	private RightPane rightPane;
	private JSplitPane splitPane;
	
	/**
	 * Spousteci metoda aplikace.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new MainWindow();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Konstruktor aplikace
	 */
	public MainWindow() {
		initComponents();
		
		String errMsg = ((MainWindowController) getController(Controllers.MAIN_WINDOW_CTRL)).getErrorMessage(); 
		if(errMsg != null){
			JOptionPane.showMessageDialog(this, "Nasledujici soubory nebyly nacteny: \n" + errMsg, "Varovani", JOptionPane.INFORMATION_MESSAGE);
		}
		this.setVisible(true);
	}
		
		
	/**
	 * Metoda, ktera vykresli aplikaci
	 */
	private void initComponents() {
		
		MainWindowController controller = new MainWindowController(this);
		addController(controller);
		
		this.setTitle(MAIN_WINDOW_TITLE);
		this.setSize(DIMENSION_OF_APP);
		this.setLocationRelativeTo(null);
		this.setMinimumSize(UtilityClass.getDimension(DIMENSION_OF_APP, 0.85));
		this.setIconImage(Toolkit.getDefaultToolkit().createImage("resources/webTest.png"));
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.addWindowListener(controller);

		this.getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		
		createMenu(controller);
		createSplitPane(controller);
	}
	
	/**
	 * Metoda, ktera vytvori v aplikaci menu
	 */
	private void createMenu(IController ctrl) {
		this.menu = new MainMenu();
		this.setJMenuBar(this.menu);
	}

	/**
	 * Metoda pro vytvoreni panelu, ve kterem se budou vyskytovat stromy a editory
	 */
	private void createSplitPane(IController ctrl){
		this.rightPane = new RightPane();
		this.treePane = new TreePanel();
		this.splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.treePane, this.rightPane);
		this.splitPane.setResizeWeight(0.2);
		this.getContentPane().add(this.splitPane);
	}
	
	public static IController getController(Controllers ctrl){
		return data.getController(ctrl);
	}
	
	public static void addController(IController ctrl){
		data.addController(ctrl);
	}
}
