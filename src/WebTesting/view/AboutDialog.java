package WebTesting.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import WebTesting.enums.Controllers;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 28. 2. 2016
 * File: AboutDialog.java
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog {
	
	private JPanel contentPanel;
	private JPanel panel;
	private JButton btnOk;
	private JLabel lblDescription;
	
	private JDialog dialog;
	private Dimension sizeOfAbout = new Dimension(320, 280);
	
	/**
	 * Create the dialog.
	 */
	public AboutDialog() {
		initComponents();
	}
	private void initComponents() {
		
		//TODO 	doimplementovat a implementovat al
		dialog = this;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(sizeOfAbout);
		setLocationRelativeTo((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView());
		setModal(true);
		
		getContentPane().setLayout(new BorderLayout());
		
		contentPanel = new JPanel();
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel = new JPanel();
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		lblDescription = new JLabel("\"Okenni aplikace pro testovani webovych projektu\"");
		contentPanel.add(lblDescription);
		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		btnOk = new JButton("OK");
		btnOk.addActionListener(new BtnOkActionListener());
		panel.add(btnOk);
		
		setVisible(true);
	}

	private class BtnOkActionListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			dialog.dispose();
		}
	}
}
