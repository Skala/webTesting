/**
 * 
 */
package WebTesting.view;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showInfoNotification;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import WebTesting.controller.MainMenuController;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.rightPane.proceed.EditorProceedController;
import WebTesting.view.rightPane.proceed.EditorProceed;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 3. 2017
 * File: MainMenu.java
 */
public class MainMenu extends JMenuBar {

	/**
	 * File item in menu
	 */
	private JMenu menuFile;
	private JMenuItem menuItemExit;
	
	/**
	 * Options item in menu
	 */
	private JMenu menuOptions;
	private JMenuItem menuItemCountsOfTests;
	private JMenuItem menuItemLanguage;
	
	/**
	 * Help item in menu
	 */
	private JMenu menuHelp;
	private JMenuItem menuItemAbout;
	
	
	public MainMenu(){
		initComponents();
	}

	/**
	 * 
	 */
	private void initComponents() {
		
		MainMenuController controller = new MainMenuController(this);
		MainWindow.addController(controller);
		
		//Bookmark file
		menuFile = new JMenu(getLabel("file"));
		this.add(menuFile);

		menuItemExit = new JMenuItem(getLabel("close"));
		menuItemExit.setActionCommand("exit");
		menuItemExit.addActionListener(controller);
		menuFile.add(menuItemExit);


		//Bookmark options
		menuOptions = new JMenu(getLabel("options"));
		this.add(menuOptions);

		menuItemCountsOfTests = new JMenuItem(getLabel("countOfRunningTests"));
		menuItemCountsOfTests.setActionCommand("countOfTests");
		menuItemCountsOfTests.addActionListener(controller);
		menuOptions.add(menuItemCountsOfTests);

		menuItemLanguage = new JMenuItem(getLabel("chooseLanguage"));
		menuItemLanguage.setActionCommand("lang");
		menuItemLanguage.addActionListener(controller);
		menuOptions.add(menuItemLanguage);
	}
}
