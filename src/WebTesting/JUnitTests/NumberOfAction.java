/**
 * 
 */
package WebTesting.JUnitTests;

import org.junit.Test;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 2. 5. 2016
 * File: NumberOfAction.java
 */
public class NumberOfAction {

	@Test
	public void test() {
		String text = "click , clicked  , ";
		int i = getNumberOfActionsInLine(text);
		System.out.println(i);
	}
	
	public static int getNumberOfActionsInLine(String line){

		line = line.replace(" ", "");
		int count = 0;
		String [] actions = {"click", "clickAndWait"};
		for(int i = 0 ; i < actions.length ; i++){
			String action = (actions[i] + ",").toLowerCase();
			if(line.toLowerCase().contains(action)){
				System.out.println(action);
				count++;
			}
		}
		return count;
		
	}

}
