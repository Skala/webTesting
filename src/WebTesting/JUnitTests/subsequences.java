/**
 * 
 */
package WebTesting.JUnitTests;

import org.junit.Test;

import WebTesting.controller.UtilityClass;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 28. 4. 2016
 * File: subsequences.java
 */
public class subsequences {

	@Test
	public void test() {
		String text = "##pozice kontakt##=xpath=##//a[@title=\"Kontakt\"]";
		int i = UtilityClass.getNumberOfSubSequences(text, "##");
		System.out.println(i);
	}

}
