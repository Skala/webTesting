/**
 * 
 */
package WebTesting.JUnitTests;

import java.util.List;

import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.Text;
import org.junit.Test;

import WebTesting.controller.XmlOperation;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 4. 2016
 * File: Element.java
 */
public class ElementTest {

	@Test
	public void test() {
		
		Element root = new Element("root").setText("ahoj");
		root.setAttribute("name", "pokus");
		
		Element el1 = new Element("el").setText("eeee1");
		el1.setAttribute("priznak", "ahoj");
		Element el2 = new Element("el").setText("eeee2");
		el2.setAttribute("priznak", "ahoj");
		Element el3 = new Element("el").setText("eeee3");
		el3.setAttribute("priznak", "ahoj");
		Element el4 = new Element("el").setText("eeee4");
		Element el5 = new Element("el").setText("eeee5");
		
		Text t1 = new Text("ahoj1");
		Text t2 = new Text("ahoj2");
		Text t3 = new Text("ahoj3");
		
		
		root.addContent(el1);
		root.addContent(t1);
		root.addContent(el2);
		root.addContent(el3);
		root.addContent(t2);
		root.addContent(t3);
		root.addContent(el4);
		root.addContent(el5);
		
		List<Content> els = root.getContent();
		
		for(int i = 0; i < els.size(); i++){
			System.out.println(els.get(i).toString());
		}
		
		System.out.println();
		
		int index = XmlOperation.getElementPositionFromContent(root, 4);
		
		System.out.println(index);
		
		
//		System.out.println();
////		
//		Element el = new Element("newel").setText("new eeee");
//		
//		root.addContent(5, el);
//	
//		
//		els = root.getContent();
//			System.out.println("new child");
//		for(int i = 0; i < els.size(); i++){
//			System.out.println(els.get(i).toString());
//		}
		
		
	}

}
