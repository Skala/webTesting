/**
 * 
 */
package WebTesting.JUnitTests;

import org.junit.Test;

import WebTesting.controller.UtilityClass;
import WebTesting.core.Config;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 15. 4. 2016
 * File: Split.java
 */
public class Split {

	@Test
	public void test() {
		//String ret = "##pozi,ce##=id=##ahoj,dd##, ##cont##=##muj, text##;";
		
		String ret = "##pozice odkryti##=xpath=##//div[@class=\"page-top\"]/div[4]/div/div[2]##, ##js##=style=##display: block##";
		String [] p1 = UtilityClass.mySplit(ret, ",");
		for(int i = 0 ; i < p1.length; i++){
			System.out.println("   " + p1[i]);
			String [] p2 = UtilityClass.mySplit(p1[i], "=");
			UtilityClass.removeString(p2, Config.constantTag);
			for(int j = 0; j < p2.length; j++){
				System.out.println(p2[j]);
			}
		}
		
	}
}
