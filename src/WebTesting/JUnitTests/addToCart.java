/**
 * 
 */
package WebTesting.JUnitTests;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 19. 4. 2016
 * File: addToCart.java
 */
public class addToCart {

	@Test
	public void test() {

//		DesiredCapabilities caps= new DesiredCapabilities();
//		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "");
//		caps.setJavascriptEnabled(true);
		
		File file = new File("C:\\Program Files\\Java\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		System.setProperty("phantomjs.binary.path", file.getAbsolutePath());
		
		
		WebDriver driver = new PhantomJSDriver(); //new MyHtmlUnitDriver(BrowserVersion.FIREFOX_38, true);
		
		
//		DesiredCapabilities caps= new DesiredCapabilities();
//		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "C:/Program Files/Java/phantomjs-2.1.1-windows/bin/phantomjs.exe");
//		caps.setJavascriptEnabled(true);
//		
//		WebDriver driver = new PhantomJSDriver(caps);
		
		//WebDriver driver = new MyHtmlUnitDriver(BrowserVersion.FIREFOX_38, true);
		//WebDriver driver = new FirefoxDriver();
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement element = null;
		
		
		driver.get("https://www.chickie.cz");
		
		element = driver.findElement(By.cssSelector("div.productPreviewGallery"));
		element.click();
		System.out.println("1 ok");
		
		element = driver.findElement(By.xpath("//div[@id=\"barva\"]/div[@class=\"dimSelectItem\"]"));
		element.click();
		System.out.println("2 ok");
		
		element = driver.findElement(By.xpath("//div[@id=\"velikost\"]/div[@class=\"dimSelectItem\"]"));
		element.click();
		
		element = driver.findElement(By.id("action"));
		element.click();
		System.out.println("3 ok");
		
		driver.close();
		
	}

}
