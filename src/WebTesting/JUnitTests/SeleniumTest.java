/**
 * 
 */
package WebTesting.JUnitTests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 3. 2017
 * File: SeleniumTest.java
 */
public class SeleniumTest {

	@Test
	public void test() {

		
		WebDriver driver;
		WebElement element;
		
		System.setProperty("webdriver.opera.driver", "c:\\Program Files\\Java\\gecko\\opera\\operadriver.exe");
		
//		DesiredCapabilities caps = new FirefoxOptions()
//		       // For example purposes only
//		      .setProfile(new FirefoxProfile())
//		      .addTo(DesiredCapabilities.firefox());
//		
//		driver = new FirefoxDriver(caps);
		driver = new OperaDriver();
		
        // And now use this to visit Google
        driver.get("http://www.google.com");
        // Alternatively the same thing can be done like this
        // driver.navigate().to("http://www.google.com");

        // Find the text input element by its name
        element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());
        
        // Google's search is rendered dynamically with JavaScript.
        // Wait for the page to load, timeout after 10 seconds
        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith("cheese!");
            }
        });

        // Should see: "cheese! - Google Search"
        System.out.println("Page title is: " + driver.getTitle());
        
        //Close the browser
        //driver.quit();
		
	}

}
