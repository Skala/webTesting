/**
 * 
 */
package WebTesting.controller;

import static WebTesting.controller.UtilityClass.*;
import static WebTesting.controller.XmlOperation.updateNodeToXml;
import static WebTesting.controller.XmlOperation.writeToXml;
import static WebTesting.core.Config.*;


import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.jdom2.Element;

import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.controller.rightPane.proceed.ListProceedController;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.SemanticException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownElementTypeException;
import WebTesting.exceptions.UnknownTypeOfLocator;
import WebTesting.exceptions.UnknownTypeOfNodeException;
import WebTesting.exceptions.UnknownTypeOfParameter;
import WebTesting.exceptions.WarningException;
import WebTesting.model.DataNode;
import WebTesting.model.TestData;
import WebTesting.model.TreeNode;
import WebTesting.model.rightPane.proceed.TableProceedModel;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.leftPane.tabs.TabProceed;
import WebTesting.view.rightPane.RightPane;
import WebTesting.view.rightPane.proceed.EditorProceed;
import WebTesting.view.rightPane.scenario.ControlScenarioUpperPanel;
import WebTesting.view.rightPane.scenario.EditorScenario;
import WebTesting.view.rightPane.test.EditorTest;
import WebTesting.view.rightPane.test.modules.ControlTestMiddlePanel;
import WebTesting.view.rightPane.test.modules.ControlTestUpperPanel;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/** Trida, ktera provadi operace vyvolane stisknutim nektereho z tlacitek, ktera jsou v aplikaci uvedena.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 4. 2016
 * File: ButtonActions.java
 */
public class ButtonActions {

	private static EditorTestSource editorTestSource = (EditorTestSource) MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).getView();
	private static TreePanelController treePanelController = (TreePanelController) MainWindow.getController(Controllers.TREE_PANEL_CTRL);
	private static MainWindow mainWindow = (MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView();
	private static EditorScenario editorScenario = (EditorScenario) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getView();
	private static EditorTest editorTest = (EditorTest) MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getView();
	private static EditorProceed editorProceed = (EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView();
	private static RightPane rightPane = (RightPane) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL).getView();
	private static RightPaneController rightPaneController = (RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL);
	private static ControlScenarioUpperPanel controlScenarioUpperPanel = (ControlScenarioUpperPanel) MainWindow.getController(Controllers.SCENARIO_UPPER_PANEL_CTRL).getView();
	private static ControlTestUpperPanel controlTestUpperPanel = (ControlTestUpperPanel ) MainWindow.getController(Controllers.TEST_UPPER_PANEL_CTRL).getView();
	private static ControlTestMiddlePanel controlTestMiddlePanel = (ControlTestMiddlePanel) MainWindow.getController(Controllers.TEST_MIDDLE_PANEL_CTRL).getView();
	private static TabProceed tabProceed = (TabProceed) MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView();
	
	
	/**
	 * Explicitni privatni konstruktor pro zamezeni vytvoreni instanece teto tridy
	 */
	private ButtonActions(){}
	
	
	
	/** Metoda, ktera provadi praci za tlacitko ulozit v editacnich panelech testu a scenare.
	 * @param node Ukladany uzel.
	 */
	public static synchronized void updateButton(TreeNode node){
		if(node != null){
			setInfoMessage(node, "", COLOR_WHITE);
			//pokud uz byl uzel ulozen
			if(node.getFilePath() != null){
				try {
					File file = new File(node.getFilePath());
									
					setName(node, file.getName());
					updateNodeToXml(node);
					
					if(file.exists()){
						file.delete();
					}

					System.out.println(node.getData().elementsToString());
					writeToXml(node, file);
					ButtonActions.setInfoMessage(node, getLabel("successfullyUpdated"), Config.COLOR_GREEN);
				} catch (UnknownElementTypeException | UnknownTypeOfNodeException | UnknownTypeOfParameter
						| SemanticException | UnknownTypeOfLocator | IndexOutOfBoundsException | NotYetImplementedException | UniverzalException e) {
					setInfoMessage(node, e.getMessage(), COLOR_RED);
					
					// pri padu nastavit data do elemnetu
					node.getRootElement().removeChild(UtilityClass.TEXT_AREA);
					node.getRootElement().addContent(new Element(UtilityClass.TEXT_AREA).setText(editorTestSource.getTextArea().getText()));
					
					e.printStackTrace();
				}
				finally {
					treePanelController.reloadTreeAfterModify(node);
				}
			}
			else{
				showAlertNotification(getLabel("nodeHasNotBeenSavedYet") + " " + getLabel("saveIt"), getLabel("saveNode"));
			}
		
		}
		else{
			showInfoNotification(getLabel("selectNodeForUpdate"), getLabel("nodeHasNotBeenSelected"));
		}
	}
	
	
	
	/** Metoda, ktera prevede test na scenar a nasledne ho ulozi
	 * @param node Uzel testu, ktery prevadime
	 * @throws IndexOutOfBoundsException
	 * @throws UnknownElementTypeException
	 * @throws UnknownTypeOfNodeException
	 * @throws UnknownTypeOfParameter
	 * @throws SemanticException
	 * @throws UnknownTypeOfLocator
	 * @throws NotYetImplementedException
	 * @throws UniverzalException
	 */
	public static void saveScenarioFromTest(TreeNode node) throws IndexOutOfBoundsException, UnknownElementTypeException, UnknownTypeOfNodeException, UnknownTypeOfParameter, SemanticException, UnknownTypeOfLocator, NotYetImplementedException, UniverzalException{
		
		String path;
		try {
			path = setPath(node);
		} catch (Exception e1) {
			e1.getMessage();
			return;
		}
		
		String hasName = getName(node);
		
		JFileChooser fc = choseLocationtoSave(path, hasName);
		int retVal = fc.showSaveDialog(mainWindow);

		File file = null;
		if(retVal == JFileChooser.APPROVE_OPTION){

			try {
				file = createFile(fc);
				node.getData().setName(file.getName());
				updateNodeToXml(node);
				
				if(file.exists()){
					file.delete();
				}
								
				writeToXml(node, file);
		
			} catch ( UnknownTypeOfNodeException | UnknownTypeOfParameter | WarningException
					| SemanticException | UnknownTypeOfLocator |IndexOutOfBoundsException | NotYetImplementedException | UniverzalException e) {
				setInfoMessage(node, e.getMessage(), Config.COLOR_RED);
				e.printStackTrace();
			} catch(UnknownElementTypeException e){
				showInfoNotification(getLabel("nodeNotSaved") + "\n" + e.getMessage(), getLabel("nodeNotSaved"));
				setInfoMessage(node, e.getMessage(), Config.COLOR_RED);
				e.printStackTrace();
				return;
			}
			finally {
				treePanelController.reloadTreeAfterModify(node);
			}
			setInfoMessage(node, getLabel("successfullySaved"), Config.COLOR_GREEN);
		}
		XmlOperation.updateNodeToXml(node);
	}
	
	

	/** Metoda, ktera vytvori soubor na zaklade dat, ktera uzivatel zadal do okna pro ulozeni uzlu
	 * @param fc Instance file chooseru, ze ktereho ziskame nami zvoleny nazev pro uzel
	 * @return Instanci tridy File.
	 * @throws WarningException 
	 * @throws Exception 
	 */
	private static File createFile(JFileChooser fc) throws WarningException  {
		File file = fc.getSelectedFile();
		String name = file.getName();
		
		if(name == null){
			String message = getLabel("fileNameNotSpecified") + "\n" + getLabel("saveFailed");
			showAlertNotification(message, getLabel("fileNotSaved"));
			throw new WarningException(message);
		}
		
		if(!name.contains(".xml")){
			String message = getLabel("fileNotSavedInDesiredFormatXml") + "\n" + getLabel("fileNotSaved");
			showAlertNotification(message, getLabel("fileNotSaved"));
			throw new WarningException(message);
		}
		return file;
	}

	/** Metoda, ktera nastavi uzlu nove jmeno
	 * @param node Uzel, kteremu nastavujeme jmeno
	 * @param name Jmeno, ktere chceme uzlu nastavit
	 */
	public static void setName(TreeNode node, String name) {
		
		name = getValidName(name);
		if(node.getTypeOfNode().equals(TYPE_SCENARIO)){
			if(rightPane.getEditorScenario().isVisible()){
				controlScenarioUpperPanel.getTfScenarioName().setText(name);
				node.getData().setName(name);
				node.getData().getRootElement().setAttribute("name", name);
			}
		}
		else if(node.getTypeOfNode().equals(TYPE_TEST)){
			if(rightPane.getEditorTests().isVisible()){
				controlTestUpperPanel.getTfTitleTest().setText(name);
				node.getData().setName(name);
				node.getRootElement().setAttribute("name", name);
			}
		}
	}

	/**	Metoda, ktera vrati nazev, ktery je uveden u daneho scenare nebo testu
	 * @param type Parametr oznacujici typ uzlu
	 * @return Nazev testu nebo scenare
	 */
	private static String getName(TreeNode node){
		if(node.getTypeOfNode().equals(TYPE_SCENARIO)){
			return controlScenarioUpperPanel.getTfScenarioName().getText();
		}
		else if(node.getTypeOfNode().equals(TYPE_TEST)){
			return controlTestUpperPanel.getTfTitleTest().getText();
		}
		else{
			return null;
		}
	}
	
	/**	Metoda, ktera provede ulozeni aktualniho uzlu (testu, scenare)
	 * @param node Uzel, ktery ma byt ulozen
	 * @throws NotYetImplementedException 
	 */
	public static synchronized void saveButton(TreeNode node){
		if(node != null){

			setInfoMessage(node, "", COLOR_WHITE);
			
			String path;
			try {
				path = setPath(node);
			} catch (Exception e1) {
				e1.getMessage();
				return;
			}
			
			String hasName = getName(node);
			
			JFileChooser fc = choseLocationtoSave(path, hasName);
			int retVal = fc.showSaveDialog(mainWindow);

			File file = null;
			if(retVal == JFileChooser.APPROVE_OPTION){

				try {
					file = createFile(fc);
					setName(node, file.getName());
					updateNodeToXml(node);
					
					if(file.exists()){
						file.delete();
					}
									
					System.out.println(node.getData().elementsToString());
					writeToXml(node, file);
			
				} catch ( UnknownTypeOfNodeException | UnknownTypeOfParameter | WarningException
						| SemanticException | UnknownTypeOfLocator |IndexOutOfBoundsException | NotYetImplementedException | UniverzalException e) {
					setInfoMessage(node, e.getMessage(), COLOR_RED);
					e.printStackTrace();
					return;
				} catch(UnknownElementTypeException e){
					showInfoNotification(getLabel("nodeNotSaved") + "\n" + e.getMessage(), getLabel("nodeNotSaved"));
					setInfoMessage(node, e.getMessage(), Config.COLOR_RED);
					e.printStackTrace();
					return;
				}
				finally {
					treePanelController.reloadTreeAfterModify(node);
					treePanelController.reloadTree(node);
				}
				setInfoMessage(node, getLabel("successfullySaved"), Config.COLOR_GREEN);
			}
		}
		else{
			showInfoNotification(getLabel("noTestChosenToSave"));
		}
	}
	
	
	/** Metoda, ktera v pripade nejakeho problemu nastavi pozadovane textfieldy
	 * @param node Uzel, nad kterym doslo k chybe
	 * @param e Vyjimka, ktera nastala
	 */
	public static void setInfoMessage(TreeNode node, String message, Color color) {
		String type = node.getTypeOfNode();
		
		switch(type){
		case TYPE_TEST:
			controlTestMiddlePanel.getInfoTF().setText(message);
			controlTestMiddlePanel.getInfoTF().setBackground(color);
			new Timer().schedule(new TimerTask() {
				
				@Override
				public void run() {
					controlTestMiddlePanel.getInfoTF().setBackground(Config.COLOR_WHITE);
				}
			}, 3000);
			
			break;
		
		case TYPE_SCENARIO:
			editorScenario.getInfoTF().setText(message);
			editorScenario.getInfoTF().setBackground(color);
			new Timer().schedule(new TimerTask() {
				
				@Override
				public void run() {
					editorScenario.getInfoTF().setBackground(Config.COLOR_WHITE);
				}
			}, 3000);
			break;
		
		default: 
			try {
				throw new Exception("unknown type of node");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
		
	
	/** Metoda, ktera vrati cestu do korenove slozky pro ulozeni scenaru nebo testu
	 * @return Cesta do hledaneho adresare
	 * @throws Exception 
	 */
	private static String setPath(TreeNode data) throws Exception {
		if(compareString(data.getTypeOfNode(), TYPE_SCENARIO)){
			return UtilityClass.getPathtoscenarios();
		}
		else if(compareString(data.getTypeOfNode(), TYPE_TEST)){
			return UtilityClass.getPathtotests();
		}
		else{
			throw new Exception("Unknown data to save to XML");
		}
	}
	
	/** Metoda, ktera umozni vyber, kam se budou uzlu ukladat
	 * @param path Cesta do zvoleneho adresare
	 * @param hasName Vychozi nazev uzlu
	 * @return Instanci JFileChooser.
	 */
	private static JFileChooser choseLocationtoSave(String path, String hasName) {
		JFileChooser fc;
		if(!UtilityClass.isNullOrEmpty(hasName)){
			fc = new JFileChooser(path);
			hasName = getValidName(hasName);
			fc.setSelectedFile(new File(hasName));
		}
		else{
			fc = new JFileChooser(path);
		}
		return fc;
	}


	
	
	
	
	
	
	
	
	
	/** Metoda, ktera pridat uzly z parametru do tabulky testu pro spusteni
	 * @param list Seznam testu pro pridani do tabulkz pro spusteni
	 */
	public static void addTestsToProceed(ArrayList<TreeNode> list){
		TableProceedModel model = (TableProceedModel)(editorProceed.getTable().getModel());
		for (int i = 0; i < list.size(); i++) {
			removeWhenExists(list.get(i));
			
			if(list.get(i).getRootElement() != null){
				list.get(i).resetAllStatus();
				model.myAddRow(new TestData((TreeNode) list.get(i)));
			}
			
		}
		ListProceedController.populateListProceed(tabProceed.getListProceed());
	}


	/** Metoda, ktera se provadi pri pridavani testu do tabulky pripravenych testu ke spusteni. V pripade, ze se tam test jiz
	 * vyskytuje, tak se prepise
	 * @param node Test, ktery ma byt pridan do tabulkz
	 * @return Rozhodnuti, zde se test v tabulce jiz vyskytuje
	 */
	public static boolean removeWhenExists(TreeNode node){
		TableProceedModel model = (TableProceedModel)(editorProceed.getTable().getModel());
		for(int i = 0; i < model.getData().size(); i++){
			if(model.getData().get(i).getNode().compareNode(node)){
				model.getData().remove(i);
			}
		}
		return false;
	}


	/** Metoda, ktera provadi smazani uzlu z stromove struktury testu nebo scenaru. Muze se jednat bud o test nebo slozku s testy
	 * @param node Uzel urceny ke smazani
	 */
	public static void deleteNodeFromTree(TreeNode node)  {

		//checkCurrentNode(node);
				
		int retVal = JOptionPane.showConfirmDialog(mainWindow, getLabel("doYouReallyWantToDeleteThisData"), getLabel("delete"), JOptionPane.WARNING_MESSAGE);
		if(retVal == JOptionPane.OK_OPTION ){
			
			try {
				File file = new File(node.getFilePath());			
				if(file.isDirectory()){
					UtilityClass.deleteFilesFromDirectory(file);
					treePanelController.reloadTreeAfterModify(node);
					treePanelController.reloadTree(node);
				}
				else{
					if(node.getTypeOfNode().equals(TYPE_TEST)){
						
						TreeNode currTest = (TreeNode) MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getModel();
						if(currTest != null){
							if(currTest.compareNode(node)){
								MainWindow.getController(Controllers.EDITOR_TEST_CTRL).setModel(new TreeNode());
							}
						}
					}
					else if(node.getTypeOfNode().equals(TYPE_SCENARIO)){
						
						TreeNode currScenario = (TreeNode) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getModel();
						if(currScenario != null){
							if(currScenario.compareNode(node)){
								MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).setModel(new TreeNode());
							}
						}
					}
					else{
						try {
							throw new UniverzalException(getLabel("unknownTypeOfNode"));
						} catch (UniverzalException e) {
							e.printStackTrace();
						}
					}
					
					if(file.delete()){
						
						System.out.println(node.getData().getName() + " " + getLabel("deteted"));
						node.removeFromParent();
						treePanelController.reloadTreeAfterModify(node);
						treePanelController.reloadTree(node);
					}
					else{
						showInfoNotification(getLabel("fileNotDeletedBecauseIsUsed" + "\n" + node.getData().getName()));
					}
				}
			} catch (NullPointerException e) {
				showInfoNotification(getLabel("selectNodeToDelete"));
			}
		}
	}

	

	/** Metoda, ktera slouzi pro prejmenovani zvoleneho uzlu
	 * @param node Uzel pro prejmenovani
	 */
	public static void renameNode(TreeNode node) {
		if(node != null){
			String retVal = JOptionPane.showInputDialog(mainWindow, getLabel("enterNewNodeTitle"), node.getData().getName());
			
			//nastavit file name a reloadcurrentTest/scenario
			if(!UtilityClass.isNullOrEmpty(retVal)){
				
				File oldFile = new File(node.getFilePath());
				if(oldFile != null){
					
					//Prevedeni nazvu do validni formy
					retVal = getValidName(retVal);
					
					//Prejmenovani v XML strukture
					node.getRootElement().setAttribute("name", retVal);
					
					//Prejmenoavni v data nodu
					node.getData().setName(retVal);
					//updateButton(node);
					
					
					//Prejmenovani souboru
					String newPath = oldFile.getPath();
					newPath = newPath.substring(0, newPath.lastIndexOf("\\")) + "\\" + retVal;
					node.setFilePath(newPath);
					oldFile.renameTo(new File(newPath));
					
					XmlOperation.writeToXml(node);
					
					showInfoNotification(getLabel("nodeRenamed"));
				}
			}
			else{
				showInfoNotification(getLabel("nodeNotRenamed"), getLabel("message"));
			}
		}
	}
	
	/** Metoda, ktera kontroluje novy nazev pro uzel, zda obsahuje koncovku xml. Pokud ne, tak ji nastavi.
	 * @param name Nove jmeno uzlu
	 * @return Validni nazev uzlu
	 */
	public static String getValidName(String name){
		if(name.contains(".xml")){
			return name;
		}
		else{
			String newName;
			int index = name.lastIndexOf('.');
			if(index != -1){
				newName = name.substring(0, index);
				newName += ".xml";
			}
			else{
				newName = name + ".xml";
			}
			return newName; 
		}
	}
}
