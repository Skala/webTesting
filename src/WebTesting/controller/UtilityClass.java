package WebTesting.controller;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showInfoNotification;
import static WebTesting.controller.XmlOperation.updateNodeToXml;
import static WebTesting.controller.XmlOperation.writeToXml;
import static WebTesting.core.Config.ACTION;
import static WebTesting.core.Config.ATTRIBUTE;
import static WebTesting.core.Config.COLOR_RED;
import static WebTesting.core.Config.COLOR_WHITE;
import static WebTesting.core.Config.LABEL;
import static WebTesting.core.Config.LOCATION;
import static WebTesting.core.Config.PARAMETER;
import static WebTesting.core.Config.TAG;
import static WebTesting.core.Config.VALUE;
import static WebTesting.core.Config.WAIT;

import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Stack;

import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;

import org.jdom2.Element;

import WebTesting.Interfaces.IListItem;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.EnumTypes;
import WebTesting.enums.scenarios.Actions;
import WebTesting.enums.scenarios.Parameters;
import WebTesting.enums.tests.Attributes;
import WebTesting.enums.tests.Locators;
import WebTesting.enums.tests.Tags;
import WebTesting.enums.tests.Values;
import WebTesting.enums.tests.Waits;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.SemanticException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownElementTypeException;
import WebTesting.exceptions.UnknownTypeOfLocator;
import WebTesting.exceptions.UnknownTypeOfNodeException;
import WebTesting.exceptions.UnknownTypeOfParameter;
import WebTesting.exceptions.WarningException;
import WebTesting.model.TreeNode;
import WebTesting.model.test.source.ControlTestListPanel;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioListPanel;
import net.sourceforge.htmlunit.corejs.javascript.tools.debugger.Main;

/**	Trida reprezentuje knihovni tridu, ve ktere jsou nedefinovany pomocne metody a konstanty. 
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 2. 4. 2016
 * File: UtilityClass.java
 */
public final class UtilityClass {

	
	public static final String TEXT_AREA = "textarea";
		
	
	/**
	 * Privatni explicitni konstruktor pro znemozneni vytvoreni instance z teto tridy
	 */
	private UtilityClass(){}
	
	
	/*------------------------
	 	Pristupove metody
	  ----------------------*/
	
	/** Method which return a label in current language
	 * @param name keyword of required word
	 * @return label in required language
	 */
	public static String getLabel(String name){
		
		try {
			return ResourceBundle.getBundle("WebTesting.locale." + Config.locale).getString(name);
		} catch (MissingResourceException e) {
			try {
				return ResourceBundle.getBundle("WebTesting.locale.lang_en_GB").getString(name);
			} catch (MissingResourceException e1) {
				e1.printStackTrace();
				showAlertNotification(name + ": nebyl nalezen preklad pro tento prvek");
				return "xxx " + name;
			}
		}
	}
	
	/** Methods for show notification in dialog windows and on CLI
	 * @param text text to show
	 * @param title title of dialog
	 */
	public static void showInfoNotification(String text, String title){
		showNotification(text, title, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showAlertNotification(String text, String title){
		showNotification(text, title, JOptionPane.WARNING_MESSAGE);
	}
	
	public static void showInfoNotification(String text){
		showInfoNotification(text, "");
	}
	
	public static void showAlertNotification(String text){
		showAlertNotification(text, "");
	}
	
	private static void showNotification(String text, String title, int type){
		if(type == JOptionPane.INFORMATION_MESSAGE)
			System.out.println(text);
		else
			System.err.println(text);
		
		JOptionPane.showMessageDialog(null, text, title, type);
	}
	
	/**
	 * @return the pathtoscenarios
	 */
	public static String getPathtoscenarios() {
		return Config.PATH_TO_SCENARIOS;
	}

	/**
	 * @return the pathtotests
	 */
	public static String getPathtotests() {
		return Config.PATH_TO_TESTS;
	}
		
	/**
	 * 	Metoda, ktera zjistuje, zda je textovy retezec prazdny, nebo nullovy
	 * @param value Vstupni textovy retezec
	 * @return Rozhodnuti zda je retezec null nebo prazdny
	 */
	public static boolean isNullOrEmpty(String value){
		if(value == null || value.trim().compareTo("") == 0){
			return true;
		}
		else return false;
	}
	
	/**	Metoda, ktera porovna dva retezce a rozhodne, zda jsou stejne nebo ne. Velikost pismen neni dulezita
	 * @param ret1 Prvni vstupni retezec
	 * @param ret2 Druhy vstupni retezec
	 * @return Pravda, pokud jsou retezce stejne, jinak nepravda
	 */
	public static boolean compareString(String ret1, String ret2){
		if(ret1.toLowerCase().compareTo(ret2.toLowerCase()) == 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	/** Metoda, ktera vytvori pole retezcu, ktere bude rozdeleno z radky v parametru. 
	 * Vytvoreno z duvodu potreby ignorace  rozdeleni podle specifickeho znaku.
	 * @param text Vstupni retezec pro rozdeleni
	 * @param c Znak podle ktere se ma vstup rozdelit
	 * @return Pole retezcu rozdelene podle specifickeho znaku a 
	 * @throws UniverzalException 
	 */
	public static String[] mySplit(String text, String c){
		
		boolean run = true;
		
		int start = 0;
		int mean = 0;
		int end = 0;
		
		List<String> list = new ArrayList<String>();
		
		while(start < text.length()&& mean < text.length()){
			String newString = "";
			//dokud bude rozdelovaci znak mezi nesparovanymi znaku oznacovaci vstup od uzivatele, pak budeme hledat dalsi vyskyt
			while(run){
				
				end = text.indexOf(c, mean);
				if(end == -1){
					end = text.length();
				}
				
				newString = text.substring(start, end);
				run = checkCondition(newString);
				mean = end+1;
			}
			run = true;
			start = end+1;
			mean = end+1;
			
			list.add(newString);
		}
		
		String [] pole = new String[list.size()];
		for(int i = 0 ; i < list.size(); i++){
			pole[i] = list.get(i);
		}
		return pole;
	}
	
	/** Metoda, ktera z hodnot v poli odstrani uvedeni retezec
	 * @param fields Pole, ze ktereho bude odebiran retezec z druheho parametru
	 * @param replace Odebirany retezec
	 */
	public static void removeString(String [] fields, String replace){
		for(int i = 0 ; i < fields.length ; i++){
			fields[i] = fields[i].replace(replace, "");
		}
	}
	
	/** Metoda, ktera kontroluje, zda je vysledny text bud obklopen znaky urcujici hranici uzivatelskeho vstupu, nebo nicim
	 * @param newString Text, ktery je kontrolovan
	 * @return Pravka/Nepravda v zavislosti na vstupnim textu
	 */
	private static boolean checkCondition(String newString) {
		//Hodnota, ktera urcuje, zda jsem uvnit nejakeho zadaneho textu od uzivatele. ## pozice , tady ## --> nesmim rozdelit na 2
		int i = 0;
		int index = 0;
		while(index < newString.length()){			
			index = newString.indexOf(Config.constantTag, index);
			if(index == -1){
				break;
			}
			else {
				index += Config.constantTag.length();
				i++;
			}
		}
		return (i%2 == 0) ? false : true;		
	}
	
	/**
	 * Metoda, ktera vrati nasobek puvodni velikosti okna
	 * @param dim Puvodni velikost okna
	 * @param rate Nasobek
	 * @return Pozadovany rozmer
	 */
	public static Dimension getDimension(Dimension dim, double rate){
		int width = (int) (dim.getWidth() * rate);
		int height = (int) (dim.getHeight() * rate);

		return new Dimension(width, height);
	}

	/** Metoda, ktera provede po odstranění uzlu ze stromu take odstraneni prislusnych souboru uvnitr souboroveho systemu
	 * @param file Cesta k souboru nebo k adresari, ktery ma byt smazan, respektive adresar, jehoz obsah ma byt smazan
	 */
	public static void deleteFilesFromDirectory(File file){
			
		int end = 50;		
		boolean success = true;
		
		Stack<File> stack = new Stack<File>();
		stack.push(file);

		while(!stack.isEmpty() && end>0){
			end--;
			File currentFile = stack.pop();
			if(currentFile.isDirectory()){
				File [] subDir = currentFile.listFiles();
				if(subDir.length != 0){
					stack.push(currentFile);
					for(int i = 0 ; i < subDir.length; i++){
						stack.push(subDir[i]);
					}
				}
				else{	
					if(currentFile.delete()){
						System.out.println("adresar smazan: " + file.getPath());
					}
					else{
						success = false;
						System.out.println("problem s mazanim adresare: " + file.getPath());
					}
				}
			}
			else{
				if(currentFile.delete()){
					System.out.println("soubor smazan: " + currentFile.getPath());
				}
				else{
					success = false;
					System.out.println("problem pri mazani souboru: " + currentFile.getPath());
				}
			}
		}
		
		if(!success){
			showInfoNotification(getLabel("problemDataDeleting" + "\n" + getLabel("someItemsAreUsed")));
		}
	}	
	
	/** Metoda, ktera otestuje, zda element uvedeny v parametru ma potomka, ktery nabyva hodnoty uvedene v nekterem ze seznamu. 
	 * Ve kterem typu seznamu se bude hledat se urci v prvnim parametru
	 * @param typeOfEnum Parametr urcujici typ seznamu 
	 * @param element Element, jehoz potomky testujeme
	 * @return Potomka, ktery odpovida hodnote ze seznamu
	 * @throws Exception 
	 */
	public static Element findElementInEnumFromParent(EnumTypes enumType, Element element) throws Exception{
		
		//Seznam prvku z enumu
		List<String> enumVals;
		
		switch(enumType){
		case ACTION:
			enumVals = Actions.getValues();
			break;
		case PARAMETER:
			enumVals = Parameters.getValues();
			break;
		case ATTRIBUTE:
			enumVals = Attributes.getValues();
			break;
		case LOCATOR:
			enumVals = Locators.getValues();
			break;
		case TAG:
			enumVals = Tags.getValues();
			break;
		case VALUE:
			enumVals = Values.getValues();
			break;
		case WAIT:
			enumVals = Waits.getValues();
			break;
		default:
			throw new Exception(getLabel("unknownTypeOfList") + ": " + enumType);
		}
		
		List<Element> subEl = element.getChildren();
		
		for (Element el : subEl) {
			if(isItemInList(enumVals, el.getName())){
				if(!el.getName().equals(LABEL)){
					return el;
				}
			}
		}
		throw new UnknownTypeOfLocator(getLabel("unknownTypeOfList") + ": " + enumType);
		
//		JList<IListItem> list;
//		switch(typeOfList){
//		case LOCATION:
//			list = ((ControlTestListPanel) MainWindow.getController(Controllers.TEST_LIST_PANEL_CTRL).getView()).getListLocators();
//			break;
//		case ATTRIBUTE:
//			list = ((ControlTestListPanel) MainWindow.getController(Controllers.TEST_LIST_PANEL_CTRL).getView()).getListAttributes();
//			break;
//		case TAG:
//			list = ((ControlTestListPanel) MainWindow.getController(Controllers.TEST_LIST_PANEL_CTRL).getView()).getListTags();
//			break;
//		case VALUE:
//			list = ((ControlTestListPanel) MainWindow.getController(Controllers.TEST_LIST_PANEL_CTRL).getView()).getListValues();
//			break;
//		case WAIT:
//			list = ((ControlTestListPanel) MainWindow.getController(Controllers.TEST_LIST_PANEL_CTRL).getView()).getListWaits();
//			break;
//		case ACTION:
//			list = ((ControlScenarioListPanel) MainWindow.getController(Controllers.SCENARIO_LIST_PANEL_CTRL).getView()).getListAction();
//			break;
//		case PARAMETER:
//			list = ((ControlScenarioListPanel) MainWindow.getController(Controllers.SCENARIO_LIST_PANEL_CTRL).getView()).getListParams();
//			break;
//		default:
//			throw new Exception(getLabel("unknownTypeOfList") + ": " + typeOfList);
//		}
//		
//		List<Element> subEl = element.getChildren();
//		for (Element el : subEl) {
//			if(isItemInList(list, el.getName())){
//				if(!el.getName().equals(LABEL)){
//					return el;
//				}
//			}
//		}
//		
	}
	
	
	/** Metoda, ktera vraci pravdivostni hodnotu v zavislosti na tom, zda se prvek ze druheho parametru naleza v JListu z prvniho parametru
	 * @param list JList ve kterem hledame pozadovanou hodnotu
	 * @param item Hodnota, kterou hledame v JListu
	 * @return Pravda, pokud se hodnota v JListu naleza, jinak nepravda
	 */
	public static boolean isItemInList(List<String> enumVals, String item){
		
		for (String enumVal : enumVals) {
			if(compareString(enumVal, item)){
				return true;
			}
		}
		return false;
	}
	
	
	/** Metoda, ktera vraci pravdivostni hodnotu, zda se polozka uvedena v paramtru naleze v nekterem ze seznamu.
	 * @param item Polozka, kterou hledame
	 * @return Rozhodnuti o existenci
	 */
	public static boolean isItemInLists(String item){
		
//		List<String> allValues = new ArrayList<String>();
//		
//		allValues.addAll(Actions.getValues());
//		allValues.addAll(Parameters.getValues());
//		allValues.addAll(Attributes.getValues());
//		allValues.addAll(Locators.getValues());
//		allValues.addAll(Tags.getValues());
//		allValues.addAll(Values.getValues());
//		allValues.addAll(Waits.getValues());
		
		List<List<String>> allEnums = new ArrayList<List<String>>();
		
		allEnums.add(Actions.getValues());
		allEnums.add(Parameters.getValues());
		allEnums.add(Attributes.getValues());
		allEnums.add(Locators.getValues());
		allEnums.add(Tags.getValues());
		allEnums.add(Values.getValues());
		allEnums.add(Waits.getValues());
		
		for (List<String> list : allEnums) {
			if(isItemInList(list, item)){
				return true;
			}
		}
		return false;
	}
	
	/** Metoda, ktera vraci pravdivostni hodnotu, zda se polozka uvedena v paramtru naleze v nekterem ze seznamu uvedenem v editaci testu.
	 * @param item Polozka, kterou hledame
	 * @return Rozhodnuti o existenci
	 */
	public static boolean isItemInListsEditTest(String item){
		
		List<List<String>> lists = new ArrayList<List<String>>();
		lists.add(Actions.getValues());
		lists.add(Parameters.getValues());
		
		for(int i = 0; i < lists.size(); i++){
			if(isItemInList(lists.get(i), item)){
				return true;
			}
		}
		return false;
	}
	
	/** Metoda, ktera vrati pocet znaku ze druheho parametru v textu uvedenem v prvnim parametru
	 * @param text Text, ve kterem hledame znak ze druheho paramteru
	 * @param c Hledany znak
	 * @return Pocet nalezenych znaku
	 */
	public static int getNumberOfContents(String text, char c){
		int count = 0;
		for(int i = 0; i < text.length(); i++){
			if(text.charAt(i) == c){
				count++;
			}
		}
		return count;
	}
	
	/** Metoda, ktera kontroluje, zda je uvedeny retezec v textu vzdy parovy. Pouziva se vyhradne pro kontrolu uzivatelskeho vstupu uvozeneho sekvenci ##
	 * @param text Text, ve kterem hledame podretezec ze druhe parametru
	 * @param occur hledany podretezec
	 * @return Pocet vyskytu podretezce.
	 */
	public static int getNumberOfSubSequences(String text, String occur){
		
		
		int count = 0;
		int index = 0;
		
		while(index < text.length()){
			index = text.indexOf(occur, index);
			if(index == -1){
				break;
			}
			count++;
			index +=occur.length();
		}
		return count;
	}
	
	/** Metoda urcena pro validaci prikazu. Zajisti, ze se v prikazu bude vyskytovat pouze jeden typ akce. Slouzi predevsim pro odhaleni nezadaneho ukoncovaciho znaku pro prikaz
	 * @param line Radka, ve ktere hledame retezce urcujici typ akce
	 * @return
	 */
	public static int getNumberOfActionsInLine(String line){
		line = line.replace(Config.subCommandSeparator, " ").replace(Config.endOfCommand, " ").trim();
		String [] tokens = line.split(" ");
		
		int count = 0;
		JList<IListItem> list = ((ControlScenarioListPanel) MainWindow.getController(Controllers.SCENARIO_LIST_PANEL_CTRL).getView()).getListAction();
		
		for(int i = 0 ; i < list.getModel().getSize() ; i++){
			String action = list.getModel().getElementAt(i).getName().toLowerCase();
			
			for(int j = 0; j < tokens.length ; j++){
				if(compareString(tokens[j], action)){ //line.toLowerCase().contains(action)
					System.out.println(action);
					count++;
				}
			}
		}
		return count;
	}
	
	public static String newNameDuplicate(String oldName){
		String newName = oldName;
		int index = newName.lastIndexOf(".");
		if(index == -1){
			index = newName.length();
		}
		newName = newName.substring(0, index) + "_copy.xml";
		return newName;
	}
}
