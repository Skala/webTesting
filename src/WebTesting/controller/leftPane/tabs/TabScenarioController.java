/**
 * 
 */
package WebTesting.controller.leftPane.tabs;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.tree.TreePath;

import WebTesting.Interfaces.IController;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabScenario;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: TabScenarioController.java
 */
public class TabScenarioController implements IController, KeyListener, MouseListener{

	private TabScenario view;
	
	public TabScenarioController(TabScenario view){
		this.view = view;
	}
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Controllers getType() {
		return Controllers.TAB_SCENARIO_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}
	
	/**
	 * Posluchac, ktery reaguje na klavesove prikazy nad stromem se scenari
	 */
	@Override 
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_DELETE){
			TreeNode node = (TreeNode) this.view.getTreeScenario().getLastSelectedPathComponent();
			if(node != null){
				ButtonActions.deleteNodeFromTree(node);
			}
		}
	}
	
	/**
	 * Posluchac pro manipulaci se stromem obsahujici scenare
	 */
	@Override 
	public void mouseReleased(MouseEvent me) {
		TreePath tp = this.view.getTreeScenario().getPathForLocation(me.getX(), me.getY());
		this.view.getTreeScenario().setSelectionPath(tp);
		TreeNode node = (TreeNode) this.view.getTreeScenario().getLastSelectedPathComponent();
		((TreePanelController) MainWindow.getController(Controllers.TREE_PANEL_CTRL)).clickAtTree(node, me);
	}

	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}
	
	@Override public void keyReleased(KeyEvent e) {}
	@Override public void keyTyped(KeyEvent e) {}

	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
	}

}
