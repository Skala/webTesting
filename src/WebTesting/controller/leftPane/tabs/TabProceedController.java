/**
 * 
 */
package WebTesting.controller.leftPane.tabs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import WebTesting.Interfaces.IController;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.TestData;
import WebTesting.model.rightPane.proceed.TableProceedModel;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.leftPane.tabs.TabProceed;
import WebTesting.view.rightPane.RightPane;
import WebTesting.view.rightPane.proceed.EditorProceed;
import WebTesting.view.rightPane.proceed.detail.TestDetail;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: tabProceedController.java
 */
public class TabProceedController implements IController, MouseListener {

	private TabProceed view;
	
	
	public TabProceedController(TabProceed view) {
		this.view = view;
	}
	
	
	/**
	 * Metoda, ktera ma za ukol vytvorit seznam testu, ktere jsou pridany do editoru pro spusteni
	 */
	public void populateListProceed(JList<TestData> listProceed) {
		
		if(listProceed != null){
			try {
				DefaultListModel<TestData> model = new DefaultListModel<TestData>();
				
				ArrayList<TestData> data = ((TableProceedModel) ((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).
						getTable().getModel()).getData();
				
				for(int i = 0 ; i < data.size(); i++){
					model.addElement(data.get(i));
				}
				listProceed.setModel(model);
			} catch (NullPointerException e) {
				System.err.println("zadne testy ke spusteni");
			}
		}
	}
	
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public Controllers getType() {
		return Controllers.TAB_PROCEED_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}


	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getClickCount() == 2){
			MainWindow.getController(Controllers.TEST_DETAIL_CTRL).setModel(this.view.getListProceed().getSelectedValue());
			((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.TEST_DETAIL);
		}
	}
	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}


	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}


	
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}

}
