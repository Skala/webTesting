/**
 * 
 */
package WebTesting.controller.leftPane.tabs;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.nio.channels.NotYetBoundException;

import javax.swing.tree.TreePath;

import WebTesting.Interfaces.IController;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabTest;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: TabTestController.java
 */
public class TabTestController implements IController, KeyListener, MouseListener {

	private TabTest view;
	private TreeNode model;
	
	
	public TabTestController(TabTest view) {
		this.view = view;
	}
		
	
	@Override
	public Object getModel() {
		return null;
	}

	
	@Override
	public void setModel(Object model) {
		this.model = (TreeNode) model;
		notifyController();
	}
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Controllers getType() {
		return Controllers.TAB_TEST_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}
	
	
	/*------------*
	 * Posluchace *
	 *------------*/
	
	/**
	 * Posluchac, ktery reaguje na klavesove prikazy nad stromem s testy
	 */
	@Override 
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_DELETE){
			TreeNode node = (TreeNode) this.view.getTreeTest().getLastSelectedPathComponent();
			if(node != null){
				ButtonActions.deleteNodeFromTree(node);
			}
		}
	}
	
	/**
	 * Posluchac urceny pro manipulaci se strom s testy
	 */
	@Override 
	public void mouseReleased(MouseEvent e) {
		TreePath tp = this.view.getTreeTest().getPathForLocation(e.getX(), e.getY());
		this.view.getTreeTest().setSelectionPath(tp);
		TreeNode node = (TreeNode) this.view.getTreeTest().getLastSelectedPathComponent();
		((TreePanelController) MainWindow.getController(Controllers.TREE_PANEL_CTRL)).clickAtTree(node, e);
	}

	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}
	
	@Override public void keyReleased(KeyEvent e) {}
	@Override public void keyTyped(KeyEvent e) {}
}
