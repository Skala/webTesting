/**
 * 
 */
package WebTesting.controller.leftPane;

import static WebTesting.controller.UtilityClass.compareString;
import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.core.Config.TYPE_SCENARIO;
import static WebTesting.core.Config.TYPE_TEST;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.tree.DefaultTreeModel;

import org.jdom2.JDOMException;

import WebTesting.Interfaces.IController;
import WebTesting.controller.MainWindowController;
import WebTesting.controller.XmlOperation;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.controller.rightPane.scenario.EditorScenarioController;
import WebTesting.controller.rightPane.test.EditorTestController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownTypeOfNodeException;
import WebTesting.exceptions.WarningException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.leftPane.tabs.TabScenario;
import WebTesting.view.leftPane.tabs.TabTest;
import WebTesting.view.popup.TabScenarioPopupMenu;
import WebTesting.view.popup.TabTestPopupMenu;
import WebTesting.view.rightPane.RightPane;
import WebTesting.view.rightPane.scenario.EditorScenario;
import WebTesting.view.rightPane.test.EditorTest;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 3. 2017
 * File: TreeController.java
 */
public class TreePanelController implements IController, ChangeListener{

	
	private TreePanel view;
	
	
	public TreePanelController(TreePanel view) {
		this.view = view;
	}
	
	
	/** Metoda, ktera vlozi do stromove struktury data
	 * @param tree Strom, do ktereho budou vkladana data
	 */
	public void populateTree(JTree tree)  {
		Stack<TreeNode> stack = new Stack<TreeNode>();

		TreeNode root = (TreeNode) tree.getModel().getRoot();
		root.removeAllChildren();

		stack.push(root);

		TreeNode currentNode;

		while(!stack.isEmpty()){
			currentNode = stack.pop();

			File input = new File(currentNode.getFilePath());
			if(input != null){

				if(input.isDirectory()){

					File [] subItem = input.listFiles();
					for (File file : subItem) {
						try {
							TreeNode newNode;

							if(file.isFile()){

								newNode = new TreeNode(XmlOperation.loadFromXml(file), file.getPath());

							}
							else{
								newNode = new TreeNode(file);
								
								stack.push(newNode);
							}
							newNode.setFilePath(file.getPath());
							currentNode.add(newNode);

						} 
						catch (JDOMException e) {
							String errMsg = ((MainWindowController) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL)).getErrorMessage();
							if(errMsg == null){
								errMsg = file.getPath();
							}
							else{
								errMsg = errMsg + "\n" + file.getPath();
							}
							((MainWindowController) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL)).setErrorMessage(errMsg);
							
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		reloadTree(tree);
	}

	
	/** Metoda, ktera zobrazi aktualizovana data v modelu stromu
	 * @param tree Strom, ktery chceme obnovit
	 */
	public void reloadTree(JTree tree){
		((DefaultTreeModel) tree.getModel()).reload();
	}
	
	
	/** Metoda, ktera po aktualizaci dat v uzlu provede aktualizaci celeho stromu, aby v nem byly provedeny zmeny
	 * @param treeNode Aktualizovany uzel, pomoci nehoz identifikujeme strom pro aktualizaci
	 */
	public void reloadTree(TreeNode treeNode){
		if(treeNode.getTypeOfNode().equals(TYPE_SCENARIO)){
			reloadTree(((TabScenario) MainWindow.getController(Controllers.TAB_SCENARIO_CTRL).getView()).getTreeScenario());
		}
		else if(treeNode.getTypeOfNode().equals(TYPE_TEST)){
			reloadTree(((TabTest) MainWindow.getController(Controllers.TAB_TEST_CTRL).getView()).getTreeTest());
		}
		else{
			try {
				throw new UniverzalException(getLabel("unknownTypeOfNode"));
			} catch (UniverzalException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/** Obnoveni stromu, ktere je vyvolano po modifikaci nejakeho uzlu
	 * @param node Uzel, ktery akci vyvolal
	 */
	public void reloadTreeAfterModify(TreeNode node){
		if(node.getTypeOfNode().equals(TYPE_TEST)){
			reloadTreeAfterModify(((TabTest) MainWindow.getController(Controllers.TAB_TEST_CTRL).getView()).getTreeTest());
		}
		else if (node.getTypeOfNode().equals(TYPE_SCENARIO)){
			reloadTreeAfterModify(((TabScenario) MainWindow.getController(Controllers.TAB_SCENARIO_CTRL).getView()).getTreeScenario());
		}
		else{
			try {
				throw new WarningException(getLabel("unknownTypeOfNode") + "\n" + getLabel("treeCanNotBeReloaded"));
			} catch (WarningException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	/** Metoda, ktera vyvola obnoveni stromu po modifikaci nektereho z listu
	 * @param tree Strom, ktery ma byt obnoven
	 */
	public void reloadTreeAfterModify(JTree tree){
		String typeOfNode = ((TreeNode) tree.getModel().getRoot()).getTypeOfNode();
		
		if(typeOfNode == null){
			typeOfNode = ((TreeNode) tree.getModel().getRoot()).getRootElement().getName();
		}
		
		if(compareString(typeOfNode, TYPE_TEST)){
			System.out.println("after modify test");
			populateTree(tree);
		
		}
		else if(compareString(typeOfNode, TYPE_SCENARIO)){
			System.out.println("after modify scenario");
			populateTree(tree);
			
		}
		else{
			try {
				throw new UnknownTypeOfNodeException();
			} catch (UnknownTypeOfNodeException e) {
				e.printStackTrace();
			}
		}
	}

	
	/** Metoda, ktera vraci vsechny listy, ktere dany uzel ma.
	 * @param node Uzel, jehoz listy chceme ziskat
	 * @return Seznam listu daneho uzlu
	 */
	public static ArrayList<TreeNode> getAllChildren(TreeNode node){
		
		ArrayList<TreeNode> list = new ArrayList<TreeNode>();
		Stack<TreeNode> stack = new Stack<TreeNode>();

		stack.push(node);

		while(!stack.isEmpty()){
			TreeNode currentNode = stack.pop();

			if(currentNode.isLeaf()){
				list.add(currentNode);
			}
			else{
				for(int i = 0; i < currentNode.getChildCount(); i++){
					stack.add((TreeNode)currentNode.getChildAt(i));
				}
			}
		}
		return list;
	}
	
	/**
	 * Metoda, ktera zobrazi spravny obsah v prave casti na zaklade aktivni karty v leve casti.
	 */
	public void showCurrentTabInLeftPane(){
		MainWindow mainEditor = (MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView();
		if(mainEditor != null){
			
			RightPaneController rpc = (RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL);
			
			if(rpc != null){

				int index = ((TreePanel) MainWindow.getController(Controllers.TREE_PANEL_CTRL).getView()).getTabbedPane().getSelectedIndex();
//				System.out.println("index tab: " + index);
				
				switch(index){
				case 0:
					rpc.showPanel(RightPaneEditor.EDITOR_TEST);
					break;
				case 1:
					rpc.showPanel(RightPaneEditor.EDITOR_SCENARIO);
					break;
				case 2:
					rpc.showPanel(RightPaneEditor.EDITOR_PROCEED);
					break;
				}
			
			}
			
		}
	}
	
	
	/**
	 * Metoda, ktera provede operaci nad danym uzlem
	 * @param node Uzel na ktery uzivatel klikl
	 * @param me Instance mouse eventu
	 */
	public void clickAtTree(TreeNode node, MouseEvent me)  {

		if(node != null){
			int clickCount = me.getClickCount();
			boolean leftClick = SwingUtilities.isLeftMouseButton(me);
			
			if(clickCount == 1 && !leftClick){	//jedno kliknuti a navic pravym tlacitkem -> popup menu
				
				if(node.getTypeOfNode().equals(TYPE_SCENARIO)){//UtilityClass.compareString(node.getTypeOfNode(), UtilityClass.TYPE_SCENARIO)
					TabScenarioPopupMenu popMenu = new TabScenarioPopupMenu();
					popMenu.show(me.getComponent(),me.getX(), me.getY());
				}
				else if(node.getTypeOfNode().equals(TYPE_TEST)){
					TabTestPopupMenu popMenu = new TabTestPopupMenu();
					popMenu.show(me.getComponent(),me.getX(), me.getY());
				}
				else{
					try {
						throw new UnknownTypeOfNodeException();
					} catch (UnknownTypeOfNodeException e) {
						e.printStackTrace();
					}
				}
			}
			else{
				if(clickCount == 2 && leftClick){
					if(node.isLeaf()){
						
						if(node.getTypeOfNode() == TYPE_TEST)
						{
							System.out.println("otevri test");
							((EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL)).setModel(node);
//							ContentPanel.showPanel(ContentPanel.getEditorTests());
						}
						else if(node.getTypeOfNode() == TYPE_SCENARIO){
							System.out.println("otevri scenar");
							((EditorScenarioController) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL)).setModel(node);
//							ContentPanel.showPanel(ContentPanel.getEditorScenario());
						}
						else{
							try {
								throw new UnknownTypeOfNodeException();
							} catch (UnknownTypeOfNodeException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		else{
			try {
				throw new UniverzalException("node has not been selected");
			} catch (UniverzalException e) {
				System.err.println(e.getMessage());
			}
		}	
	}


	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public Controllers getType() {
		return Controllers.TREE_PANEL_CTRL;
	}


	@Override
	public Object getView() {
		return this.view;
	}


	/* (non-Javadoc)
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		showCurrentTabInLeftPane();
	}


	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IController#setModel(java.lang.Object)
	 */
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}
	
	
}
