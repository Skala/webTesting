/**
 * 
 */
package WebTesting.controller;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showInfoNotification;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import WebTesting.Interfaces.IController;
import WebTesting.controller.rightPane.proceed.EditorProceedController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.view.MainMenu;
import WebTesting.view.MainWindow;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: MainMenuController.java
 */
public class MainMenuController implements ActionListener, IController {

	
	private MainMenu view;
	
	
	public MainMenuController(MainMenu view) {
		this.view = view;
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	
	
	/*----------------*
	 * Ostatni metody *
	 *----------------*/
	private void setCountOfTests(){
		JTextField firefox = new JTextField(EditorProceedController.firefoxMax+ "");
		JTextField guiless = new JTextField(EditorProceedController.guilessMax+ "");
		JComponent [] inputs = {new JLabel("Firefox"), firefox, new JLabel(getLabel("browserWithoutGUI")), guiless};
		
		/* vypis prihlasovaciho okna */
		if (JOptionPane.showConfirmDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(),
				inputs, getLabel("settings"), JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
		{
			int ff = 1;
			int gl = 1;
			try {
				ff = Integer.parseInt(firefox.getText());
				gl = Integer.parseInt(guiless.getText());
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), 
						getLabel("requiredIntOnInput") + "\n" + getLabel("setupToDefaultValue") + ": 1");
				EditorProceedController.firefoxMax = ff;
				EditorProceedController.guilessMax = gl;
				return;
			}
			
			EditorProceedController.firefoxMax = ff;
			EditorProceedController.guilessMax = gl;
			String msg = getLabel("followingValuesWereSet") + "\nFirefox: " + ff + "\n" + getLabel("browserWithoutGUI") + ": " + gl;
			JOptionPane.showMessageDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), msg);
		}
	}
	
	/**
	 * Metoda, ktera nastavi pozadovany jazyk
	 */
	private void setLanguage() {
		JRadioButton czech = new JRadioButton("Czech");
		czech.setActionCommand("cz");
		
		JRadioButton english = new JRadioButton("English");
		english.setActionCommand("en");
		
		
		JRadioButton deutsch = new JRadioButton("Deutsch");
		deutsch.setActionCommand("de");
		
		ButtonGroup group = new ButtonGroup();
		group.add(czech);
		group.add(deutsch);
		group.add(english);
		
		JComponent [] langs ={czech, english, deutsch};
		
		if (JOptionPane.showConfirmDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), 
				langs, getLabel("choiceOfLanguage"), JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
		{
			String ret = null;
			switch(group.getSelection().getActionCommand()){
			case "cz":
				ret = "lang_cs_CZ";
				break;
			case "en":
				ret = "lang_en_GB";
				break;
			case "de":
				ret = "lang_de_DE";
				break;
			}
			
			if(ret != null){
				File f = new File("files\\locale.txt");
				if(f.exists()){
					f.delete();
				}
				try {
					PrintWriter pw = new PrintWriter("files\\locale.txt");
					pw.write(ret);
					pw.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				showInfoNotification(getLabel("toDisplayChangesRestartProgram"));
			}
			else{
				UtilityClass.showAlertNotification("errorWhenChangingLanguage");
			}
		}
	}

//	private MainWindow getMainWindowView() {
//		return ((MainWindowController) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL)).getView();
//	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "countOfTests":
			setCountOfTests();
			break;
		case "exit":
			((MainWindowController) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL)).exitProgram();
			break;
		case "lang":
			setLanguage();
			break;
		}
	}
	
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Controllers getType() {
		return Controllers.MAIN_MENU_CTRL;
	}
	
	@Override
	public Object getView() {
		return this.view;
	}

	@Deprecated
	@Override
	public Object getModel() {
		return null;
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		
	} 
}
