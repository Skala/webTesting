package WebTesting.controller;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.getNumberOfActionsInLine;
import static WebTesting.controller.UtilityClass.getNumberOfSubSequences;
import static WebTesting.controller.UtilityClass.isItemInList;
import static WebTesting.controller.UtilityClass.isItemInLists;
import static WebTesting.controller.UtilityClass.mySplit;
import static WebTesting.controller.UtilityClass.removeString;
import static WebTesting.core.Config.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Content.CType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.scenarios.Actions;
import WebTesting.enums.scenarios.Parameters;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.SemanticException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownElementTypeException;
import WebTesting.exceptions.UnknownTypeOfLocator;
import WebTesting.exceptions.UnknownTypeOfNodeException;
import WebTesting.exceptions.UnknownTypeOfParameter;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioListPanel;
import WebTesting.view.rightPane.scenario.EditorScenario;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/** Trida, ktera slouzi pro manipulaci s uzlu ve stromu testu a scenaru. Umoznuje jejich nacitani a ukladni z/do XML souboru
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 3. 2016
 * File: XmlOperation.java
 */
public class XmlOperation {

	/**	Bezparametricky explicitni kontruktor pro zamezeni vytvoreni instance
	 * 
	 */
	private XmlOperation(){}

	
	/*--------------------
	 	Vlastni metody
	 -------------------*/
	
	/**	Metoda, ktera zaktualizuje data z textArey do xml RootElementu. Metoda vyhazuje vyjimku v pripade, ze nastene nejaky problem pri aktualizaci uzlu.
	 * @param node uzel, do ktereho se budou data aktualizovat
	 */
	public static void updateNodeToXml(TreeNode node) throws IndexOutOfBoundsException, UnknownElementTypeException, UnknownTypeOfNodeException, UnknownTypeOfParameter, SemanticException, UnknownTypeOfLocator, NotYetImplementedException, UniverzalException    { 
		//data jiz existuji a tak se musi nahradit stavajici rootElement s xml a vytvorit novy
		if(node != null){
			if(node.getData() != null){
				createXmlTree(node);
			}
			else{
				node.setData(new DataNode());
				createXmlTree(node);
			}
		}
		else{
			node = new TreeNode(new DataNode());
			createXmlTree(node);
		}	
		node.getData().setNodeText(null);
	}
	
	
	/** Metoda, ktera zaktualizuje xml strukturu pro test, nebo scenar
	 * @param node Uzel, ktery ma byt aktualizovan.
	 */
	private static void createXmlTree(TreeNode node) throws UnknownElementTypeException, UnknownTypeOfNodeException, UnknownTypeOfParameter, SemanticException, UnknownTypeOfLocator, IndexOutOfBoundsException, NotYetImplementedException, UniverzalException {
				
		if(node.getTypeOfNode().equals(TYPE_TEST)){
			
			makeTestXmlTree(node);
		}
		else if(node.getTypeOfNode().equals(TYPE_SCENARIO)){
			
			makeScenarioXmlTree(node);
		}
		else{
			throw new UnknownTypeOfNodeException(getLabel("unknownTypeOfNode"));
		}
	}



	/** Pomocna metoda pro vytvoreni xml struktury pro scenar
	 * @param node uzel scenare
	 * @throws UnknownElementTypeException
	 * @throws UniverzalException
	 */
	private static void makeScenarioXmlTree(TreeNode node) throws UnknownElementTypeException, UniverzalException {
		
		Element root = new Element(TYPE_SCENARIO);
		root.setAttribute(new Attribute("name", node.getData().getName()));
		
		String text = ((EditorScenario) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getView()).
				getTextArea().getText();
		String [] line = text.split(Config.endOfCommand);
		
		//zde zacnu parsovat jednotlive radky
		for(int i = 0 ; i < line.length; i++){
			line[i] = line[i].trim();
			
			if(getNumberOfActionsInLine(line[i]) != 1){
				throw new UniverzalException(getLabel("errorOnLine") +" "+ (i+1) + ", " + getLabel("moreTypesOfEvents"));
			}
			
			if(line[i].isEmpty()){
				continue;
			}
			Element command = new Element("command");
			
			if((getNumberOfSubSequences(line[i], Config.constantTag) % 2) != 0){
				throw new UniverzalException(getLabel("controlCharactersNotPaired") + " - " + getLabel("line") + " " + (i+1));
			}
			String [] subLine = mySplit(line[i], Config.subCommandSeparator);
			
			//zde se parsuji elementy radky
			for(int j = 0 ; j < subLine.length; j++){
				subLine[j] = subLine[j].trim();
				makeScenarioCommand(command, subLine, i, j);
//				command = makeScenarioCommand(command, subLine, i, j);
			}
			root.addContent(command);
		}
		node.getData().setRootElement(root);
	}
	
	
	/** Metoda, ktera z radky textu vytvori prikaz, ktery se nasledne pripoji k root prikazu.
	 * @param command Prikaz, 
	 * @param subLine
	 * @param lineNumber
	 * @param subLineNumber
	 * @throws UnknownElementTypeException
	 * @throws UniverzalException
	 */
	private static void makeScenarioCommand(Element command, String[] subLine, int lineNumber,  int subLineNumber)
			throws UnknownElementTypeException, UniverzalException {
		if( subLineNumber == 0 ){
			if(UtilityClass.isItemInList(Actions.getValues(), subLine[subLineNumber])){
				command.setAttribute(new Attribute("action", subLine[subLineNumber].trim()));
				if(command.getChild(WAIT) != null){
					String value = command.getChildText(WAIT);
					try {
						Integer.parseInt(value);
						command.setAttribute(new Attribute("wait", value));
					} catch (NumberFormatException e) {
						throw new NumberFormatException(getLabel("requiredIntOnInput") + " (" + value + ") - " + getLabel("line") + " " + (lineNumber+1) + ", " + 
														getLabel("command") + " " + (subLineNumber+1));
					}
				}
			}
			else{
				throw new UnknownElementTypeException(getLabel("unknownTypeOfAction") + ": " + getLabel("line") + ": " + (lineNumber + 1) + " (" + subLine[subLineNumber] + ")");
			}
		}
		else{
					
			String [] token = mySplit(subLine[subLineNumber], "=");
			removeString(token, Config.constantTag);
			
			if(UtilityClass.isItemInList(Parameters.getValues(), token[0])){
				Element subCommand = new Element(token[0].toLowerCase());
				
				Element subSubCommand = new Element("label");
				if(token.length > 1){
					subSubCommand.setText(token[1]);
				}
				subCommand.addContent(subSubCommand);
				command.addContent(subCommand);
				
			}
			else{
				throw new UnknownElementTypeException(getLabel("badFormatOnLine") + ": " + (lineNumber+1) + " ("+ token[0] + ")");
			}
		}
	}
	
	/** Pomocna metoda pro vytvoreni xml struktury reprezentujici uzel testu
	 * @param node Uzel typu test
	 * @throws UnknownElementTypeException
	 * @throws UnknownTypeOfParameter
	 * @throws SemanticException
	 * @throws UnknownTypeOfLocator
	 * @throws IndexOutOfBoundsException
	 * @throws NotYetImplementedException
	 * @throws UniverzalException
	 */
	private static void makeTestXmlTree(TreeNode node) throws UnknownElementTypeException, UnknownTypeOfParameter, SemanticException, UnknownTypeOfLocator, IndexOutOfBoundsException, NotYetImplementedException, UniverzalException {
		
		Element root = node.getRootElement();
		List<Element> command = root.getChildren();
				
		String text = ((EditorTestSource) MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).getView()).
				getTextArea().getText();
		String [] line = text.split(Config.endOfCommand); 
		
				
		//zde zacnu parsovat jednotlive radky
		for(int i = 0 ; i < line.length; i++){
			
			line[i] = line[i].trim();
			if(line[i].isEmpty()){
				continue;
			}
			
			List<Element> subCommand = command.get(i).getChildren(); //location, url, value,...
			
			if((getNumberOfSubSequences(line[i], Config.constantTag) % 2) != 0){
				throw new UniverzalException(getLabel("controlCharactersNotPaired") + " - " + getLabel("line" )+ ": " + (i+1));
			}
			
			String [] subLine = mySplit(line[i], Config.subCommandSeparator);
			
			//zde se parsuji elementy z radkyradky
			try {
				for(int j = 0 ; j < subLine.length; j++){
					subLine[j] = subLine[j].trim();
					makeTestSubCommand(subCommand.get(j), subLine[j], i, j);
				}
			} catch (IndexOutOfBoundsException e) {
				throw new IndexOutOfBoundsException(getLabel("errorOnLineWithIndexation") + ": " + (i+1));
			}
		}
	}


	
	/** Pomocna metoda pro vytvoreni xml struktury testoveho uzlu.
	  * @param subCommand Element, kteremu odpovida cast radky
	  * @param subLine Podretezec dane radkz
	  * @param lineNumber Index radky
	  * @param subLineNumber Index casti textu uvnitr radky oznacenet indexem i
	  * @throws UnknownTypeOfParameter
	  * @throws SemanticException
	  * @throws UnknownTypeOfLocator
	  * @throws IndexOutOfBoundsException
	  * @throws NotYetImplementedException
	  * @throws UniverzalException
	  */
	 private static void makeTestSubCommand(Element subCommand, String subLine, int lineNumber, int subLineNumber) throws UnknownTypeOfParameter, SemanticException, UnknownTypeOfLocator, IndexOutOfBoundsException, NotYetImplementedException, UniverzalException {
			
		String [] token = mySplit(subLine, "=");
		removeString(token, Config.constantTag);
		
		String subComString = subCommand.getName();
		
		if(subComString != null){
			subComString = subComString.trim().toLowerCase();
			if(UtilityClass.isItemInList(Parameters.getValues(), subComString)){
				try{
					switch(subComString){
					case "url":
						writeOneToOneContent(subCommand, token);
						break;
					case "location":
						writeOneToTwoContent(subCommand, token);
						break;
					case "count":
						writeOneToOneContent(subCommand, token);
						break;
					case "content":
						writeContent(subCommand, token);
						break;	
					case "variable":
						writeOneToOneContent(subCommand, token);
						break;
					case "wait":
						writeOneToTwoContent(subCommand, token);
						break;
					case "direction":
						writeOneToOneContent(subCommand, token);
						break;
					default:
						throw new NotYetImplementedException(getLabel("convertingParameterNotImplemented") + ": " + subComString);
					}
				}catch(UniverzalException e){
					throw new UniverzalException(e.getMessage() + " " + getLabel("onLine") + " " + (lineNumber+1) + "(" + (subLineNumber+1) + " " + getLabel("command") + ")");
				}catch(UnknownTypeOfLocator e){
					throw new UnknownTypeOfLocator(e.getMessage() + " " + getLabel("onLine") + " " + (lineNumber+1) + "(" + (subLineNumber+1) + " " + getLabel("command") + ")");
				}
			}
			else{
				throw new UnknownTypeOfParameter(getLabel("unknownTypeOfParameter") + ": " + subComString + " " + getLabel("onLine") + " " + (lineNumber+1) + "(" + (subLineNumber+1) + " " + getLabel("command") + ")");
			}
		}
	}
	
	/** Metoda, ktera zapise data, ktera byla urcena parametrem content
	 * @param subCommand Element, ktery se aktualizuje
	 * @param token Data, ktere se zapisi do elementu z atributu
	 * @throws UniverzalException
	 */
	private static void writeContent(Element subCommand, String[] token) throws UniverzalException {
		
		subCommand.removeContent();
		
		if(token.length > 0){
			subCommand.addContent(new Element("label").setText(token[0]));
		}
		
		if(token.length == 2){
			subCommand.addContent(new Element("content").setText(token[1]));
			return;
		}
		else if(token.length == 3){
			if(isItemInLists(token[1])){
				subCommand.addContent(new Element(token[1]).setText(token[2]));
			}
			else{
				throw new UniverzalException(getLabel("elementNotBelongToAnyLists") + ": " + token[1]);
			}
		}
		else if(token.length > 3){
			throw new UniverzalException(getLabel("tooManyTypesOfParameter"));
		}
	}
	
	/** Metoda, ktera zapisuje elementy, ktere obsahuji pouze hodnotu, tj. 1 parametru
	 * @param subCommand Element, do ktereho se data zapisuji
	 * @param token Data, ktera se zapisi do elementu
	 * @throws UniverzalException
	 */
	private static void writeOneToOneContent(Element subCommand, String[] token) throws UniverzalException {
		subCommand.removeContent();
		if(token.length > 0){
			subCommand.addContent(new Element("label").setText(token[0]));
		}
		if(token.length>1){
			if(isItemInList(Parameters.getValues(), subCommand.getName())){
				if(subCommand.getName().equals(WAIT)){
					try {
						Integer.valueOf(token[1]);
						subCommand.addContent(new Element(CONTENT).setText(token[1]));
					} catch (NumberFormatException e) {
						throw new NumberFormatException(getLabel("requiredIntOnInput"));
					}
				}
				else{
					subCommand.addContent(new Element(CONTENT).setText(token[1]));
				}
			}
		}
		if(token.length > 2){
			throw new UniverzalException(getLabel("tooManyParameters"));
		}
	}

	
	/** Metoda, ktera ulozi do xml struktury dva elementy. Prvni je label s prislusnou strukturou a druhy je element z druheho tokenu s nastavenou hodnotu ze tretiho parametru
	 * @param subCommand Element, do ktereho se ukladaji data
	 * @param token Text, ktery se uklada do elementu
	 * @throws UnknownTypeOfLocator
	 * @throws UniverzalException
	 */
	private static void writeOneToTwoContent(Element subCommand, String[] token) throws UnknownTypeOfLocator, UniverzalException {
		
		subCommand.removeContent();
		//System.err.println("   lokace: " + token[2]);
		
		if(token.length > 0){
			subCommand.addContent(new Element("label").setText(token[0]));
		}
		
		if(token.length > 1){
			token[1] = token[1].trim();
			
			if(UtilityClass.isItemInLists(token[1])){
				subCommand.addContent(new Element(token[1]));
			}
			else{
				throw new UniverzalException(getLabel("elementNotBelongToAnyLists") + " (" + token[1] + ")");
			}
		}
		if(token.length > 2){
			subCommand.getChild(token[1]).setText(token[2].trim());
		}
		
		if(token.length > 3){
			throw new UniverzalException(getLabel("tooManyParameters"));
		}
	}
	

	/**Metoda, ktera zapise uzel z parametru do souboru xml. 
	 * @param node Zapisovany uzel
	 * @param file Soubor, do ktereho se zapisuje
	 */
	public static synchronized void writeToXml(TreeNode node, File file) {
		if(node != null){
					
			Document doc = new Document(node.getData().getRootElement().clone());
			if(file != null){
				node.setFilePath(file.getPath());
			}
			else{
				file = new File(node.getFilePath());
			}
			
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.getFormat().setEncoding("UTF-8");
			
			try {
				FileWriter fw = new FileWriter(file);
				xmlOutput.output(doc, fw);
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				
			}
			
		}
	}
	
	/**
	 * @param node
	 */
	public static void writeToXml(TreeNode node) {
		writeToXml(node, null);		
	}

	/** Metoda, ktera nacita xml strukturu xml souboru do instance mechanismu JDOM
	 * @param file Zdrojovy soubor s daty
	 * @return Data ze souboru.
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static synchronized DataNode loadFromXml(File file) throws JDOMException, IOException {
		
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(file);
	
		Element rootElement = doc.getRootElement();
		DataNode data = new DataNode(rootElement);
		return data;
	}
		
	/** Metoda, ktera pozici elementu v ramci celeho contentu u JDOM struktury. Content obsahuje data Element, Text.
	 * @param root
	 * @param position
	 * @return
	 */
	public static int getElementPositionFromContent(Element root, int position){
		List<Content> listContent = root.getContent();
		int index = 0;
		
		while(index < listContent.size()){
			
			if(listContent.get(index).getCType() == CType.Element){
				position--;
				if(position < 0){
					return index;
				}
			}
			index++;
		}
		return index;
	}


	
}
