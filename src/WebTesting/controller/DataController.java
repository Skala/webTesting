package WebTesting.controller;

import java.util.ArrayList;
import java.util.List;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.ControllerNotFoundException;



public class DataController {

	
	//Seznam vsech kontroleru
	private List<IController> controllerList = new ArrayList<IController>();
	
	public void addController(IController controller){
		this.controllerList.add(controller);
	}
	
	public void notifyController(Controllers ctrl){
		getController(ctrl).notifyController();
	}
	
	public IController getController(Controllers ctrl){
		IController retCtrl = null;
		for (IController controller : this.controllerList) {
			if(controller.getType() == ctrl){
				retCtrl = controller;
				break;
			}
		}
		
		if(retCtrl != null){
			return retCtrl;
		}
		else{
			try {
				throw new ControllerNotFoundException(ctrl.getTitle());
			} catch (ControllerNotFoundException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
