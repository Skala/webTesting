/**
 * 
 */
package WebTesting.controller;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.core.Config.locale;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.JOptionPane;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.view.MainWindow;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: MainWindowController.java
 */
public class MainWindowController implements IController, WindowListener, ActionListener{

	private MainWindow view;
	
	private String errorMessage = null;
	private PrintStream out;
	private boolean isLogged = false;
	
	
	public MainWindowController(MainWindow view){
		this.view = view;
		setLocale();
		if(isLogged){	
			setLoger();	
		}
	}
	
	/*-------------------*
	 * Pristupove metody *
	 *-------------------*/
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public PrintStream getOut() {
		return out;
	}

	/*----------------*
	 * Ostatni metody *
	 *----------------*/
	
	private void setLocale() {
		try {
			BufferedReader br = new BufferedReader(new java.io.FileReader("resources\\locale.txt"));
			locale = br.readLine();
			br.close();
		} catch (Exception e) {
			locale = "lang_en_GB";
			e.printStackTrace();
		}
	}
	
	/**
	 * Metoda, ktera cely program ukonci
	 */
	public void exitProgram(){
		int retVal = JOptionPane.showConfirmDialog(this.view, getLabel("doYouReallyWantToCloseTheApplicatio"), getLabel("closeApplication"), JOptionPane.OK_CANCEL_OPTION);
		if(retVal == JOptionPane.OK_OPTION){
			try {
				out.close();
			} catch (NullPointerException e) {
				System.err.println(getLabel("LoggingNotActive"));
			}
			System.exit(0);
		}
	}
	
	/**
	 * Metoda, ktera nastavi standardni a chybovy vystup do souboru 
	 */
	public void setLoger(){
		File f = new File("files\\log.txt");
		if(f.exists()){
			f.delete();
		}
		try {
			out = new PrintStream(new FileOutputStream("files\\log.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.setOut(out);
		System.setErr(out);
	}

	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Controllers getType() {
		return Controllers.MAIN_WINDOW_CTRL;
	}
	
	@Deprecated
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}


	@Deprecated
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
	}
	@Override
	public MainWindow getView() {
		return view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "exit":
			exitProgram();
			break;
		}
	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		exitProgram();
	}
	
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowOpened(WindowEvent e) {}

	
}
