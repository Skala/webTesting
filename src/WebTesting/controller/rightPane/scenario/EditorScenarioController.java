/**
 * 
 */
package WebTesting.controller.rightPane.scenario;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import WebTesting.Interfaces.IController;
import WebTesting.controller.ButtonActions;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.NullModelException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.EditorScenario;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: EditorScenarioControlller.java
 */
public class EditorScenarioController implements IController {

	private EditorScenario view;
	private TreeNode model;
	
	
	public EditorScenarioController(EditorScenario view) {
		this.view = view;
		setkeyShortcut();
	}
	
	@Override
	public Object getModel() {
		return this.model;
	}

	@Override
	public void setModel(Object model) {
		this.model = (TreeNode) model;
		notifyController();
	}
	
	private void setkeyShortcut() {
		UndoManager manager = new UndoManager();

		Document doc = this.view.getTextArea().getDocument();

		doc.addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				manager.addEdit(e.getEdit());
			}
		});

		this.view.getTextArea().getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
		this.view.getTextArea().getActionMap().put("Undo", new AbstractAction("Undo") {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(manager.canUndo()){
						manager.undo();
					}
				}catch (CannotUndoException e){
					e.printStackTrace();
				}
			}
		});
		
		
		this.view.getTextArea().getInputMap().put(KeyStroke.getKeyStroke("control Y"), "Redo");
		this.view.getTextArea().getActionMap().put("Redo", new AbstractAction("Redo") {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(manager.canRedo()){
						manager.redo();
					}
				}catch(CannotRedoException e){
					e.printStackTrace();
				}
				
			}
		});
		
		this.view.getTextArea().getInputMap().put(KeyStroke.getKeyStroke("control S"), "Update");
		this.view.getTextArea().getActionMap().put("Update", new AbstractAction("Update") {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ButtonActions.updateButton(model);
			}
		});
		
	}


	@Override
	public void notifyController() {
		if(this.model != null){
			if(this.model.getData() != null){
				//pokud je nodeText null, pak vygeneruju text z elementu
				if(this.model.getData().getNodeText() == null){
					this.view.getTextArea().setText(this.model.getData().elementsToTextScenario());
				}
				//jinak vypisu data z textNode
				else{
					this.view.getTextArea().setText(this.model.getData().getNodeText());
				}
				notifyAllControllers();
			}
			else{
				try {
					throw new Exception("Current node Data is null");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else{
			try {
				throw new NullModelException();
			} catch (NullModelException e) {
				e.printStackTrace();
			}
		}

	}


	@Override
	public void notifyAllControllers() {
		MainWindow.getController(Controllers.SCENARIO_UPPER_PANEL_CTRL).notifyController();
		MainWindow.getController(Controllers.SCENARIO_LOWER_PANEL_CTRL).notifyController();
		MainWindow.getController(Controllers.SCENARIO_LIST_PANEL_CTRL).notifyController();
	}

	
	@Override
	public Controllers getType() {
		return Controllers.EDITOR_SCENARIO_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}
}
