/**
 * 
 */
package WebTesting.controller.rightPane.scenario;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.sound.midi.SysexMessage;
import javax.swing.JList;
import javax.swing.SwingUtilities;

import WebTesting.Interfaces.IController;
import WebTesting.Interfaces.IListItem;
import WebTesting.enums.Controllers;
import WebTesting.enums.scenarios.Actions;
import WebTesting.enums.scenarios.Parameters;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioListPanel;
import WebTesting.view.rightPane.scenario.EditorScenario;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ControlScenarioListPanelController.java
 */
public class ControlScenarioListPanelController implements IController, MouseListener {

	private ControlScenarioListPanel view;
	
	public ControlScenarioListPanelController(ControlScenarioListPanel view){
		this.view = view;
	}
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void notifyController() {
		System.err.println("ScenarioListPanelController notification unused");
	}

	
	@Override
	public void notifyAllControllers() {
		System.err.println("ScenarioListPanelController notification all unused");
	}

	
	@Override
	public Controllers getType() {
		return Controllers.SCENARIO_LIST_PANEL_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}


	/**
	 * Posluchac pro spolupraci se seznamy
	 */
	@Override public void mouseReleased(MouseEvent me) {
		if(me.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(me)){
			
			@SuppressWarnings("unchecked")
			JList<IListItem> list = (JList<IListItem>) me.getSource();
			String newString = "";

			Object source = list.getSelectedValue();
							
			if(Actions.contains(source.toString())){
				newString += actionToTextArea(Actions.getValue(source.toString()));
				
			}
			else if(Parameters.contains(source.toString())){
				
				newString += source.toString() + "=## ##";
			}
			else{
				try {
					throw new UniverzalException();
				} catch (UniverzalException e) {
					System.err.println("blbost");
					e.printStackTrace();
				}
			}

			EditorScenario editorScenario = (EditorScenario) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getView();
			editorScenario.getTextArea().insert(newString, editorScenario.getTextArea().getCaretPosition());
		}
	}
	
	/** Metoda, ktera vraci hodnotu, ktera se doplni do editoru
	 * @param source Polozka v seznamu, na kterou uzivatel kliknul
	 * @return Text, ktery se doplni do editoru
	 */
	private String actionToTextArea(Actions action) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(action);
		
		String [] params = action.getParameters().split(",");
					for(int i = 0 ; i < params.length ; i++){
			sb.append(", " + params[i] + "=## ##");
		}
		sb.append(";");
		
		return sb.toString();
	}
	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}
}
