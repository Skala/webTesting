/**
 * 
 */
package WebTesting.controller.rightPane.scenario;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioUpperPanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 6. 7. 2017
 * File: ControlScenarioUpperPanelController.java
 */
public class ControlScenarioUpperPanelController implements IController {

	private ControlScenarioUpperPanel view;
	
	
	public ControlScenarioUpperPanelController(ControlScenarioUpperPanel view) {
		this.view = view;
	}
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void notifyController() {
		if(getModel() != null){
			this.view.getTfScenarioName().setText(((TreeNode) getModel()).getData().getName());
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Controllers getType() {
		return Controllers.SCENARIO_UPPER_PANEL_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}

	
	

}
