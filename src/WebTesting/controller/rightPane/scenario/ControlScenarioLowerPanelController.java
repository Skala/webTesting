/**
 * 
 */
package WebTesting.controller.rightPane.scenario;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showInfoNotification;
import static WebTesting.core.Config.TYPE_SCENARIO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.sound.midi.SysexMessage;
import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlIDREF;

import org.jdom2.Element;

import WebTesting.Interfaces.IController;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.XmlOperation;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioLowerPanel;
import WebTesting.view.rightPane.scenario.ControlScenarioUpperPanel;
import WebTesting.view.rightPane.scenario.EditorScenario;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: ControlScenarioLowerPanelController.java
 */
public class ControlScenarioLowerPanelController implements IController, ActionListener {

	private ControlScenarioLowerPanel view;
	
	public ControlScenarioLowerPanelController(ControlScenarioLowerPanel view){
		this.view = view;
	}
	
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void notifyController() {
		System.err.println("ScenarioLowerPanel notification is unused");
	}

	@Override
	public void notifyAllControllers() {
		System.err.println("ScenarioLowerPanel notification all is unused");
	}

	@Override
	public Controllers getType() {
		return Controllers.SCENARIO_LOWER_PANEL_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "duplicate":
			duplicateScenario((TreeNode) this.getModel());
			break;
		case "new":
			newScenario();
			break;
		case "undo":
			undo();
			break;
		case "test":
			scenarioToTest();
			break;
		case "update":
			ButtonActions.updateButton((TreeNode) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getModel());
			break;
		case "save":
			ButtonActions.saveButton((TreeNode) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getModel());
			break;
		}
	}
	
	/**Metoda, ktera prevede aktualni scenar v editoru scenare na test. 
	 * 
	 */
	public void scenarioToTest() {
		
		EditorScenarioController ctrl = (EditorScenarioController) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL);
		
		if(ctrl != null){
			if(ctrl.getModel() != null){

				EditorScenario model = (EditorScenario) ctrl.getModel();
				String actualContent = model.getTextArea().getText().replace("##", "");
				actualContent= actualContent.replaceAll("\\s+", "");
				
				String prevContent = ((TreeNode) ctrl.getModel()).getData().elementsToTextScenario().replace(" ", "").replace("##", "");
				prevContent= actualContent.replaceAll("\\s+", "");
				
				if(!actualContent.equals(prevContent)){
					showInfoNotification(getLabel("nodeHasNotBeenSavedYet")+ "\n" + getLabel("saveIt"), getLabel("saveScenario"));
					System.out.println("actual: \n" + actualContent);
					System.out.println("prev: \n" + prevContent);
					
					return;
				}
				else{
					DataNode dataNode = DataNode.convertScenarioToTestXml((TreeNode) ctrl.getModel());
					TreeNode treeNode = new TreeNode(dataNode);
					treeNode.setTypeOfNode(Config.TYPE_TEST);
					
					MainWindow.getController(Controllers.EDITOR_TEST_CTRL).setModel(treeNode);
					((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.EDITOR_TEST);
				}
			
			}
		}
		else{
			JOptionPane.showMessageDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), getLabel("youHaveToFirstSelectScenarioYouWantToCreateTest"), getLabel("fault"), JOptionPane.WARNING_MESSAGE);
		}
		
		
	}

	/**
	 * 
	 */
	private void undo() {
		try {

			File file = new File(((TreeNode) getModel()).getFilePath());

			DataNode newData;
			newData = XmlOperation.loadFromXml(file);
			((TreeNode) getModel()).setData(newData);
			
			MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).notifyController();
			showInfoNotification(getLabel("fileWasLoadedSuccessfully"));

		}catch (NullPointerException e){
			JOptionPane.showMessageDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), getLabel("firstYouHaveToSelectScenario"));
			e.printStackTrace();
		}catch(Exception e){
			JOptionPane.showMessageDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), getLabel("problemDuringLoadingOfSourceFile"));
			e.printStackTrace();
		}
	}
	
	
	/** Metoda, ktera vytvorit novy scenar.
	 * @param node Instance scenare, ktera se dodefinuje.
	 */
	public static void newScenario() {
		
		TreeNode node = new TreeNode(new DataNode());
		node.setTypeOfNode(Config.TYPE_SCENARIO);
		node.setRootElement(new Element(TYPE_SCENARIO).setAttribute("name", "new scenario"));
		node.getData().setName("new scenario");
		
		MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).setModel(node);
		
		System.out.println("Novy scenar vytvoren");
	}

	/**	Metoda, ktera zduplikuje dany scenar
	 * @param currentScenario
	 * @throws NotYetImplementedException 
	 */
	public void duplicateScenario(TreeNode currentScenario) {
		TreeNode newScenario = (TreeNode) currentScenario.clone();
		newScenario.setFilePath(null);
		
		String newName = UtilityClass.newNameDuplicate(newScenario.getData().getName()) ;
				
		newScenario.getData().setName(newName);
		newScenario.getData().getRootElement().setAttribute("name", newName);
		
		ButtonActions.saveButton(newScenario);
		
		MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).setModel(newScenario);
		((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.EDITOR_SCENARIO);
	}
	

}
