/**
 * 
 */
package WebTesting.controller.rightPane.proceed;

import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.model.TestData;
import WebTesting.model.rightPane.proceed.TableProceedModel;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.proceed.EditorProceed;


/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 31. 3. 2017
 * File: ListProceedController.java
 */
public class ListProceedController  {

	
	
	/**
	 * Metoda, ktera ma za ukol vytvorit seznam testu, ktere jsou pridany do editoru pro spusteni
	 */
	public static void populateListProceed(JList<TestData> listProceed) {
		
		DefaultListModel<TestData> model = new DefaultListModel<TestData>();
		
		ArrayList<TestData> data = ((TableProceedModel) ((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).getTable().getModel()).getData();
		
		for(int i = 0 ; i < data.size(); i++){
			model.addElement(data.get(i));
		}
		listProceed.setModel(model);
	}
}
