/**
 * 
 */
package WebTesting.controller.rightPane.proceed.detail;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import WebTesting.Interfaces.IController;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.model.TestData;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.proceed.detail.ControlDetailUpperPanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 7. 2017
 * File: ControlDetailUpperPanelController.java
 */
public class ControlDetailUpperPanelController implements IController, ActionListener {

	private ControlDetailUpperPanel view;
	
	public ControlDetailUpperPanelController(ControlDetailUpperPanel view) {
		this.view = view;
	}
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.TEST_DETAIL_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
	}
	
	
	@Override
	public void notifyController() {
		TestData model = (TestData) this.getModel();
		if(model != null){
			this.view.setTfTestName(model.getNode().getData().getName());
		}
	}

	@Override
	public void notifyAllControllers() {
		// TODO Auto-generated method stub

	}

	@Override
	public Controllers getType() {
		return Controllers.DETAIL_UPPER_PANEL_CTRL;
	}

	@Override
	public Object getView() {
		return this.view;
	}

	

	@Override
	public void actionPerformed(ActionEvent e) {
		((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.EDITOR_PROCEED);
	}

}
