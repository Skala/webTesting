/**
 * 
 */
package WebTesting.controller.rightPane.proceed.detail;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;

import WebTesting.Interfaces.IController;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.proceed.detail.TestDetailOfCommand;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 10. 7. 2017
 * File: TestDetailOfCommandController.java
 */
public class TestDetailOfCommandController implements IController, ActionListener{

	private TestDetailOfCommand view;
	private String model;
	
	public TestDetailOfCommandController(TestDetailOfCommand view) {
		this.view = view;
	}
	
	@Override
	public Object getView() {
		return this.view;
	}
	
	@Override
	public Object getModel() {
		return this.model;
	}

	@Override
	public void setModel(Object model) {
		this.model = (String) model;
		notifyController();
	}
	
	@Override
	public void notifyController() {
		if(this.model != null){
			this.view.getTextArea().setText(this.model);
		}
	}
	
	@Override
	public void notifyAllControllers() {
		// TODO Auto-generated method stub

	}

	@Override
	public Controllers getType() {
		return Controllers.TEST_DETAIL_COMMAND_CTRL;
	}

	

	

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.TEST_DETAIL);
	}

}
