/**
 * 
 */
package WebTesting.controller.rightPane.proceed.detail;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.jdom2.Element;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.model.TestData;
import WebTesting.model.rightPane.proceed.detail.TableDetailModel;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.proceed.detail.ControlDetailUpperPanel;
import WebTesting.view.rightPane.proceed.detail.TestDetail;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: TestDetailController.java
 */
public class TestDetailController implements IController, MouseListener {

	private TestDetail view;
	private TestData model;
	
	public TestDetailController(TestDetail view){
		this.view = view;
	}
	
	@Override
	public Object getModel() {
		return this.model;
	}

	
	@Override
	public void setModel(Object model) {
		this.model = (TestData) model;
		notifyController();
	}
	
	@Override
	public void notifyController() {
		if(this.model != null){
			
			MainWindow.getController(Controllers.DETAIL_UPPER_PANEL_CTRL).notifyController();
			
			//musim ty data zobrazit v tabulce
			TableDetailModel model = (TableDetailModel) this.view.getTable().getModel();
			model.setRootElement(this.model.getNode().getRootElement());
		}
	}
	
	@Override
	public void notifyAllControllers() {
		// TODO Auto-generated method stub

	}

	@Override
	public Controllers getType() {
		return Controllers.TEST_DETAIL_CTRL;
	}

	@Override
	public Object getView() {
		return this.view;
	}

	@Override 
	public void mouseReleased(MouseEvent e) {
		int row = this.view.getTable().getSelectedRow();
		int column = this.view.getTable().getSelectedColumn();
		
		switch(column){
		case 2:
			TableDetailModel model = (TableDetailModel) this.view.getTable().getModel();
			Element child = model.getChild(row);
			this.view.getTable().setValueAt(child, row, column);
			break;
		}
		System.out.println("table listener: " + row + ":" + column);
	}
	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}
}
