/**
 * 
 */
package WebTesting.controller.rightPane.proceed;

import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabProceed;
import WebTesting.view.rightPane.proceed.ControlProceedUpperPanel;
import WebTesting.view.rightPane.proceed.EditorProceed;
import net.sourceforge.htmlunit.corejs.javascript.tools.debugger.Main;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.rightPane.proceed.EditorProceedController.runChosen;
import static WebTesting.core.Config.COLOR_BLUE;
import static WebTesting.core.Config.COLOR_GREEN;
import static WebTesting.core.Config.STATUS_ERROR;
import static WebTesting.core.Config.STATUS_RUNNING;
import static WebTesting.view.rightPane.proceed.EditorProceed.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import WebTesting.Interfaces.IController;
import WebTesting.core.MainTest;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.TestData;
import WebTesting.model.leftPane.ListProceedRenderer;
import WebTesting.model.rightPane.proceed.TableProceedModel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 3. 2017
 * File: EditorProceedController.java
 */
public class EditorProceedController implements IController, ActionListener, MouseListener{
	
	public EditorProceed view;
	
	private static int maxNumberOfThreads;
	private static int actualNumberOfThreads;
	
	public static int firefoxMax = 2;
	public static int guilessMax = 5;
	
	private static int indexOfTest = 0;
	
	
	private static boolean lastTest;
	
	public EditorProceedController(EditorProceed view){
		this.view = view;
		setTable();
	}
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
	}
	
	
	/**
	 * Metoda pro vytvoreni tabulky s testy
	 */
	private void setTable(){

		DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) this.view.getTable().getTableHeader().getDefaultRenderer();
		renderer.setHorizontalAlignment(SwingConstants.CENTER);
		renderer.setVerticalAlignment(SwingConstants.CENTER);

		this.view.getTable().getColumnModel().getColumn(0).setMinWidth(50);
		this.view.getTable().getColumnModel().getColumn(0).setMaxWidth(50);
		this.view.getTable().getColumnModel().getColumn(1).setPreferredWidth(300);
		this.view.getTable().getColumnModel().getColumn(2).setMinWidth(200);
		this.view.getTable().getColumnModel().getColumn(2).setMaxWidth(200);
		this.view.getTable().getColumnModel().getColumn(3).setMinWidth(80);
		this.view.getTable().getColumnModel().getColumn(3).setMaxWidth(80);
		
		this.view.getTable().setRowHeight(30);
	}
	
	/**
	 * metoda, ktera reaguje na stisknuti tlacitka pro vymazani dokoncenych testu z tabulky
	 */
	public void deleteFinished(){
		int retVal = JOptionPane.showConfirmDialog(
				((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()), 
				getLabel("doYouReallyWantToDeleteFinishedtests"), getLabel("choice") ,JOptionPane.INFORMATION_MESSAGE);
		if(retVal == JOptionPane.OK_OPTION){
			TableProceedModel model = (TableProceedModel) ((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).
					getTable().getModel();

			int size = model.getData().size();
			int index = 0;

			while(index < size){
				if(model.getData().get(index).getProgress() == 100){
					model.getData().remove(index);
					model.fireTableRowsDeleted(index, index);
					size--;
				}
				else{
					index ++;
				}
			}
			ListProceedController.populateListProceed(((TabProceed)MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView()).getListProceed());
		}
	}
	
	/**
	 * Metoda, ktera reaguje na stisknuti tlacitka pro smazani zvolenych testu z tabulky
	 */
	public void deleteSelected(){
		int retVal = JOptionPane.showConfirmDialog(
				((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()), 
				getLabel("doYouReallyWantToDeleteChosenTests"), getLabel("choice") ,JOptionPane.INFORMATION_MESSAGE);
		if(retVal == JOptionPane.OK_OPTION){
			TableProceedModel model = (TableProceedModel) ((EditorProceed) 
					MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()
					).getTable().getModel();
			
			int size = model.getData().size();
			int index = 0;
			
			while(index < size){
				if(model.getData().get(index).isChosen()){
					model.getData().remove(index);
					model.fireTableRowsDeleted(index, index);
					size--;
				}
				else{
					index ++;
				}
			}
			ListProceedController.populateListProceed(((TabProceed) 
					MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView()).getListProceed());
		}
	}

	/**
	 * metoda, ktera reaguje na kliknuti do zaskrtavaciho tlacitka v leve dolni casti editoru. Oznaci nebo zrusi oznaceni u vsech testu
	 */
	public void chooseAll(boolean choose) {
		TableProceedModel model = (TableProceedModel) ((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).getTable().getModel();
		for(int i = 0; i < model.getData().size(); i++){
			model.getData().get(i).setChosen(choose);
		}
		model.fireTableDataChanged();
	}
	
	/**
	 * Metoda, ktera provede akci na aktualnim polickem tabulky
	 */
	public void tableActions(){
		
		EditorProceed editorProceed = (EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView();
		
		int row = editorProceed.getTable().getSelectedRow();
		int column = editorProceed.getTable().getSelectedColumn();
		
		switch(column){
		case 0:
			editorProceed.getTable().setValueAt(null, row, column);
			break;
		case 1:
			break;
		case 2: 
			break;
		case 3:
			TableProceedModel model = (TableProceedModel) editorProceed.getTable().getModel();
			TestData data = model.getData().get(row);
			editorProceed.getTable().setValueAt(data, row, column);
			break;
		}
		
		System.out.println("table listener: " + row + ":" + column);
	}
	
	
	public void runAll(){
		int retVal = JOptionPane.showConfirmDialog(((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()), getLabel("doYouReallyWantToRunAllTests"), getLabel("choice") ,JOptionPane.INFORMATION_MESSAGE);
		
		if(retVal == JOptionPane.OK_OPTION){
			TableProceedModel model = (TableProceedModel) ((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).getTable().getModel();
			runTests(model.getData(), false);
		}
	}

	/**
	 * metoda, ktera reaguje na stisknuti tlacitka pro spusteni oznacenych testu z tabulky
	 */
	public static void runChosen(){
		int retVal = JOptionPane.showConfirmDialog(((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView()), getLabel("doYouReallyWantToRunChosenTests"), getLabel("choice") ,JOptionPane.INFORMATION_MESSAGE);

		if(retVal == JOptionPane.OK_OPTION){
			TableProceedModel model = (TableProceedModel) ((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).getTable().getModel();
			
			ArrayList<TestData> list = new ArrayList<TestData>();
			
			for(int i = 0 ; i < model.getData().size() ; i++){
				if(model.getData().get(i).isChosen()){
					list.add(model.getData().get(i));
				}
			}
			runTests(list, false);
		}
	}
	
	
	/**
	 * Metoda, ktera dekrementuje pocet aktualne provadenych testu. Metoda je volana pouze z vyhodnocovaciho mechanismu po dokoncenem testu.
	 */
	public static void decrementActualNumberOfThread(){
		actualNumberOfThreads--;
		if(actualNumberOfThreads == 0 && lastTest){
			((ControlProceedUpperPanel) MainWindow.getController(Controllers.PROCEED_UPPER_PANEL_CTRL).getView()).
				getRunningStatus().setBackground(COLOR_GREEN);
			((ControlProceedUpperPanel) MainWindow.getController(Controllers.PROCEED_UPPER_PANEL_CTRL).getView()).
				getRunningStatus().setText("Dokonceno");
		}
	}
	
	public static int getActualNumberOfThreads() {
		return actualNumberOfThreads;
	}
	
	/** Metoda, ktera provede spusteni vsech pozadovanych testu
	 * @param list Seznam s testy, ktere jsou urceny ke spusteni
	 * @param runChoosed Pravdivostni hodnota, ktera oznacuje, zda chceme spustit vsechny testy z tabulky, nebo jen vybrane.
	 */
	private static void runTests(ArrayList<TestData> list, boolean runChoosed) {
		
		ControlProceedUpperPanel upperPanel = (ControlProceedUpperPanel) MainWindow.getController(Controllers.PROCEED_UPPER_PANEL_CTRL).getView();
		
		maxNumberOfThreads = ((upperPanel.showInBrowser()) ? firefoxMax : guilessMax ); 
		actualNumberOfThreads = 0;
		lastTest = false;
		MainTest.setRun(true);	
		
		upperPanel.getRunningStatus().setBackground(COLOR_BLUE);
		upperPanel.getRunningStatus().setText(getLabel("testsAreRunning"));
		
		for(int i = 0 ; i < list.size(); i++){
			list.get(i).getNode().getRootElement().setAttribute("status", STATUS_RUNNING);
			list.get(i).setProgress(0);
		}
		
		indexOfTest=0;
		
		new Thread(){
			public void run() {
				
				while(indexOfTest < list.size() && MainTest.getRun()){
					
					if(actualNumberOfThreads < maxNumberOfThreads){
						new MainTest(list.get(indexOfTest));
						actualNumberOfThreads++;
						indexOfTest++;	
					}
					else{
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				
				while(indexOfTest < list.size()){
					list.get(indexOfTest).getNode().getRootElement().setAttribute("status", STATUS_ERROR);
					indexOfTest++;
				}
				
				((TabProceed) MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView()).getListProceed().repaint();
				lastTest = true;
				upperPanel.getRunningStatus().setBackground(COLOR_GREEN);
				upperPanel.getRunningStatus().setText(getLabel("finished"));
				
			};
		}.start();
	}

	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Controllers getType() {
		return Controllers.EDITOR_PROCEED_CTRL;
	}

	@Override
	public Object getView() {
		return this.view;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "choose":
			chooseAll(this.view.getChBChoose().isSelected());
			break;
		case "runAll":
			runAll();
			break;
		case "deleteSelected":
			deleteSelected();
			break;
		case "deleteFinished":
			deleteFinished();
			break;
		case "runChosen":
			runChosen();
			break;
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		tableActions();
	}

	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}

	
	
	
}
