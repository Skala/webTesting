/**
 * 
 */
package WebTesting.controller.rightPane.proceed;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import WebTesting.Interfaces.IController;
import WebTesting.core.MainTest;
import WebTesting.enums.Controllers;
import WebTesting.view.rightPane.proceed.ControlProceedUpperPanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ControlProceedUpperPanelController.java
 */
public class ControlProceedUpperPanelController implements IController, ActionListener {

	private ControlProceedUpperPanel view;
	

	public ControlProceedUpperPanelController(ControlProceedUpperPanel view) {
		this.view = view;
	}
	
	@Override
	public void notifyController() {
		// TODO Auto-generated method stub

	}

	
	@Override
	public void notifyAllControllers() {
		// TODO Auto-generated method stub

	}

	
	@Override
	public Controllers getType() {
		return Controllers.PROCEED_UPPER_PANEL_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "cancelTests":
			MainTest.setRun(false);
			break;
		}
	}

	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}

}
