/**
 * 
 */
package WebTesting.controller.rightPane;

import java.awt.CardLayout;

import javax.swing.Icon;

import com.gargoylesoftware.htmlunit.javascript.host.SharedWorker;

import WebTesting.Interfaces.IController;
import WebTesting.Interfaces.IRightPane;
import WebTesting.controller.leftPane.TreePanelController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.TreePanel;
import WebTesting.view.rightPane.RightPane;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: RightPaneController.java
 */
public class RightPaneController implements IController {

	private RightPane view;
	
	
	public RightPaneController(RightPane view) {
		this.view = view;
	}
	
	
	/*----------------*
	 * Ostatni metody *
	 *----------------*/
	
	/**
	 * Metoda, ktera nam zobrazi pozadovany panel
	 * @param panel Pozadovany panel
	 */
	public void showPanel(IRightPane panel){
		
		CardLayout cl = (CardLayout) this.view.getLayout();
		cl.show(this.view, panel.getPaneTitle());
		
	
		int indexOfTab;
		if(panel == this.view.getEditorScenario()){
			indexOfTab = 1;
		}
		else if(panel == this.view.getEditorProceed() || panel == this.view.getTestDetail() || panel == this.view.getTestDetailOfCommand()){
			indexOfTab = 2;
		}
		else{
			indexOfTab = 0;
		}
		
		((TreePanel) ((TreePanelController) MainWindow.getController(Controllers.TREE_PANEL_CTRL)).getView()).
			getTabbedPane().setSelectedIndex(indexOfTab);
	}
	
	public void showPanel(RightPaneEditor editor){
		switch(editor){
		case EDITOR_TEST:
			showPanel(this.view.getEditorTests());
			break;
		case EDITOR_SCENARIO:
			showPanel(this.view.getEditorScenario());
			break;
		case EDITOR_PROCEED:
			showPanel(this.view.getEditorProceed());
			break;
		case TEST_DETAIL:
			showPanel(this.view.getTestDetail());
			break;
		case COMMAND_DETAIL:
			showPanel(this.view.getTestDetailOfCommand());
			break;
		}
	}
	
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public Controllers getType() {
		return Controllers.RIGHT_PANEL_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}


	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IController#getModel()
	 */
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IController#setModel(java.lang.Object)
	 */
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}

}
