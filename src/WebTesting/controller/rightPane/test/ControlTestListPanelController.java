/**
 * 
 */
package WebTesting.controller.rightPane.test;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.SwingUtilities;

import WebTesting.Interfaces.IController;
import WebTesting.Interfaces.IListItem;
import WebTesting.enums.Controllers;
import WebTesting.enums.tests.Attributes;
import WebTesting.enums.tests.Locators;
import WebTesting.enums.tests.Tags;
import WebTesting.enums.tests.Values;
import WebTesting.enums.tests.Waits;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.test.source.ControlTestListPanel;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 6. 7. 2017
 * File: ControlTestListPanelController.java
 */
public class ControlTestListPanelController implements IController, MouseListener {

	private ControlTestListPanel view;
	
	
	public ControlTestListPanelController(ControlTestListPanel view) {
		this.view = view;
	}
	
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public Controllers getType() {
		return Controllers.TEST_LIST_PANEL_CTRL;
	}

	@Override
	public Object getView() {
		return this.view;
	}


	
	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent me) {
		if(me.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(me)){
			@SuppressWarnings("unchecked")
			JList<IListItem> list = ((JList<IListItem>) me.getSource());
			String newString = "";
			
			Object source = list.getSelectedValue();
			
			if(Locators.contains(source.toString())){
				System.out.println("kliknuto na lokator");
				newString += source.toString();
			}
			else if(Tags.contains(source.toString())){
				System.out.println("kliknuto na tag");
				newString += source.toString();
							
			}
			else if(Attributes.contains(source.toString())){
				System.out.println("kliknuto na atribut");
				newString += source.toString();
			}
			else if(Values.contains(source.toString())){
				System.out.println("kliknuto na hodnotu");
				newString += source.toString();
			}
			else if(Waits.contains(source.toString())){
				System.out.println("Kliknuto na cekani");
				newString += source.toString();
			}
			else{
				System.out.println("blbost");
			}
			
			if(!constantList(newString)){
				newString += "=## ##";
			}
			
			EditorTestSource editor = (EditorTestSource) MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).getView();
			editor.getTextArea().insert(newString, editor.getTextArea().getCaretPosition());
		}
	}
	
	/** Metoda slouzici pro vypis konstant, ktere maji jiny vysledny format v editoru.
	 * @param ret Vkladana konstanta do editoru
	 * @return Pravdivostni hodnotu, zda se jedna o konstantu
	 */
	private boolean constantList(String ret){
		switch(ret){
		case "forward":
		case "back":
			return true;
		}
		return false;
	}
	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mousePressed(MouseEvent e) {}


	/* (non-Javadoc)
	 * @see WebTesting.Interfaces.IController#getModel()
	 */
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}
}
