/**
 * 
 */
package WebTesting.controller.rightPane.test;

import static WebTesting.controller.UtilityClass.getLabel;

import java.util.List;

import org.jdom2.Element;

import WebTesting.Interfaces.IController;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.NullModelException;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.EditorTest;
import WebTesting.view.rightPane.test.modules.ControlTestLowerPanel;
import WebTesting.view.rightPane.test.modules.ControlTestMiddlePanel;
import WebTesting.view.rightPane.test.modules.ControlTestUpperPanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: EditorTestController.java
 */
public class EditorTestController implements IController {

	private EditorTest view;
	private TreeNode model;
	
	
	public EditorTestController(EditorTest view) {
		this.view = view;
	}
	
	
	/** Vrati instanci soucasneho testu
	 * @return Instance soucasneho testu
	 */
	public TreeNode getModel() {
		return this.model;
	}
	
	/** Nastaveni soucasneho test na vybrany
	 * @param currentTest Novy test k zobrazeni v editoru
	 */ 
	@Override
	public void setModel(Object model) {
		this.model = (TreeNode) model;
		notifyController();
	}
	
	
	
	/** Metoda, ktera vrati seznam vsech prikazu obsazenych v testu
	 * @return List prikazu v testu
	 */
	public List<Element> getCurrentCommands()
	{
		return this.model.getData().getRootElement().getChildren("command");
	}
	
	@Override
	public void notifyController() {
		if(this.model != null){

			switch(this.view.getMiddleControl().getTabbedPane().getSelectedIndex()){
			case 0:
				this.view.getMiddleControl().getControlTestPanel().get(0).reloadCurrentTest();
				break;
			case 1:
				this.view.getMiddleControl().getControlTestPanel().get(1).reloadCurrentTest();
				break;
			default:
				try {
					throw new NotYetImplementedException();
				} catch (NotYetImplementedException e) {
					e.printStackTrace();
				}
				break;
			}
			((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.EDITOR_TEST);  
			notifyAllControllers();
		}
		else{
			try {
				throw new NullModelException();
			} catch (NullModelException e) {
				e.printStackTrace();
			}
		}
	}

	
	@Override
	public void notifyAllControllers() {
		MainWindow.getController(Controllers.TEST_UPPER_PANEL_CTRL).notifyController();
		MainWindow.getController(Controllers.TEST_LOWER_PANEL_CTRL).notifyController();
		MainWindow.getController(Controllers.TEST_MIDDLE_PANEL_CTRL).notifyController();
	}

	
	@Override
	public Controllers getType() {
		return Controllers.EDITOR_TEST_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}
}
