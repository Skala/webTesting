/**
 * 
 */
package WebTesting.controller.rightPane.test;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.newNameDuplicate;
import static WebTesting.controller.UtilityClass.showAlertNotification;
import static WebTesting.controller.UtilityClass.showInfoNotification;
import static WebTesting.core.Config.TYPE_TEST;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import org.jdom2.Element;

import WebTesting.Interfaces.IController;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.XmlOperation;
import WebTesting.controller.rightPane.RightPaneController;
import WebTesting.controller.rightPane.proceed.ListProceedController;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.RightPaneEditor;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.DataNode;
import WebTesting.model.TestData;
import WebTesting.model.TreeNode;
import WebTesting.model.rightPane.proceed.TableProceedModel;
import WebTesting.view.MainWindow;
import WebTesting.view.leftPane.tabs.TabProceed;
import WebTesting.view.rightPane.proceed.EditorProceed;
import WebTesting.view.rightPane.test.modules.ControlTestLowerPanel;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ControlTestLowerPanelController.java
 */
public class ControlTestLowerPanelController implements IController, ActionListener{

	
	private ControlTestLowerPanel view;
	
	public ControlTestLowerPanelController(ControlTestLowerPanel view){
		this.view = view;
	}
	
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Object getView() {
		return this.view;
	}
	
	@Override
	public void notifyController() {
		System.err.println("testLowerController notification unused");
	}

	
	@Override
	public void notifyAllControllers() {
		System.err.println("testLowerController notify all unused");
	}

	
	@Override
	public Controllers getType() {
		return Controllers.TEST_LOWER_PANEL_CTRL;
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		TreeNode dataNode = (TreeNode) getModel();
		
		switch(e.getActionCommand()){
		case "duplicate":
			duplicateTest(dataNode);
			break;
		case "new":
			newTest();
			break;
		case "undo":
			undo();
			break;
		case "add":
			addTest((TreeNode) this.getModel());
			break;
		case "update":
			ButtonActions.updateButton((TreeNode) this.getModel());
			break;
		case "save":
			ButtonActions.saveButton((TreeNode) this.getModel());
			break;

		}
	}


	/**
	 * Metoda, ktera prida test z parametru do tabulky testu pripravenych na spusteni
	 */
	public void addTest(TreeNode test) {
		
		if(test != null){
			
			String actualString = ((EditorTestSource) MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).getView()).getTextArea().getText();

			actualString = actualString.replace("##", "");
			actualString = actualString.replaceAll("\\s+", "");
			
			String prevString = test.getData().elementsToTextTest();
			prevString = prevString.replace("##", "");
			prevString = prevString.replaceAll("\\s+", "");
		
			System.out.println("actual: \n" + actualString);
			System.out.println("prev: \n" + prevString);
			
			if(!actualString.equals(prevString)){
				showInfoNotification(getLabel("FirstTestMustBeSaved"), getLabel("saveTest"));
				return;
			}
			
			ButtonActions.removeWhenExists(test);
			test.resetAllStatus();
			
			((TableProceedModel)(((EditorProceed) MainWindow.getController(Controllers.EDITOR_PROCEED_CTRL).getView()).getTable().getModel())).myAddRow(new TestData(test));
			ListProceedController.populateListProceed(((TabProceed) MainWindow.getController(Controllers.TAB_PROCEED_CTRL).getView()).getListProceed());
			((RightPaneController) MainWindow.getController(Controllers.RIGHT_PANEL_CTRL)).showPanel(RightPaneEditor.EDITOR_PROCEED);
			
			
		}
		else{
			showInfoNotification(getLabel("youHaveToFirstSelectTestToRun"));
			//JOptionPane.showMessageDialog(null, "Nejdrive musite vybrat test pro spusteni", "Current je null", JOptionPane.INFORMATION_MESSAGE);
		}
	}


	/**
	 * Natazeni ulozeneho testu ze souboru a ztrata neulozenych dat
	 */
	public void undo() {
		try {
			
			EditorTestController ctrl = (EditorTestController) MainWindow.getController(Controllers.EDITOR_TEST_CTRL);
			
			File file = new File(ctrl.getModel().getFilePath());
			DataNode newData;
			newData = XmlOperation.loadFromXml(file);
			ctrl.getModel().setData(newData);
			ctrl.notifyController();
			showInfoNotification(getLabel("fileWasLoadedSuccessfully"));
		}catch (NullPointerException ee){
			//JOptionPane.showMessageDialog(MainWindow.frame, "Operaci nelze provest, protoze neni otevren uzel, nad kterym bychom akci mohli provest");
			showAlertNotification(getLabel("firstYouHaveToSelectTest"));
			ee.printStackTrace();
		}catch(Exception ee){
			//JOptionPane.showMessageDialog(MainWindow.frame, "Problem pri nacitani zdrojoveho souboru");
			showAlertNotification(getLabel("problemDuringLoadingOfSourceFile"));
			ee.printStackTrace();
		}
	}
	
	/** Metoda, ktera je volana pri vytvareni noveho testu
	 * @param node Vytvareny uzel
	 */
	public void newTest() {
		
		TreeNode node = new TreeNode(new DataNode());
		node.setTypeOfNode(Config.TYPE_TEST);
		node.setRootElement(new Element(TYPE_TEST).setAttribute("name", "new test"));
		//TODO definice nazvu na dvou mistech... zde + data.getName(); mozna bude chtit upravit
		MainWindow.getController(Controllers.EDITOR_TEST_CTRL).setModel(node);
		
		System.out.println("Novy test vytvoren");
	}
	
	
	/** Metoda, ktera zduplikujej test, ktery je obsazen v parametru funkce
	 * @param currentTest Test, ktery se duplikuje
	 */
	public void duplicateTest(TreeNode currentTest){
		
		TreeNode node = (TreeNode) currentTest.clone();
		node.setFilePath(null);
		
		String newName = newNameDuplicate(node.getData().getName());
		
		node.getData().setName(newName);
		node.getData().getRootElement().setAttribute("name", newName);
				
		ButtonActions.saveButton(node);
		MainWindow.getController(Controllers.EDITOR_TEST_CTRL).setModel(node);
	}
}
