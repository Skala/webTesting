/**
 * 
 */
package WebTesting.controller.rightPane.test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.modules.ControlTestUpperPanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ControlTestUpperPanelController.java
 */
public class ControlTestUpperPanelController implements  IController {

	private ControlTestUpperPanel view; 
	
	public ControlTestUpperPanelController(ControlTestUpperPanel view){
		this.view = view;
	}
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void notifyController() {
		
		if(getModel() != null){
			DataNode data = ((TreeNode) getModel()).getData();
			if(data != null){
				this.view.setTfTitleTest(data.getName());
			}
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException("controller has no followers");
		} catch (NotYetImplementedException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public Controllers getType() {
		return Controllers.TEST_UPPER_PANEL_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}
}
