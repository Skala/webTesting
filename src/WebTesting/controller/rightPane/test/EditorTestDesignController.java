/**
 * 
 */
package WebTesting.controller.rightPane.test;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.view.rightPane.test.design.EditorTestDesign;
import WebTesting.view.rightPane.test.source.EditorTestSource;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: EditorTestDesignController.java
 */
public class EditorTestDesignController implements IController {

private EditorTestDesign view;
	
	public EditorTestDesignController(EditorTestDesign view) {
		this.view = view;
	}
	
	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public Controllers getType() {
		return Controllers.EDITOR_TEST_DESIGN_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}

	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}

}
