/**
 * 
 */
package WebTesting.controller.rightPane.test;

import java.nio.channels.NotYetConnectedException;

import org.openqa.selenium.remote.server.handler.GetElementDisplayed;

import WebTesting.Interfaces.IControlTest;
import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.EditorTest;
import WebTesting.view.rightPane.test.source.EditorTestSource;
import net.sourceforge.htmlunit.corejs.javascript.tools.debugger.Main;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ControlTestSourceController.java
 */
public class EditorTestSourceController implements IController {

	private EditorTestSource view;
	
	public EditorTestSourceController(EditorTestSource view) {
		this.view = view;
	}
	
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void notifyController() {
		TreeNode node = (TreeNode) getModel(); 
		
		if(node != null){
			if(node.getData() != null){
				this.view.getTextArea().setText(node.getData().elementsToTextTest());
			}
		}
	}

	
	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Override
	public Controllers getType() {
		return Controllers.EDITOR_TEST_SOURCE_CTRL;
	}

	
	@Override
	public Object getView() {
		return this.view;
	}
}
