/**
 * 
 */
package WebTesting.controller.rightPane.test;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showAlertNotification;
import static WebTesting.controller.UtilityClass.showInfoNotification;
import static WebTesting.core.Config.COMMAND;
import static WebTesting.core.Config.LABEL;
import static WebTesting.core.Config.TYPE_SCENARIO;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.jdom2.Element;

import WebTesting.Interfaces.IController;
import WebTesting.controller.ButtonActions;
import WebTesting.controller.UtilityClass;
import WebTesting.controller.XmlOperation;
import WebTesting.core.Config;
import WebTesting.enums.Controllers;
import WebTesting.enums.scenarios.Actions;
import WebTesting.enums.scenarios.Parameters;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.exceptions.SemanticException;
import WebTesting.exceptions.UniverzalException;
import WebTesting.exceptions.UnknownElementTypeException;
import WebTesting.exceptions.UnknownTypeOfLocator;
import WebTesting.exceptions.UnknownTypeOfNodeException;
import WebTesting.exceptions.UnknownTypeOfParameter;
import WebTesting.model.DataNode;
import WebTesting.model.TreeNode;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.scenario.ControlScenarioListPanel;
import WebTesting.view.rightPane.scenario.EditorScenario;
import WebTesting.view.rightPane.test.modules.ControlTestMiddlePanel;
import WebTesting.view.rightPane.test.source.modify.ModifyTestWindow;
import net.sourceforge.htmlunit.corejs.javascript.Token.CommentType;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ModifyTestWindowController.java
 */
public class ModifyTestWindowController implements IController, ActionListener {

	private ModifyTestWindow view;

	public ModifyTestWindowController(ModifyTestWindow view) {
		this.view = view;
	}

	@Override
	public void notifyController() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void notifyAllControllers() {
		try {
			throw new NotYetImplementedException();
		} catch (NotYetImplementedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public Controllers getType() {
		return Controllers.MODIFY_TEST_SOURCE_CTRL;
	}


	@Override
	public Object getView() {
		return this.view;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "addCommand":
			addCommand();
			break;
		case "removeCommand":
			removeCommand();
			break;
		case "removeParam":
			removeParam();
			break;
		case "saveScenario":
			saveScenario();
			break;
		case "close":
			closeEdit();
			break;
		case "save":
			save();
			break;
		default:
			break;
		}

	}

	private void addCommand() {
		//JTextField pozice = new JTextField();


		System.out.println(new DataNode(this.view.getElement()).elementsToString());

		JTextField akce = new JTextField();
		JComponent [] inputs = {new JLabel(getLabel("titleOfAction")), akce}; //new JLabel("Pozice"), pozice ,

		/* vypis prihlasovaciho okna */
		if (JOptionPane.showConfirmDialog((MainWindow) MainWindow.getController(Controllers.MAIN_WINDOW_CTRL).getView(), 
				inputs, getLabel("addCommand"), JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
		{
			if(!UtilityClass.isItemInList(Actions.getValues(), akce.getText().trim())){
				showInfoNotification(getLabel("unknownAction") + "\n" + getLabel("commandNotAdded"));
				return;
			}

			int position = this.view.getTextArea().getCaretPosition();
			String text = this.view.getTextArea().getText().substring(0, position);
			int index = UtilityClass.getNumberOfContents(text, ';');

			System.err.println("caret: " + index);

			Element newEl = new Element(COMMAND).setAttribute("action", akce.getText().trim());

			int pp = XmlOperation.getElementPositionFromContent(this.view.getElement(), index);

			this.view.getElement().addContent(pp, newEl);

			showInfoNotification(getLabel("commandAdded"));

			this.view.getTextArea().setText(printToTextArea());
			((ControlTestMiddlePanel) MainWindow.getController(Controllers.TEST_MIDDLE_PANEL_CTRL).getView()).getControlTestPanel().get(0).reloadCurrentTest();
		}
	}
	
	
	/**
	 * Posluchac, ktery reaguje na stisknuti tlacitka pro odebrani prikazu z testu
	 */
	private void removeCommand() {
		try{
			int index = this.view.getTextArea().getCaretPosition();
			String text = this.view.getTextArea().getText().substring(0, index);
			int count = UtilityClass.getNumberOfContents(text, ';');
			
			int position = XmlOperation.getElementPositionFromContent(this.view.getElement(), count);
			this.view.getElement().removeContent(position);
			
			showInfoNotification(getLabel("commandRemoved"));
			
			this.view.getTextArea().setText(printToTextArea());
			((ControlTestMiddlePanel) MainWindow.getController(Controllers.TEST_MIDDLE_PANEL_CTRL).getView()).getControlTestPanel().get(0).reloadCurrentTest();
		}catch(IndexOutOfBoundsException e){
			showInfoNotification(getLabel("commandToDeleteNotFound"));
			e.printStackTrace();
		}catch(Exception e){
			showInfoNotification(getLabel("commandNotDeleted"));
			e.printStackTrace();
		}
	}
	
	private void removeParam() {
		int index = this.view.getTextArea().getCaretPosition();
		String ret = this.view.getTextArea().getText().substring(0, index);
		int count = UtilityClass.getNumberOfContents(ret, ',') -1;
		System.err.println("caret parame: " + (count));
		if(count >= 0){
			
			int position = XmlOperation.getElementPositionFromContent(this.view.getElement(), count);
			
			System.out.println("remove");
			this.view.getElement().removeContent(position);
		}
		else{
			try {
				throw new UniverzalException(getLabel("parameterNotRemoved"));
			} catch (UniverzalException e1) {
				e1.printStackTrace();
			}
		}
		this.view.getTextArea().setText(printToTextArea());
		((ControlTestMiddlePanel) MainWindow.getController(Controllers.TEST_MIDDLE_PANEL_CTRL).getView()).getControlTestPanel().get(0).reloadCurrentTest();
	}
	
	private void saveScenario() {
		try{
			saveTextToCommands();
		} catch (UniverzalException e) {
			showAlertNotification(getLabel("controlCharactersNotPaired") + "\n" + getLabel("unsaved"));
			e.printStackTrace();
		}
		
		((EditorScenario) MainWindow.getController(Controllers.EDITOR_SCENARIO_CTRL).getView()).getTextArea().setText(this.view.getTextArea().getText());
		
		DataNode dataNode = new DataNode(this.view.getElement());
		TreeNode treeNode = new TreeNode(dataNode);
		treeNode.setTypeOfNode(TYPE_SCENARIO);
		
		try {
			ButtonActions.saveScenarioFromTest(treeNode);
		} catch (IndexOutOfBoundsException | UnknownElementTypeException | UnknownTypeOfNodeException
				| UnknownTypeOfParameter | SemanticException | UnknownTypeOfLocator | NotYetImplementedException
				| UniverzalException e) {
			showAlertNotification(getLabel("convertTestToScenarioError") + "\n" + e.getMessage());
			e.printStackTrace();
		}
		showInfoNotification(getLabel("scenarioSuccessfullySaved"));
	}
	
	private void save() {
		try {
			saveTextToCommands();
			((ControlTestMiddlePanel) MainWindow.getController(Controllers.TEST_MIDDLE_PANEL_CTRL).getView()).getControlTestPanel().get(0).reloadCurrentTest();
			closeEdit();
		} catch (UniverzalException e) {
			showAlertNotification(getLabel("controlCharactersNotPaired") + "\n" + getLabel("unsaved"));
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void closeEdit() {
		this.view.dispose();
	}
	
	/** Metoda, ktera vraci text, ktery se vykresli v editoru
	 * @return Text do editoru
	 */
	public String printToTextArea(){
		if(this.view.getElementIndex() != -1){
			return new DataNode(this.view.getElement()).commandToTextScenario(this.view.getElement()); //elementToTextArea();
		}
		else{
			return new DataNode(this.view.getElement()).elementsToTextScenario();
		}
	}
	
	/**
	 * Metoda, ktera provede vykresleni pozadovanych komponent v zavislosti na editaci prikazu nebo testu.
	 */
	public void setVisibility(){
		if(this.view.getElementIndex() == -1){
			this.view.getBtnAddCommand().setVisible(true);
			this.view.getBtnRemoveCommand().setVisible(true);
			this.view.getBtnRemoveParam().setVisible(false);
			this.view.getBtnSaveScenario().setVisible(true);
			this.view.setTitle(getLabel("modifyTest"));
		}
		else{
			this.view.getBtnAddCommand().setVisible(false);
			this.view.getBtnRemoveCommand().setVisible(false);
			this.view.getBtnRemoveParam().setVisible(true);
			this.view.getBtnSaveScenario().setVisible(false);
			this.view.setTitle(getLabel("modifyCommand"));
		}
	}

	/** Metoda, ktera zapise proveden zmeny do XML elementu.
	 * @throws UniverzalException
	 */
	private void saveTextToCommands() throws UniverzalException{
		
		String faultMessage = "";
		
		String [] tokens = this.view.getTextArea().getText().trim().split(Config.endOfCommand); 
		//pokud jsme editovali jen jeden prikaz
		if(this.view.getElementIndex() != -1){
			try{
				saveTextToCommand(this.view.getElement(), tokens[0]);
			}catch (UniverzalException e){
				faultMessage += (tokens[0] + System.lineSeparator());
			}
		}
		//Editace celeho testu
		else{
			List<Element> list = this.view.getElement().getChildren(COMMAND);
			for(int i = 0 ; i < tokens.length; i++){
				tokens[i] = tokens[i].trim();
				
				int pocet = UtilityClass.getNumberOfActionsInLine(tokens[i]); 
				if(pocet == 0){
					showInfoNotification(getLabel("onLineDoesNotOccurAnyTypeOfEvent"));
					return;
				}
				if(pocet > 1){
					showInfoNotification(getLabel("onLineThereAreMoreTypesOfEvents") + ": " + (i+1) + ", (" + pocet + ")");
					return;
				}
				
				try {
					saveTextToCommand(list.get(i), tokens[i]);
				}catch (UniverzalException e){
					faultMessage += (tokens[i] + System.lineSeparator());
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}	
		if(!UtilityClass.isNullOrEmpty(faultMessage)){
			
			showAlertNotification(getLabel("followingCommandsWereNotSavedBecauseTheyWereNotPairedControlCharacters") + "\n" + faultMessage, getLabel("unsaved"));
			//			JOptionPane.showMessageDialog(MainWindow.frame, "Nasledujici prikazy nebyly ulozeny, protoze byly nesparovane ridici prikazy \n" + faultMessage , 
//					"Neulozeno", JOptionPane.WARNING_MESSAGE);
		}
	}
	
		
	/** Metoda, ktera ulozi jeden prikaz
	 * @param element Element, do ktereho data ukladame, respektive aktualizujeme
	 * @param line Radka, ze ktere ziskame data
	 * @throws UniverzalException 
	 */
	private void saveTextToCommand(Element element, String line) throws UniverzalException{
		
		if((UtilityClass.getNumberOfSubSequences(line, Config.constantTag) % 2 ) != 0){
			throw new UniverzalException(getLabel("controlCharactersNotPairedCommandNotSaved"));
		}
		
		String [] subLine = UtilityClass.mySplit(line, Config.subCommandSeparator);
		for(int i = 0 ; i < subLine.length; i++){
			if(i == 0 ){
				updateAction(element, subLine[i].trim());
			}
			else{
				updateElement(element, subLine[i]);
			}
		}
	}
	
	/** Metoda, ktera zapisuje typ akce do daneho elementu
	 * @param element Element, do ktereho zapisujeme
	 * @param subLine Data, ktera chceme zapsat.
	 */
	private void updateAction(Element element, String subLine){
		//TODO slo by prepsat na enum
		if(UtilityClass.isItemInList(Actions.getValues(), subLine)){
			this.view.getElement().setAttribute("action", subLine);
		}
		else{
			showAlertNotification(getLabel("unknownAction") + ": " + subLine + "\n" + getLabel("actionWasNotSaved"));
		}
	}
	
	
	/** Metoda, ktera do daneho elementu zapisuje parametry prikazu
	 * @param element Element, do ktereho zapisujeme
	 * @param subLine Data, ktera chceme zapsat
	 * @throws UniverzalException
	 */
	private void updateElement(Element element, String subLine) throws UniverzalException{
		String [] token = UtilityClass.mySplit(subLine, "=");
		token[0] = token[0].replace("#", "").trim();
		token[1] = token[1].replace("#", "").trim();
		
		if(token.length > 1){
			Element el = element.getChild(token[0]); // <url></url>
			if(el == null){
				if(UtilityClass.isItemInList(Parameters.getValues(), token[0])){
					Element newEl = new Element(token[0]);
					Element label = new Element(LABEL).setText(token[1]);
					newEl.addContent(label);
					element.addContent(newEl);
				}
				else{
					showAlertNotification(getLabel("unknownParameter") + ": " + token[0] + "\n" + getLabel("parameterWasNotSaved"));
				}
			}
			// v el budu mit napr url, location..o
			else{
				Element label = el.getChild(LABEL);
				if(label == null){
					el.addContent(new Element(LABEL).setText(token[1]));
				}
				else{
					label.setText(token[1]);
				}
			}
		}
	}

	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setModel(Object model) {
		// TODO Auto-generated method stub
		
	}
}