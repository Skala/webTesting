/**
 * 
 */
package WebTesting.controller.rightPane.test;

import static WebTesting.controller.UtilityClass.getLabel;
import static WebTesting.controller.UtilityClass.showInfoNotification;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import WebTesting.Interfaces.IController;
import WebTesting.enums.Controllers;
import WebTesting.exceptions.MethodProhibitedException;
import WebTesting.exceptions.NotYetImplementedException;
import WebTesting.view.MainWindow;
import WebTesting.view.rightPane.test.modules.ControlTestMiddlePanel;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 5. 7. 2017
 * File: ControlTestMiddlePanelController.java
 */
public class ControlTestMiddlePanelController implements IController, ChangeListener {

	
	private ControlTestMiddlePanel view;
	
	
	public ControlTestMiddlePanelController(ControlTestMiddlePanel view){
		this.view = view;
	}
	
	
	@Override
	public Object getView() {
		return this.view;
	}
	
	@Override
	public Object getModel() {
		return MainWindow.getController(Controllers.EDITOR_TEST_CTRL).getModel();
	}

	@Deprecated
	@Override
	public void setModel(Object model) {
		try {
			throw new MethodProhibitedException();
		} catch (MethodProhibitedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void notifyController() {
		notifyAllControllers();
	}

	
	@Override
	public void notifyAllControllers() {
		MainWindow.getController(Controllers.EDITOR_TEST_SOURCE_CTRL).notifyController();
//		MainWindow.getController(Controllers.EDITOR_TEST_DESIGN_CTRL).notifyController();
	}

	
	@Override
	public Controllers getType() {
		return Controllers.TEST_MIDDLE_PANEL_CTRL;
	}

	/** Posluchac pro obsluhu udalosti po kliknuti na zalozku. 
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JTabbedPane) {
			JTabbedPane pane = (JTabbedPane) e.getSource();
			try {
				setTabbedPaneContent(pane);
			} catch (NullPointerException npe) {
				showInfoNotification(getLabel("youHaveToSelectTheTestToShow"));
			}
		}
	}
	
	private void setTabbedPaneContent(JTabbedPane pane){
		System.out.println("Selected paneNo : " + pane.getSelectedIndex());
		
		//TODO pred prepnutim se musi editor ulozit
		switch(pane.getSelectedIndex()){
		case 0:
			this.view.getControlTestPanel().get(0).reloadCurrentTest();
			break;
		case 1:
			this.view.getControlTestPanel().get(1).reloadCurrentTest();
			break;
		default:
			try {
				throw new NotYetImplementedException();
			} catch (NotYetImplementedException e) {
				
			}
			break;
		}
	}
}
