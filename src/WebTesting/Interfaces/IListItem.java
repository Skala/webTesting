/**
 * 
 */
package WebTesting.Interfaces;

/** Rozhrani pro manipulaci s uzly v seznamech
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 7. 3. 2016
 * File: IListSelenium.java
 */
public interface IListItem {

	/** Metoda pro ziskani nazvu dane polozky ze seznamu.
	 * @return Nazev polozky
	 */
	public String getName();
	
	/** Metoda pro ziskani parametru dane polozky
	 * @return Parametry polozky
	 */
	public String getParameters();
	
	/** Metoda pro ziskani popisu polozky ze seznamu.
	 * @return Popis polozky
	 */
	public String getHint();
	
//	/** Metoda pro ziskani vsech hodnot z enumu
//	 * @return Seznam vsech hodnot
//	 */
//	public String[] getValues();
//	
//	/** Metoda pro overeni, zda nejaky enum obsahuje danou polozku
//	 * @param ret Hledana polozka
//	 * @return odpoved, zda se polozka v enumu vyskytuje nebo ne
//	 */
//	public boolean contains(String ret);
}
