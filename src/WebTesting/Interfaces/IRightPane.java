/**
 * 
 */
package WebTesting.Interfaces;

/** Znackovaci rozhrani
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 4. 7. 2017
 * File: IRightPane.java
 */
public interface IRightPane {
	
	public String getPaneTitle();
}
