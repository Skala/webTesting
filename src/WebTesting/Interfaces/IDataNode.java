/**
 * 
 */
package WebTesting.Interfaces;

/** Rozhrani pro manipulaci s uzly.
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 15. 3. 2016
 * File: IDataNode.java
 */
public interface IDataNode {

	
	/**	Metoda, ktera ma za ukol prevest cely xml soubor do textove podoby
	 * @return Textovy retezec, ktery reprezentuje stromovou xml strukturu
	 */
	public String elementsToString();
	
	/**	Metoda, ktera prevede korenovy element nacteny ze xml souboru na textovou podobu reprezentujici test.
	 * @return Text, ktery reprezentuje konkretni test
	 */
	public String elementsToTextTest();
	
	
	/**	Z konkretniho xml souboru nacte data a zobrazi je v podobe scenare
	 * @return Text, ktery reprezentuje konkretni scenar
	 */
	public String elementsToTextScenario();
	
}
