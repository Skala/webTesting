/**
 * 
 */
package WebTesting.Interfaces;

import org.jdom2.Element;

import WebTesting.exceptions.LessElementsFoundException;
import WebTesting.exceptions.NoElementsFoundException;
import WebTesting.exceptions.NoInputStringException;
import WebTesting.exceptions.UnexpectedValueException;
import WebTesting.exceptions.UniverzalException;


/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 24. 3. 2017
 * File: IMyDriver.java
 */
public interface IMyDriver {

	/*
	 * definice metod rozhrani
	 */
	public void assertElements(Element currentCommand, String currentAction) throws Exception;
	public void assertText(Element currentCommand, String currentAction) throws Exception;
	public void assertTextPresent(String text, String expectedText) throws UniverzalException, NoElementsFoundException, LessElementsFoundException;
	public  void assertTitle(Element currentCommand) throws UnexpectedValueException, NoElementsFoundException, NoInputStringException, UniverzalException, InterruptedException;
	public void clear(Element currentCommand) throws Exception;
	public void click(Element currentCommand) throws Exception;
	public void dropDown(Element currentCommand, String currentAction) throws Exception;
	public void exists(Element currentCommand) throws Exception;
	public void changeHtml(Element currentCommand) throws Exception;
	public void mouseHover(Element currentCommand) throws Exception;
	public void navigate(Element currentCommand) throws UnexpectedValueException, NoElementsFoundException, NoInputStringException, UniverzalException;
	public void sleep(Element currentCommand) throws Exception;
	public void submit(Element currentCommand) throws Exception;
	public void switchToFrame(Element currentCommand) throws Exception;
	public void switchToDefault();
	public void type(Element currentCommand, String currentAction) throws Exception;
	public void visit(Element currentCommand) throws Exception; 
	public void verifyText(Element currentCommand, String currentAction) throws UnexpectedValueException, UniverzalException, NoElementsFoundException, NoInputStringException;
}
