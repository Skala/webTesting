/**
 * 
 */
package WebTesting.Interfaces;

/**
 * @author Pavel Skala
 * @version 1.0 
 * 
 * Created on: 6. 1. 2017
 * File: IControlTest.java
 */
public interface IControlTest {

	/**
	 * Metoda, ktera prekresli editor testu zdrojovymi daty
	 */
	public void reloadCurrentTest();
	public String getTitle();
}
